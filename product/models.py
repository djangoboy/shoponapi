from django.db import models
# from imagekit.models import ImageSpecField
# from imagekit.processors import ResizeToFill
# from product.productutility import IsUniqueComb
# Create your models here.

from lookup.models import lookupItem, lookupMainGroup

AVAILABLE_SIZES = (
    (None,None),
    ('S','S'),
    ('M','M'),
    ('L','L'),
    ('XL','XL'),
    ('XXL','XXL'),
    ('XXXL','XXXL')

)


# class ProductAvailableSize(models.Model):
#     size = models.CharField(choices=AVAILABLE_SIZES,default=None)

GENDER_TYPE = (
    (None,None),
    ('MEN','MEN'),
    ('KIDS','KIDS'),
    ('WOMEN','WOMEN')
)

PRODUCT_ITEM_TYPE = (
    ('SHOE','SHOE'),
    ('SHIRT','SHIRT'),
    ('TSHIRT','TSHIRT'),
    ('SWEATSHIRT','SWEATSHIRT'),
    ('PAINT','PAINT'),
    ('BARMUDAS','BARMUDAS')
)

CHEST_SIZE = (
    ('S','S'),
    ('M','M'),
    ('L','L'),
    ('XL','XL'),
    ('XXL','XXL'),
    ('XXXL','XXXL'),
)



MATERIAL_TYPE = (
    ('COTTON','COTTON'),
    ('JEANS','JEANS'),
    ('WOOLEN','WOOLEN')

)
WEAR_TYPE = (
    ('TOP_WEAR','TOP_WEAR'),
    ('BOTTOM_WEAR','BOTTOM_WEAR'),
    ('FOOT_WEAR','FOOT_WEAR')
)

COLOR_CHOICES = (
    ('BLACK','BLACK'),
    ('RED','RED'),
    ('WHITE','WHITE'),

)

GENERIC_SIZE_CHOICES=(
    ('SMALL','SMALL'),
    ('MEDIUM','MEDIUM'),
    ('LARGE','LARGE'),

)

FEET_SIZE_CHOICES= (
    (6,6),
    (7,7),
    (8,8),
    (9,9),
    (10,10)
)


CHEST_SIZE_CHOICES= (
    ('S','S'),
    ('M','M'),
    ('L','L'),
    ('XL','XL'),
    ('XXL','XXL')
)


WAIST_SIZE_CHOICES= (
    (30,30),
    (31,31),
    (32,32),
    (33,33)
    )

PRODUCT_TYPES = (
    ('CLOTHES','CLOTHES'),
    ('SHOES','SHOES'),
    )


class AvailableFeetSize(models.Model):
    feet_size = models.CharField(choices=FEET_SIZE_CHOICES,max_length=20)
    avail_quant = models.IntegerField(default=0)

    def __str__(self):
        return self.feet_size




class AvailableChestSize(models.Model):
    chest_size = models.CharField(choices=CHEST_SIZE_CHOICES,max_length=20)
    avail_quant = models.IntegerField(default=0)

    def __str__(self):
        return self.chest_size




class AvailableWaistSize(models.Model):
    waist_size = models.CharField(choices=WAIST_SIZE_CHOICES,max_length=20)
    avail_quant = models.IntegerField(default=0)

    def __str__(self):
        return self.chest_size


class ProductImageFiles(models.Model):
    image = models.FileField()

from django.http import Http404

# SIZE_N_COL_CHOICES=
WEAR_SIZE_TYPES=(
    (None,'None'),
    ('TOPWEAR','TOPWEAR'),
    ('MIDDLEWEAR','MIDDLEWEAR'),
    ('BOTTOMWEAR','BOTTOMWEAR')

)

COLLOR_TYPE_CHOICES=(
    (None,'None'),
    ('ROUND_NECK','ROUND_NECK'),
    ('V_NECK','V_NECK'),
    ('HOODS','HOODS')

)

SLEEVES_TYPE_CHOICES=(
    (None,'None'),
    ('HALF','HALF'),
    ('MEDIUM','MEDIUM'),
    ('FULL','FULL')

)

FITTING_TYPE_CHOICES=(
    (None,'None'),
    ('SLIM','SLIM'),
    ('LOOSE','LOOSE'),
    ('STRAIGHT','STRAIGHT')

)


# class ProductAvailCount(models.Model):
#     quantity = models.IntegerField()
#     # d_obj['availablilty'][0]['avail_size']['colors']
#     product_type=models.CharField(default='CLOTHES',choices=PRODUCT_TYPES,max_length=20)
#     avail_size = models.ForeignKey(AvailableSize,on_delete=models.SET_NULL,null=True,blank=True)

#     def __str__(self):
#         return str(self.quantity)+str(self.product_type)+str(self.avail_size)
#         # chest_size = models.ManyToManyField(AvailableChestSize,blank=True,null=True)
#         # waist_size = models.ManyToManyField(AvailableWaistSize,blank=True,null=True)

#     # def save(self, *args , **kwargs):
#     #     fs,cs,ws = self.feet_size,self.chest_size,self.waist_size
#     #     count=0
#     #     checkList = [fs,cs,ws]
#     #     for i in checkList:
#     #         if checkList[i]!=None:
#     #             count+=1

#     #     if count != 1:
#     #         raise Http404
#     #     # if not checkL
#     #     return supeAvr().save(self,*args,**kwargs)

class Product(models.Model):
    name = models.CharField(max_length=200, null=True,blank=True)
    # company_name = models.CharField(max_length=100)
    star_ratting = models.IntegerField(default=0)
    no_of_reviews = models.IntegerField(default=0)
    ratted_by = models.IntegerField(default=0)
    #apply validators
    # product_image_files = models.ManyToManyField(ProductImageFiles,blank=True)
    discount_per = models.IntegerField(default=0)
    gender_type = models.CharField(choices=GENDER_TYPE,max_length=100)
    # item_type = models.CharField(choices=PRODUCT_ITEM_TYPE,max_length=100)
    # wear_type = models.CharField(choices=WEAR_TYPE,max_length=100)
    material_type= models.CharField(choices=MATERIAL_TYPE,default=None,max_length=100)
    # available_colors = models.ManyToManyField(AvailableColor,blank=True)
    # availablilty = models.ForeignKey(ProductAvailCount,on_delete=models.SET_NULL,null=True,blank=True)
    # availablilty = models.ManyToManyField(AvailableSize)

    @staticmethod
    def checkifProdcutColImgsFound(prod_pk,col):
        product = Product.objects.get(pk=prod_pk)
        prod_avail = product.availablilty.all()
        colors_set = prod_avail.filter(colors=col)
        count = 0
        if colors_set:
            for p_img in colors_set:
                count = p_img.product_images.all().count
        return count


class AvailableSize(models.Model):
    product=models.ForeignKey(Product,on_delete=models.CASCADE)
    instock = models.BooleanField(default=True)
    quantity = models.IntegerField()
    # wear_type =models.CharField(default=None,choices=WEAR_SIZE_TYPES,max_length=20,blank=True)
    
    chest_size = models.CharField(choices=CHEST_SIZE_CHOICES,default=None,max_length=20,null=True,blank=True)
    category = models.ForeignKey(lookupItem, related_name='product_category', on_delete=models.CASCADE)
    # group_category = models.ForeignKey(lookupMainGroup, related_name='product_category', on_delete=models.CASCADE, null=True, blank=True)
    
    # collor_type = models.CharField(choices=COLLOR_TYPE_CHOICES,default=None,max_length=20,null=True,blank=True)
    # sleeves_type = models.CharField(choices=SLEEVES_TYPE_CHOICES,default=None,max_length=20,null=True,blank=True)
    # fitting_type = models.CharField(choices=FITTING_TYPE_CHOICES,default=None,max_length=20,null=True,blank=True)
    
    feet_size = models.CharField(choices=FEET_SIZE_CHOICES,default=None,max_length=20,null=True,blank=True)
    # collor_type = models.CharField(choices=COLLOR_TYPE_CHOICES,default=None,max_length=20,null=True,blank=True)

    waist_size = models.CharField(choices=WAIST_SIZE_CHOICES,default=None,max_length=20,null=True,blank=True)

    colors = models.CharField(choices=COLOR_CHOICES,max_length=20,null=True,blank=True)
    # product_images = models.ManyToManyField(ProductImages,blank=True,null=True)
    price = models.FloatField(default=None,blank=True,null=True)
    generic_size = models.CharField(choices=GENERIC_SIZE_CHOICES,default=None,max_length=20,null=True,blank=True)
     
    
    def IsUniqueComb(self,color,size):
        all_sets = self.product.availablesize_set.all()
        print('===///',size,self.product.availablesize_set.all(),color)

        for itm in all_sets:
            if itm.colors==color and (itm.chest_size==size or itm.feet_size==size or itm.waist_size==size or itm.generic_size==size):
                return True
                break
            else:
                return False 
                break
                
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        
        #2. On the basis of group >> decide what size parameters should be considered 
        #3. raise error if other parameters are provided or if main is not provided

        #4. check the combination of Stock ==> raise error if the same stock is being added 
        
    #     #if self.colors && self.chest_size uniquely already is added through an error reminding that
        
            #self.wear_type='BOTTOMWEAR'
        gn,gv = self.category.group.m_group.value, self.category.group.m_group.value
        size=None
        if gn=='TOP_WEAR' and gv=='TOP_WEAR':
            if not self.chest_size:
                raise NotImplementedError('You Need to provide "Chest Size" from dropdown as You ve selected "TOP_WEAR" in product type; Hence Any other size if provided will be Ignored Here')
            else:
                size = self.chest_size
        if gn=='BOTTOM_WEAR' and gv=='BOTTOM_WEAR':

            if not self.waist_size:
                raise NotImplementedError('You Need to provide "Waist Size" from dropdown as You ve selected "BOTTOM_WEAR" in product type; Hence Any other size if provided will be Ignored Here')
            else:
                size = self.chest_size
        if gn=='FOOT_WEAR' and gv=='FOOT_WEAR':
            if not self.feet_size:
                raise NotImplementedError('You Need to provide "Chest Size" from dropdown as You ve selected "FOOT_WEAR" in product type; Hence Any other size if provided will be Ignored Here')
            
        elif gn=='OTHER' and gv=='OTHER' :
            if not self.generic_size:
                raise NotImplementedError('You Need to provide "Generic Size" from dropdown as You ve selected "OTHER" in product type; Hence Any other size if provided will be Ignored Here')


        
        # TODOLATER we must impliment not letting user add the multiple of same color+size combination
        if self.IsUniqueComb(self.colors,size):
            raise NotImplementedError('this size and color combintion for this product is already there, update that if you wish!')

    #     if not self.chest_size and not self.waist_size and not self.feet_size :
    #         raise NotImplementedError('You need to choose atleast one size choice')
        return super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    # def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
    #     gn,gv = self.category.group.m_group.value, self.category.group.m_group.value
        
    #     #2. On the basis of group >> decide what size parameters should be considered 
    #     #3. raise error if other parameters are provided or if main is not provided

    #     #4. check the combination of Stock ==> raise error if the same stock is being added 
        
    #     print('selfish',self)
    #     g = self.category.group.m_group.value

    #     print(g,'self.category.group.m_group.value')
    # #     #if self.colors && self.chest_size uniquely already is added through an error reminding that
    #     crnt_size = None
    #     if self.chest_size:
    #         crnt_size = self.chest_size
    #         # self.wear_type='TOPWEAR'
    #     if self.waist_size:
    #         crnt_size = self.waist_size
    #         #self.wear_type='MIDDLEWEAR'
    #     if self.feet_size:
    #         crnt_size = self.feet_size
    #         #self.wear_type='BOTTOMWEAR'
    #     if self.generic_size:
    #         crnt_size=self.generic_size

        
    #     # TODOLATER we must impliment not letting user add the multiple of same color+size combination
    #     if self.IsUniqueComb(self.colors,crnt_size):
    #         raise NotImplementedError('this size and color combintion for this product is already there, update that if you wish!')

    # #     if not self.chest_size and not self.waist_size and not self.feet_size :
    # #         raise NotImplementedError('You need to choose atleast one size choice')
    #     return super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    def __str__(self):
        if self.feet_size:
            return 'Product_id: '+str(self.product.pk)+ '-->' + str(self.feet_size)+ '-->' +str(self.colors)
        if self.waist_size:
            return 'Product_id: '+str(self.product.pk)+ '-->' + str(self.waist_size)+'-->' +str(self.colors)
        if self.chest_size:
            return 'Product_id: '+str(self.product.pk)+ '-->' + str(self.chest_size)+'-->' +str(self.colors)
        else:
           return 'Product_id: '+str(self.product.pk)+ '-->' + str(self.colors)


PRODUCT_TYPE = (
    (None,None),
    ('TOP_WEAR','TOP_WEAR'),
    ('BOTTOM_WEAR','BOTTOM_WEAR'),
    ('FOOT_WEAR','FOOT_WEAR'),
    ('OTHER','OTHER')

)


from django_mysql.models import JSONField

class ProductState4Filter(models.Model):
    product_type = JSONField(null=True, blank=True)
    sub_type = JSONField(null=True, blank=True)
    color = JSONField(null=True,blank=True)
    item = JSONField(null=True,blank=True)

    # product_type=JSONField(max_length=20,choices=PRODUCT_TYPE,null=True,blank=True,default=None)
    # sub_type=models.CharField(max_length=20,choices=PRODUCT_TYPE,null=True,blank=True,default=None)
    # product_type=models.CharField(max_length=20,choices=PRODUCT_TYPE,null=True,blank=True,default=None)        
    # sub_type=models.CharField(max_length=20)
    # color=models.CharField(max_length=20)
    # size= JSONField(null=True, blank=True)


# from lookup.models import *
# from product.models import *
# all_ps = Product.objects.all()
# for p in all_ps:
#     p_stocks = p.availablesize_set.all()
#     cat_k,cat_v=None,None
#     for item in p_stocks:
          
#         print('item colors',item.colors)
#         print('item Group Name',item.category.group.name)
#         print('item Group Value',item.category.group.value)
#         print('item catorgory Group Name',item.category.group.m_group.name)
#         print('item catorgory Group Value',item.category.group.m_group.value)
#         print('item catorgory Name',item.category.name)
#         print('item catorgory Value',item.category.value)
#         print('item catorgory Quantity',item.quantity)

        # ProductState4Filter(
        #       product_type={'type':prod_type,'nums':1},
        #       sub_type=sub_type,
        #       color="",
        #       item=""
        #   )
        
class Topping(models.Model):
    name = models.CharField(max_length=30)

class Pizza(models.Model):
    name = models.CharField(max_length=50)
    toppings = models.ManyToManyField(Topping)

    def __str__(self):              # __unicode__ on Python 2
        return "%s (%s)" % (self.name, ", ".join(topping.name for topping in self.toppings.all()))


class Restaurant(models.Model):
    pizzas = models.ManyToManyField(Pizza, related_name='restaurants')
    best_pizza = models.ForeignKey(Pizza, related_name='championed_by',on_delete=models.CASCADE)
# from product.models import Product
# p_set = Product.checkifProdcutColImgsFound(1,'WHITE')
# for i in p_set:
#     print(i)

    # def save(self,*args,**kwargs):
    #     # print(self.availablilty.all(),'herere')
    #     return super().save(*args,**kwargs)
    # # def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
    # #     # #here check if product_images is given for the same color ever or throgh error here
    # #     # #get color
    # #     # import copy
    # #     # selfcopy = copy.copy(self)
        
    # #     print('-->>>>',self.availablilty.all())
    # #     # # self.availablilty.first()
    # #     # # product_col_imgs = Product.checkifProdcutColImgsFound(self.pk,self.colors)
    # #     # if self.availablilty:
    # #     # # product_col_imgs = Product.checkifProdcutColImgsFound(self.pk,self.colors)
    # #     #     print(self.pk,'sefl=======>>>>>>>')
    # #     #     if not product_col_imgs:
    # #     #         raise NotImplementedError('If this is for the first time please Make sure you got the images added')

    # #     return super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    # # def save(self, *args , **kwargs):
    # #     if len(p3.availablilty.values_list()) > 0:
    # #         raise Http404(e)


    #     # print()
    #     # if  == None:
    #     # print('m ithere =>>>>>>>>>>>>>>>>>>>>>>',self.availablilty.pk)
    #         # avail_size = ProductAvailCount.objects.create(feet_size=None,waist_size=None,chest_size=None)
    #         # self.availablilty = avail_size
    #     # return super().save(self,*args,**kwargs)

    # def __str__(self):
    #     return self.name + str(self.gender_type)
SIDE_CHOICES=(
    ('FRONT','FRONT'),
    ('BACK','BACK'),
    ('LEFTSIDE','LEFTSIDE'),
    ('RIGHTSIDE','RIGHTSIDE'),
    ('TOP','TOP'),
    ('BOTTOM','BOTTOM'),

)

class ProductImages(models.Model):
    side = models.CharField(max_length=100,choices=SIDE_CHOICES)
    availibility = models.ForeignKey(AvailableSize,on_delete=models.CASCADE)
    image = models.ImageField(upload_to='product_imagess', null=True, blank=True, width_field="width_field", height_field="height_field")
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    # image_thumbnail = ImageSpecField(source="image",processors=[ResizeToFill(100,50)],format='JPEG',options={'quality':60})

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        #if self.colors && self.chest_size uniquely already is added through an error reminding that
        # print(self.availibility.colors,self.availibility.wear_type,self.availibility.chest_size)
        return super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)


# from django.db import models
# from imagekit.models import ImageSpecField

# class Photo(models.Model):
#     original_image = models.ImageField(upload_to='photos')
#     formatted_image = ImageSpecField(image_field='original_image', format='JPEG',
#             options={'quality': 90})


class Doctor(models.Model):
    doctor_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=250)
    institute = models.CharField(max_length=500, blank=True)
    speciality = models.CharField(max_length=500, blank=True)
    address = models.CharField(max_length=250, blank=True)
    country = models.CharField(max_length=50, blank=True)
    latitude = models.FloatField(max_length=50, blank=True, default=0)
    longitude = models.FloatField(max_length=50, blank=True, default=0)
    phone = models.CharField(max_length=250, blank=True)
    degree = models.CharField(max_length=250, blank=True)
    field = models.CharField(max_length=250, blank=True)

    class Meta:
        ordering = ["name"]