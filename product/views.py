from django.shortcuts import render
from product.productutility  import modifyProductDetailObj
from product.filters import ProductFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    UpdateAPIView,
    DestroyAPIView,
    ListAPIView,
    )
from rest_framework.views import APIView
from .models import Product
from .serializers import *
from django_filters import rest_framework as filters

from config.jsonrand import custom_response
from rest_framework.status import HTTP_200_OK
from rest_framework.response import Response
from rest_framework.pagination import (
    LimitOffsetPagination,
    PageNumberPagination
)

def makeMeFilter(fb_url):
    filter_obj={}
    b,qu = fb_url.split('?')
    query_params_li=qu.split('&')
    product_type=None
    product_subtype=None
    for q in query_params_li:
        k,v = q.split('=')
        print('k=v',k,v)
        if k=='product_type':
            product_type=v
        elif k=='product_subtype':
            product_subtype=v
    return {product_type:product_type,product_subtype:product_subtype}

class CustomPaginationApiV3(PageNumberPagination):
    def get_paginated_response(self, data):
        filter_base_url,prod_data = data[0],data[1:]
        #here We can create A Futher Valid Filter_Obj
        print('-->',prod_data,filter_base_url,'<--')
        #here you get the price & color & size
        #on product level we have instock that goes to false if instock availibity in total goes to 0
        avail_colors = []
        for p in prod_data:
            prod_images=[]
            p_obj = dict(p)
            print(p_obj,'p_obj.name,',p_obj['name'])
            avaisize_set = dict(p_obj)['availablesize_set']
            for item in avaisize_set:
                itm = dict(item)
                print(itm['product'],'item')
            # avail_cols = p_obj['availablilty'][0]['avail_size']['colors']
            # # avail_cols = null
            # current_col = dict(avaisize_set[0])['colors']
            # self.category.group.m_group.value
        



        #here prepare filter from url in fursat
        print('filter_base_url',filter_base_url)
        ffilter_obj = makeMeFilter(filter_base_url)
        print(ffilter_obj,'final fobj')
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            # 'count': self.page.paginator.count,
            'results': prod_data,
            'filter_obj':{}
        })

from rest_framework.status import HTTP_200_OK

class ProductListv3(APIView):
    # queryset = Product.objects.all()
    serializer_class = ProductSerializer
    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        # ratted_by = self.kwargs['ratted_by']
        ratted_by = self.request.query_params.get('ratted_by', None)
        print('ratted_by',ratted_by)
        
        # return Purchase.objects.filter(purchaser__username=username)
        return Product.objects.filter(ratted_by=ratted_by)
    pagination_class=CustomPaginationApiV3
    # filterset_class = ProductFilter
    # filter_backends = (DjangoFilterBackend,)
    # # filterset_fields = ('chest_size',)
    # filter_fields = ('ratted_by',)
    def get(self, request, format=None):
        # usernames = [user.username for user in User.objects.all()]
        # data=Product.objects.all()
        data=self.get_queryset()
        paginator = CustomPaginationApiV3()
        result_page = paginator.paginate_queryset(data, request,view=self)
        serializer = ProductSerializer(result_page,many=True)
        print(request.build_absolute_uri(),'request')
        # response = Response(serializer.data, status=HTTP_200_OK)
        # return response
        base_url = request.build_absolute_uri()
        data_list=serializer.data
        data_list.insert(0,base_url)
        return paginator.get_paginated_response(data_list)
    


class CustomPagination(PageNumberPagination):
    def get_paginated_response(self, data):
        #here We can create A Futher Valid Filter_Obj
        print('-->',data,'<--')
        #here you get the price & color & size
        #on product level we have instock that goes to false if instock availibity in total goes to 0
        avail_colors = []
        for p in data:
            prod_images=[]
            p_obj = dict(p)
            print(p_obj,'p_obj.name,',p_obj['name'])
            avaisize_set = dict(p_obj)['availablesize_set']
            for item in avaisize_set:
                itm = dict(item)
                print(itm['product'],'item')
            # avail_cols = p_obj['availablilty'][0]['avail_size']['colors']
        #     # avail_cols = null
            # current_col = dict(avaisize_set[0])['colors']
            # self.category.group.m_group.value
        




        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            # 'count': self.page.paginator.count,
            'results': data,
            'filter_obj':{}
        })



class ProductListv2(ListAPIView):
    # queryset = Product.objects.all()
    serializer_class = ProductSerializer
    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        # ratted_by = self.kwargs['ratted_by']
        ratted_by = self.request.query_params.get('ratted_by', None)
        print('ratted_by',ratted_by)

        # return Purchase.objects.filter(purchaser__username=username)
        return Product.objects.all()
    pagination_class=CustomPagination
    # filterset_class = ProductFilter
    # filter_backends = (DjangoFilterBackend,)
    # # filterset_fields = ('chest_size',)
    # filter_fields = ('ratted_by',)
    

class ProductList(ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    # filter_fields = ('no_of_reviews', 'available_colors')
    # filter_class = ProductFilter
                                    # filterset_class = ProductFilter
    # filter_backends = (DjangoFilterBackend,)
    # filterset_fields = ('no_of_reviews')
    pagination_class=CustomPagination


#delete later on this is just to check if filter of any kind will work (1. djangoFilterBackend)
from rest_framework import filters as restfilters

class ProductList2ChkfilterV2(ListAPIView):
    # queryset = Product.objects.all()
    serializer_class = ProductSerializer
    def get_queryset(self,*args,**kwargs):
        """
        This view should return a list of all the Products for
        all the filters as (color,size) as determined by the color ||&& size portion of the URL.
        """
        print('==>',dir(self),'<==',self.args,self.kwargs,self.request.query_params.get('color',None))
        # username = self.kwargs['pk']
        color = self.request.query_params.get('color',None)
        #here may be if we can use prefetch_related
        #for this color and this product we need to find 
        return Product.objects.all()



#below is working beautiful
class ProductList2Chkfilter(ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    # filter_backends = (filters.DjangoFilterBackend,)
    # filterset_fields = ('no_of_reviews',)

    # filter_class = ProductFilter
    # filter_backends = (filters.DjangoFilterBackend,)
    #HERE BELOW GOES the Filters => star_rattings that we can apply from this table later
    # search_fields = ('star_ratting',)
    # filter_fields = ('no_of_reviews','wear_type')



class DoctorAPIView(ListAPIView):
    queryset = Doctor.objects.all()
    serializer_class = DoctorSerializer
    filter_backends = (restfilters.SearchFilter, filters.DjangoFilterBackend)
    search_fields = ('name', 'speciality', 'institute', 'address')
    filter_fields = ('country',)


#working
# class ProductList(ListAPIView):
#     queryset = Product.objects.all()
#     serializer_class = ProductSerializer
#     filter_backends = (DjangoFilterBackend,filters.SearchFilter)
#     filter_fields = ('no_of_reviews','gender_type','wear_type','chest_size','available_colors')
#     search_fields = ('no_of_reviews',)
#     pagination_class=CustomPagination
#     # def list(self, request, *args, **kwargs):
#     #     queryset = self.filter_queryset(self.get_queryset())
#     #     serializer = self.get_serializer(queryset, many=True)
#     #     # print(serializer.data,'<--<<<',type(serializer.data))
#     #     # final_li = serializer.data
#     #     # final_li.insert(0, {'total_count':250})
#     #     # return Response(serializer.data, status=HTTP_200_OK)
#     #     return self.pagination_class(serializer.data)

#     #     # return custom_response([], '', message='success', status_code=HTTP_200_OK)
    
    
#     # def get_queryset(self,*args,**kwargs):
#     #     query_list = self.filter_queryset(self.queryset)
#     #     print(query_list,'<--query_list')
#     #     return query_list

# class ProductDetail(APIView):


#     def get(self,request,*args,**kwargs):
#         print(kwargs)
#         pk = kwargs['pk']
#         from .models import Product
#         product = Product.objects.filter(id=pk)
#         # print('product',product)
#         # import json
#         # prod = json.dumps(product)
#         # print('prod',prod)
#         from django.core import serializers
#         data = serializers.serialize('json', product)
        
#         return custom_response(data, '', message='success', status_code=HTTP_200_OK)

#Here goes the data

def returnRelvantObj(data_obj):
    return data_obj


class ProductDetail(APIView):
    # queryset = Product.objects.all()
    # serializer_class = ProductSerializer
    # filter_backends = (filters.DjangoFilterBackend,)
    # filterset_fields = ('category', 'in_stock')
    """
    Retrieve a model instance.
    """
    from .models import Product
    from django.http import Http404
    def get_object(self, pk):
        try:
            return Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            raise Http404  
    
    def get(self, request, pk,color=None, format=None):
        available_colors = []
        available_sizes = []
        main_imgs_li = []
        prodcut_price = None
        print(color,'color')
        service = self.get_object(pk)
        serializer = ProductSerializer(service)
        ser_data = serializer.data
        res_data = ser_data
        returned_obj = modifyProductDetailObj(ser_data['availablesize_set'],wear_type='TOP_WEAR',color=color!='main' and color or None )
        del res_data['availablesize_set']
        print(returned_obj,'returned_obj')
        res_data.update(returned_obj)
        return custom_response(res_data, '', message='', status_code=HTTP_200_OK)


# class ProductDetail(APIView):
#     """
#     Retrieve a model instance.
#     """
#     from .models import Product
#     from django.http import Http404
#     def get_object(self, pk):

#         try:
#             return Product.objects.get(pk=pk)
#         except Product.DoesNotExist:
#             raise Http404  
    
#     def get(self, request, pk,color=None, format=None):
#         available_colors = []
#         available_sizes = []
#         main_imgs_li = []
#         prodcut_price = None
#         print(color,'color')
#         service = self.get_object(pk)
#         serializer = ProductSerializer(service)
#         ser_data = serializer.data
#         # avail_obj = ser_data['availablilty']
#         # if color=='main':
#         #     itr = 0
#         #     while itr < 2:
#         #         for i in avail_obj:
#         #             avail_li = dict(i)
#         #             avail_size_obj = dict(avail_li['avail_size'])
#         #             if itr==0:
#         #                 main_imgs_li = avail_size_obj['product_images']
#         #                 prodcut_price = avail_size_obj['price']
#         #             cols = avail_size_obj['colors']
#         #             chest_size = avail_size_obj['chest_size']
#         #             available_colors.append(cols)
#         #             # main_product_imgs = avail_obj[0]['product_images']
#         #             # out_of_stock = False
#         #             available_sizes.append(chest_size)
#         #             itr += 1
#         #             # ser_data['price']
#         #     del ser_data['availablilty']
#         #     ser_data['product_price'] = prodcut_price
#         #     ser_data['available_colors'] = available_colors
#         #     ser_data['available_sizes'] = available_sizes
#         #     return custom_response(ser_data, '', message='', status_code=HTTP_200_OK)

#         # else:
#         #     for i in avail_obj:
#         #         print(i,'here is i')
#         #         avail_li = dict(i)
#         #         # avail_size_obj = dict(avail_li['avail_size'])
#         #         cols = avail_li['colors']
#         #         if cols==color:
#         #             #here we might get issues of having multiple images for same color
#         #             main_imgs_li = avail_li['product_images']
#         #             prodcut_price = avail_li['price']
#         #             chest_size = avail_li['chest_size']
#         #             available_colors.append(cols)
#         #             available_sizes.append(chest_size)
#         #             break
#         #         else:
#         #             chest_size = avail_li['chest_size']
#         #             available_colors.append(cols)
#         #             available_sizes.append(chest_size)

#         #     del ser_data['availablilty']
#         #     ser_data['product_price'] = prodcut_price
#         #     ser_data['product_images'] = main_imgs_li
#             # ser_data['available_sizes'] = available_sizes
#         return custom_response(ser_data, '', message='', status_code=HTTP_200_OK)
