from django.conf.urls import url
from .views import *

app_name ='product'


urlpatterns = [
    url(r'^all', ProductListv3.as_view()),
    # url(r'^(?P<pk>[0-9]+)/(?P<slug>[\w-]+)/$', ProductDetail.as_view()),
    url(r'^(?P<pk>[0-9]+)/(?P<color>\w+)$', ProductDetail.as_view()),
   
    #this below is for Wv2
    # url(r'^tocheck_filterall/', ProductList2Chkfilter.as_view()),

    url(r'^tocheck_filterallv2/', ProductList2ChkfilterV2.as_view()),

    url(r'^doctor/', DoctorAPIView.as_view()),

    # url(r'^tocheckfilter/(?P<pk>[0-9]+)/(?P<color>\w+)$', ProductList2Chkfilter.as_view()),

]
