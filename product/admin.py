from django.contrib import admin
from .models import Product, AvailableChestSize, AvailableFeetSize, AvailableWaistSize, ProductImages, ProductImageFiles, AvailableSize, Doctor, Topping, Pizza, Restaurant


admin.site.register(Topping)
admin.site.register(Pizza)
admin.site.register(Restaurant)

admin.site.register(Product)

admin.site.register(Doctor)

# admin.site.register(AvailableColor)
admin.site.register(AvailableChestSize)
admin.site.register(AvailableSize)
admin.site.register(AvailableFeetSize)
admin.site.register(AvailableWaistSize)

admin.site.register(ProductImages)
admin.site.register(ProductImageFiles)


# Register your models here.
