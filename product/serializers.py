from rest_framework import serializers
from .models import *
# from .filters import ProductFilter


class ImagesSerializer(serializers.RelatedField):

     def to_representation(self, value):
         print('value-->',value.image,value.image.url)
         return value.image.url

     class Meta:
        model = ProductImages



# class AvailColorSerializer(serializers.RelatedField):

#      def to_representation(self, value):
#          return value.color

#      class Meta:
#         model = AvailableColor



class AvailFeetSizeSerializer(serializers.RelatedField):

     def to_representation(self, value):
         return value.feet_size

     class Meta:
        model = AvailableFeetSize


class AvailChestSizeSerializer(serializers.RelatedField):

     def to_representation(self, value):
         print(self,value,value.chest_size,'-----------herelook-------')
         return value.chest_size

     class Meta:
        model = AvailableChestSize


class AvailWaistSizeSerializer(serializers.RelatedField):

     def to_representation(self, value):
         return value.waist_size

     class Meta:
        model = AvailableWaistSize



class ProductImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImages
        fields = ('side','image')
        # fields = '__all__'


class AvailableSizeSerializer(serializers.ModelSerializer):
    # product_images = ImagesSerializer(read_only=True, many=True)
    productimages_set = ProductImagesSerializer(read_only=True, many=True) # many=True is required

    class Meta:
        # product = ProductSerializer(read_only=True,many=True)
        model = AvailableSize
        # fields = ('quantity','feet_size','chest_size','waist_size','colors','price','productimages_set')
        # fields = ('quantity',)
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    availablesize_set = AvailableSizeSerializer(read_only=True, many=True) # many=True is required
    # product_main_image = ImagesSerializer(read_only=True, many=True)
    # availablilty = AvailableSizeSerializer(many=True)
    # product_images = ImagesSerializer(read_only=True, many=True)
    # chest_size = AvailChestSizeSerializer(read_only=True, many=True)
    # waist_size = AvailWaistSizeSerializer(read_only=True, many=True)
    # feet_size = AvailFeetSizeSerializer(read_only=True, many=True)
    # availibility = AvailableSizeSerializer()
    class Meta:
        model = Product
        # depth = 2
        # fields = ['name']
        fields = '__all__'
        # filter_class = ProductFilter




class DoctorSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = Doctor
        # depth = 2
        # fields = ['name','star_ratting','availibility']
        fields = '__all__'
        # filter_class = ProductFilter


# class ProductSerializer(serializers.ModelSerializer):
#     """Serializers registration requests and creates a new user."""
#     product_images = ImagesSerializer(read_only=True, many=True)
#     available_colors = AvailColorSerializer(read_only=True, many=True)
#     chest_size = AvailChestSizeSerializer(read_only=True, many=True)
#     waist_size = AvailWaistSizeSerializer(read_only=True, many=True)
#     feet_size = AvailFeetSizeSerializer(read_only=True, many=True)

#     #chest feet waist
#     #price_type = price_type_Serializer()
#     # business_location = serializers.SlugRelatedField(queryset=Vendor_businessLocation.objects.all(),
#     #                                                  many=False,
#     #                                                  read_only=False,
#     #                                                  slug_field='business_name'
#     #                                                  )
#     # business_location_name = serializers.ReadOnlyField(source='business_name')
#     # vendor_response_time = serializers.ReadOnlyField(source='business_location.vendor_response_time')
#     # name = serializers.ReadOnlyField(source='alias_name')
#     # gallery = service_GallerySerializer(many=True, allow_null=True)
#     # opening_timings = service_WeekdayOpeningPeriodSerializer(many=True, allow_null=True)
#     # documents = service_DocumentSerializer(many=True, allow_null=True)

#     class Meta:
#         model = Product
#         fields='__all__'




# class ProductSerializer(serializers.ModelSerializer):
#     """Serializers registration requests and creates a new user."""
#     #price_type = price_type_Serializer()
#     # business_location = serializers.SlugRelatedField(queryset=Vendor_businessLocation.objects.all(),
#     #                                                  many=False,
#     #                                                  read_only=False,
#     #                                                  slug_field='business_name'
#     #                                                  )
#     # business_location_name = serializers.ReadOnlyField(source='business_name')
#     # vendor_response_time = serializers.ReadOnlyField(source='business_location.vendor_response_time')
#     # name = serializers.ReadOnlyField(source='alias_name')
#     # gallery = service_GallerySerializer(many=True, allow_null=True)
#     # opening_timings = service_WeekdayOpeningPeriodSerializer(many=True, allow_null=True)
#     # documents = service_DocumentSerializer(many=True, allow_null=True)

#     class Meta:
#         model = Product
#         fields='__all__'

