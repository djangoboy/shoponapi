# Generated by Django 2.0.2 on 2020-03-25 15:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0003_auto_20200325_1513'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='availablesize',
            name='group_category',
        ),
    ]
