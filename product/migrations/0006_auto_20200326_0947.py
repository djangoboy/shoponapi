# Generated by Django 2.0.2 on 2020-03-26 09:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0005_auto_20200325_2008'),
    ]

    operations = [
        migrations.AlterField(
            model_name='availablesize',
            name='colors',
            field=models.CharField(blank=True, choices=[('BLACK', 'BLACK'), ('RED', 'RED'), ('WHITE', 'WHITE')], max_length=20, null=True),
        ),
    ]
