from rest_framework import serializers

from .models import *


class lookupgroupserializer(serializers.ModelSerializer):

    class Meta:
        model = lookupgroup
        fields = '__all__'


class lookupitemserializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""
    lookupgroup = serializers.SerializerMethodField(lookupgroup)

    class Meta:
        model = lookupItem
        fields = '__all__'
