from django.contrib.contenttypes.models import ContentType
from django.db import models as models
from django.urls import reverse
from django_extensions.db import fields as extension_fields
from django.utils.translation import ugettext_lazy as _

sex = (
    ('M', 'Male'),
    ('F', 'Female'),
)
body_type = (
    ('S', 'Slim'),
    ('A', 'Average'),
)

eye_color = (
    ('b', 'black'),

)

chest_size = (
    ('S', 'Slim'),
    ('A', 'Average'),
)

dress_size = (
    ('S', 'Slim'),
    ('A', 'Average'),
)

hair_color = (
    ('S', 'Slim'),
    ('A', 'Average'),
)

hair_style = (
    ('S', 'Slim'),
    ('A', 'Average'),
)

height = (
    ('S', 'Slim'),
    ('A', 'Average'),
)

hips = (
    ('S', 'Slim'),
    ('A', 'Average'),

)

in_seam = (
    ('S', 'Slim'),
    ('A', 'Average'),
)

shirt_size = (
    ('S', 'Slim'),
    ('A', 'Average'),
)

shoe_size = (
    ('S', 'Slim'),
    ('A', 'Average'),
)

waist_size = (
    ('S', 'Slim'),
    ('A', 'Average'),
)

SERVICES = (
    ('m', 'Model'),
    ('c', 'Cameraman'),
    ('s', 'Studio')
)

SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY = range(7)
weekday_choices = (
    (SUNDAY, _("Sunday")),
    (MONDAY, _("Monday")),
    (TUESDAY, _("Tuesday")),
    (WEDNESDAY, _("Wednesday")),
    (THURSDAY, _("Thursday")),
    (FRIDAY, _("Friday")),
    (SATURDAY, _("Saturday")),
)

'''
from lookupgroup.model import *
lookugroup_lookupitem = lookupgroup.objects.select_related('lookupitem').all()

'''


DAILY, HOURLY = range(2)

price_type = (
    (DAILY, _("Daily")),
    (HOURLY, _("Hourly")),
)


class lookupMainGroup(models.Model):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return '{name}:  {value}'.format(
            name=self.name,
            value=self.value
        )


class lookupgroup(models.Model):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=50, blank=True)
    m_group = models.ForeignKey(lookupMainGroup,on_delete=models.CASCADE)

    def __str__(self):
        return '{name}:  {value} :: {m_group}'.format(
            name=self.name,
            value=self.value,
            m_group=self.m_group
        )


class lookupItem(models.Model):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=50)
    # value = models.CharField(max_length=30)
    group = models.ForeignKey(lookupgroup, related_name='lookupitem', on_delete=models.SET_NULL,
                              null=True, verbose_name='group', blank=True, default=None)

    class Meta:
        ordering = ['name', 'group']

    def __str__(self):
        return '{group} -> {name}:  {value}'.format(
            group=self.group,
            name=self.name,
            value=self.value
        )

    def save(self, *args, **kwargs):
        #as per the front end requirment
        self.value = self.value.upper()
        return super(lookupItem, self).save(*args, **kwargs)

# class MappedOptionsGroup(models.Model):
#     name = models.CharField(max_length=50),
#     value = models.CharField(max_length=50)
#
#
#

class MappedOptGroup(models.Model):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=50)


class MappedOption(models.Model):
    name = models.CharField(max_length=50)
    # options = models.ManyToManyField(OptionEntry)
    group = models.ForeignKey(MappedOptGroup, on_delete=models.SET_NULL,null=True,blank=True)


    def __str__(self):
        return self.name+str(self.group)
    

class OptionEntry(models.Model):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=50)
    mapped_option = models.ForeignKey(MappedOption, on_delete=models.SET_NULL,null=True,blank=True)

    def __str__(self):
        return self.name+str(self.value)+str(self.mapped_option)



# class MappedOptionsItem(models.Model):
#     name = models.CharField(max_length=50)
#     value = models.CharField(max_length=50)
#     # value = models.CharField(max_length=30)
#     # group = models.ForeignKey(lookupgroup, related_name='lookupitem', on_delete=models.SET_NULL,
#     #                           null=True, verbose_name='group', blank=True, default=None)
#
#     class Meta:
#         ordering = ['name']
#
#     def __str__(self):
#         return '{name}:  {value}'.format(
#             # group=self.group,
#             name=self.name,
#             value=self.value
#         )
#
#     def save(self, *args, **kwargs):
#         #as per the front end requirment
#         self.value = self.value.upper()
#         return super(MappedOptionsItem, self).save(*args, **kwargs)
#
#
#
# class MappedOptionsGroup(models.Model):
#     name = models.CharField(max_length=50)
#     value = models.CharField(max_length=50, blank=True)
#     item_options = models.ManyToManyField(MappedOptionsItem, null=True, blank=True)
#
#
#     def __str__(self):
#         return '{name}:  {value}'.format(
#             name=self.name,
#             value=self.value
#         )
#
