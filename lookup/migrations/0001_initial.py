# Generated by Django 2.0.2 on 2020-03-25 08:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='lookupgroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('value', models.CharField(blank=True, max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='lookupItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('value', models.CharField(max_length=50)),
                ('group', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='lookupitem', to='lookup.lookupgroup', verbose_name='group')),
            ],
            options={
                'ordering': ['name', 'group'],
            },
        ),
        migrations.CreateModel(
            name='lookupMainGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('value', models.CharField(blank=True, max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='MappedOptGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('value', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='MappedOption',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('group', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='lookup.MappedOptGroup')),
            ],
        ),
        migrations.CreateModel(
            name='OptionEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('value', models.CharField(max_length=50)),
                ('mapped_option', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='lookup.MappedOption')),
            ],
        ),
        migrations.AddField(
            model_name='lookupgroup',
            name='m_group',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lookup.lookupMainGroup'),
        ),
    ]
