from django.contrib import admin

# Register your models here.
from cartinfo.models import CartInfo

admin.site.register(CartInfo)