import time
import copy 
from datetime import datetime
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.generics import DestroyAPIView
# Create your views here.
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from cartinfo.models import CartInfo
from cartinfo.serializers import CartInfoLisSerializer
from config.jsonrand import custom_response
from rest_framework.generics import ListAPIView, CreateAPIView
from booking.utilities import SlotTimeAvailCheck, checkBookablity
# from model.models import Service as Model
from cameraman.models import Service as Cameraman
# from studio.models import Service as Studio

class CartInfoList(ListAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    queryset = CartInfo.objects.all()
    serializer_class = CartInfoLisSerializer

    def list(self, request, *args, **kwargs):
        print('list ran')
        # queryset = self.filter_queryset(self.get_queryset())
        queryset = CartInfo.objects.filter(user=self.request.user)
        print(queryset,'here is queryset')
        serializer = CartInfoLisSerializer(queryset, many=True)
        print(serializer.data)
        success_list = []
        for data in serializer.data:
            
            if data['cameraman_service_id']!=None:
                service_id = data['cameraman_service_id']
                cameraman = Cameraman.objects.get(pk=service_id)
                cameraman_data = dict((key, value) for key, value in cameraman.__dict__.items() if not callable(value) and not key.startswith('__') and not key.startswith('_'))
                data['cameraman_service_id'] = cameraman_data
                success_list.append(data)
            
        return custom_response(success_list, '', message='Cameraman Timings', status_code=HTTP_200_OK)



SERVICE_TYPE=(
# ("MODELS","MODELS"),
("PHOTOGRAPHERS","PHOTOGRAPHERS"),
# ("EQUIPMENTS","EQUIPMENTS"),
# ("STUDIOS","STUDIOS"),
)   


    
from booking.utilities import cartsync,prior_checks_addtocart


class DeleteCart(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    # queryset = CartInfo.objects.all()
    serializer_class = CartInfoLisSerializer
    def get_queryset(self):
        print('self.kwargs.pk',self.request.user)
        queryset = CartInfo.objects.filter(user=self.request.user.pk)
        print(queryset,'here queryset')
        return queryset

    def post(self,request,*args,**kwargs):
        print(request.data['service_type'],'here it is')
        # obj=None
        # if request.data['service_type']=='MODELS':
        #     obj = self.get_queryset().get(service_type=request.data['service_type'],booking_date=request.data['booking_date'],start=request.data['start'],end=request.data['end'],model_service_id=request.data['model_service_id'])
        if request.data['service_type']=='PHOTOGRAPHERS':
            obj = self.get_queryset().get(service_type=request.data['service_type'],booking_date=request.data['booking_date'],start=request.data['start'],end=request.data['end'],cameraman_service_id=request.data['cameraman_service_id'])        
        # if request.data['service_type']=='STUDIOS':
        #     obj = self.get_queryset().get(service_type=request.data['service_type'],booking_date=request.data['booking_date'],start=request.data['start'],end=request.data['end'],studio_service_id=request.data['studio_service_id'])
        
        print(obj,'here should be the object')
        obj.delete()        
        return custom_response('','','deleted sucessfully',status_code=HTTP_200_OK)



    # def destroy(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     # if instance.is_default == True:
    #     #     return custom_response('',"Cannot delete default system category","Cannot delete default system category", status=status.HTTP_400_BAD_REQUEST)
    #     # self.perform_destroy(instance)
    #     return custom_response('',"Cannot delete default system category","Cannot delete default system category", status=HTTP_400_BAD_REQUEST)


        

def addToCart_Datacheck(data):
    print(data,'heredta')
    failed_obj = {}
    if data['service_type'] == 'MODELS' and 'model_service_id' not in data:
        print('nothere isit')
        data.update({'status':'Please provide valid models pk as model_service_id'})
        failed_obj = data
        
    if data['service_type'] == 'PHOTOGRAPHERS' and 'cameraman_service_id' not in data:
        data.update({'status':'Please provide valid Photographer pk as cameraman_service_id'})
        failed_obj = data

    if data['service_type'] == 'STUDIOS' and 'studio_service_id' not in data:
        data.update({'status':'Please provide valid Studio pk as studio_service_id'})
        failed_obj = data
    
    else:
        pass

    return failed_obj


class AddToCart(CreateAPIView):
    # 1 == 'This slot is not available for this service for this time'
    # 2 == 'This booking is not bookable for this time'


    '''
    check if they all have same service_id or tell them
    take all the inputs
    create availibitlity_time_slots
    check if any slot are overlapping
    create the time slots
    '''
    authentication_classes = [JSONWebTokenAuthentication]
    queryset = CartInfo.objects.all()
    serializer_class = CartInfoLisSerializer

    def create(self, request, *args, **kwargs):
        start_time = time.time()
        print('m i atleast there.')
        datas, user, failed_slots, non_avail_slots,non_bookable_slots,r_data_list,daily_slots = request.data, self.request.user,list(),list(),list(),list(), list()
        cart_items = CartInfo.objects.filter(user=user)
        booking_dates = []
        #TODO1 => Here we can 
        for data in datas:
            if data['booking_date'] not in booking_dates:
                if data['service_type']=='MODELS':
                    booking_dates.append({'date':data['booking_date'],'service_type':data['service_type'],'service_id':data['model_service_id']})
                if data['service_type']=='PHOTOGRAPHERS':
                    booking_dates.append({'date':data['booking_date'],'service_type':data['service_type'],'service_id':data['cameraman_service_id']})
                if data['service_type']=='STUDIOS':
                    booking_dates.append({'date':data['booking_date'],'service_type':data['service_type'],'service_id':data['studio_service_id']})



        if isinstance(datas,list):
            if not datas:
                return custom_response(datas,'Nothing got added to cart as these items were found already in there.','',status_code=HTTP_200_OK)

            for data in datas:
                data['start'],data['end'],data['date']=data['start'],data['end'],data['booking_date']
                addtocart_datacheck = addToCart_Datacheck(data)
                print(addtocart_datacheck,'addtocart_Checkup')
                if addtocart_datacheck:#if there are failed object
                    print('this wrong path for right obj')
                    failed_slots.append(data)
                else:
                    print('data going in addtocard slotitmecheck',data)
                    data['weekday']=datetime.strptime(data['date'],'%Y-%m-%d').date().weekday()+1
                    print('jere is ==>',data['weekday'])
                    isslotavailable = SlotTimeAvailCheck(data)
                    print(isslotavailable,'isslotavailable in addtocart')                            
                    if isslotavailable==True:
                        bookable = checkBookablity(data)
                        if not bookable['proceed']:
                            if bookable['state']=='approved':
                                status = 2
                                non_bookable_slots.append(data)
                            else:
                                status= 0
                    else:

                        non_avail_obj = data
                        non_avail_obj['status']=1
                        non_avail_slots.append(non_avail_obj)
                # if status==2:
                #     non_bookable_slots.append(data)
            if failed_slots and non_avail_slots and non_bookable_slots:
                return custom_response(failed_slots+non_avail_slots+non_bookable_slots,'provided data has missing "service_id" for given service_type report is given in data;few timeslots are not available report is given in data;few bookings are not available report is given in data;', message='',
                            status_code=HTTP_400_BAD_REQUEST)    

            if non_avail_slots and non_bookable_slots:
                return custom_response(non_avail_slots+non_bookable_slots,'few timeslots are not available report is given in data;few bookings are not available report is given in data;', message='',
                            status_code=HTTP_400_BAD_REQUEST)    
            if failed_slots:
                return custom_response(failed_slots,'few slots failed; report is given in data', message='few slots failed; report is given in data',
                            status_code=HTTP_400_BAD_REQUEST)       
            if non_avail_slots:
                return custom_response(non_avail_slots,'few time slots are not avilable; report is given in data', message='few time slots are not avilable; report is given in data',
                            status_code=HTTP_400_BAD_REQUEST)  
            if non_bookable_slots:
                return custom_response(non_bookable_slots,'few bookings are not bookable; report is given in data', message='',
                            status_code=HTTP_400_BAD_REQUEST)
                    
            else:   
                sucess_list=[]
                for data in datas:
                    data['status']=0
                    data['user']=self.request.user.pk
                    sucess_list.append(data)
                serializer = self.get_serializer(data=sucess_list, many=isinstance(request.data, list))
                if serializer.is_valid():
                    for item in booking_dates:
                        if item['service_type']=='MODELS':
                            CartInfo.objects.filter(user=user,booking_date=item['date'],model_service_id=item['service_id']).delete()
                        if item['service_type']=='PHOTOGRAPHERS':
                            CartInfo.objects.filter(user=user,booking_date=item['date'],cameraman_service_id=item['service_id']).delete()
                        if item['service_type']=='STUDIOS':
                            CartInfo.objects.filter(user=user,booking_date=item['date'],studio_service_id=item['service_id']).delete()
                    serializer.save()

                    queryset = CartInfo.objects.filter(user=self.request.user)
                    print(queryset,'here is queryset')
                    cart_infolis_serializer = CartInfoLisSerializer(queryset, many=True)
                    print(serializer.data)
                    cart_list = []
                    for data in cart_infolis_serializer.data:
                        if data['model_service_id']!=None:
                            service_id = data['model_service_id']
                            # model = Model.objects.get(pk=service_id)
                            model_data = dict((key, value) for key, value in model.__dict__.items() if not callable(value) and not key.startswith('__') and not key.startswith('_'))
                            data['model_service_id'] = model_data
                            cart_list.append(data)
                        if data['cameraman_service_id']!=None:
                            service_id = data['cameraman_service_id']
                            cameraman = Cameraman.objects.get(pk=service_id)
                            cameraman_data = dict((key, value) for key, value in cameraman.__dict__.items() if not callable(value) and not key.startswith('__') and not key.startswith('_'))
                            data['cameraman_service_id'] = cameraman_data
                            cart_list.append(data)
                        if data['studio_service_id']!=None:
                            service_id = data['studio_service_id']
                            studio = Studio.objects.get(pk=service_id)
                            studio_data = dict((key, value) for key, value in studio.__dict__.items() if not callable(value) and not key.startswith('__') and not key.startswith('_'))
                            data['studio_service_id'] = studio_data
                            cart_list.append(data)
                    end_time = time.time()
                    print('Total Time taken =>',end_time-start_time)
                    

                    return custom_response(cart_list, '',message='Added successfully to Cart!',status_code=HTTP_200_OK)
                else:
                    return custom_response(request.data, serializer.errors,
                    message=serializer.errors,
                    status_code=HTTP_400_BAD_REQUEST)
   
        if failed_slots and non_avail_slots and non_bookable_slots:
            return custom_response(failed_slots+non_avail_slots+non_bookable_slots,'provided data has missing "service_id" for given service_type report is given in data;few timeslots are not available report is given in data;few bookings are not available report is given in data;', message='',
                        status_code=HTTP_400_BAD_REQUEST)    

        if non_avail_slots and non_bookable_slots:
            return custom_response(non_avail_slots+non_bookable_slots,'few timeslots are not available report is given in data;few bookings are not available report is given in data;', message='',
                        status_code=HTTP_400_BAD_REQUEST)    
        if failed_slots:
            return custom_response(failed_slots,'few slots failed; report is given in data', message='',
                        status_code=HTTP_400_BAD_REQUEST)       
        if non_avail_slots:
            return custom_response(non_avail_slots,'few time slots are not avilable; report is given in data', message='',
                        status_code=HTTP_400_BAD_REQUEST)  
        if non_bookable_slots:
            return custom_response(non_bookable_slots,'few bookings are not bookable; report is given in data', message='',
                        status_code=HTTP_400_BAD_REQUEST)
                        
        else:
            print('m i running at all problem')
            serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
            print(serializer.is_valid(),'serializer.is_valid()')
            if serializer.is_valid:
                CartInfo.objects.filter(user=self.request.user.pk).delete()
                CartInfo.objects.all().delete()
                serializer.save()
                return custom_response(request.data, '',
                                message='Added successfully to Cart!',
                                status_code=HTTP_200_OK)
            else:
                return custom_response(request.data, serializer.errors,
                message=serializer.errors,
                status_code=HTTP_400_BAD_REQUEST)








# class AddToCart(CreateAPIView):
#     '''
#     check if they all have same service_id or tell them
#     take all the inputs
#     create availibitlity_time_slots
#     check if any slot are overlapping
#     create the time slots
#     '''
#     authentication_classes = [JSONWebTokenAuthentication]
#     queryset = CartInfo.objects.all()
#     serializer_class = CartInfoLisSerializer

#     def create(self, request, *args, **kwargs):
#         #put checks
#         #
#         user = self.request.user
#         datas = request.data
#         # service_type checkup
#         if isinstance(datas,list):
#             for data in datas:
#                 if data['service_type'] == 'MODELS' and 'model_service_id' not in data:
#                     return custom_response(data, 'Please provide valid models pk as model_service_id', message='Please provide valid models pk as model_service_id',
#                                         status_code=HTTP_400_BAD_REQUEST)
#                 if data['service_type'] == 'PHOTOGRAPHERS' and 'cameraman_service_id' not in data:
#                     return custom_response(data, 'Please provide valid Cameraman pk as cameraman_service_id',
#                                         message='Please provide valid models pk as cameraman_service_id',
#                                         status_code=HTTP_400_BAD_REQUEST)
#                 if data['service_type'] == 'STUDIOS' and 'studio_service_id' not in data:
#                     return custom_response(data, 'Please provide valid Studio pk as cameraman_service_id',
#                                         message='Please provide valid models pk as studio_service_id',
#                                         status_code=HTTP_400_BAD_REQUEST)
#                 slot = {'start':data['start'],'end':data['end'],'weekday':data['weekday'],'service_type':data['service_type'],'service_id':data['model_service_id'],'date':data['booking_date']}
#                 isSlotAvailable = SlotTimeAvailCheck(slot)
#                 print(isSlotAvailable,'slotavaiabl')
#                 if isSlotAvailable:
#                     bookable = checkBookablity(slot)
#                     if not bookable:
#                         failed_bookings=[]
#                         return custom_response(slot, 'This Booking is not available',message='This Booking is not available',status_code=HTTP_400_BAD_REQUEST)
#                     else:
#                         serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
#                         print(serializer.is_valid(),'serializer.is_valid()')
#                         if serializer.is_valid:
#                             CartInfo.objects.all().delete()
#                             serializer.save()
#                             return custom_response(request.data, '',
#                                             message='Added successfully to Cart!',
#                                             status_code=HTTP_200_OK)
#                         else:
#                             return custom_response(request.data, serializer.errors,
#                                                 message='something went wrong please contact Admin',
#                                                 status_code=HTTP_400_BAD_REQUEST)
#                             return custom_response(request.data, '',message='This Booking is available',status_code=HTTP_200_OK)      
#                 else:
#                     return custom_response(request.data, errors='This Service at this Time Slot is Unavailable at this date; please choose another date for this service', message='Failed slots data is provided', status_code=HTTP_400_BAD_REQUEST)
        
#         data=request.data
#         if isinstance(data,object):
#             if data['service_type'] == 'MODELS' and 'model_service_id' not in data:
#                 return custom_response(data, 'Please provide valid models pk as model_service_id', message='Please provide valid models pk as model_service_id',
#                                     status_code=HTTP_400_BAD_REQUEST)
#             if data['service_type'] == 'PHOTOGRAPHERS' and 'cameraman_service_id' not in data:
#                 return custom_response(data, 'Please provide valid Cameraman pk as cameraman_service_id',
#                                     message='Please provide valid models pk as cameraman_service_id',
#                                     status_code=HTTP_400_BAD_REQUEST)
#             if data['service_type'] == 'STUDIOS' and 'studio_service_id' not in data:
#                 return custom_response(data, 'Please provide valid Studio pk as cameraman_service_id',
#                                     message='Please provide valid models pk as studio_service_id',
#                                     status_code=HTTP_400_BAD_REQUEST)
#             slot = {'start':data['start'],'end':data['end'],'weekday':data['weekday'],'service_type':data['service_type'],'service_id':data['model_service_id'],'date':data['booking_date']}
#             isSlotAvailable = SlotTimeAvailCheck(slot)
#             print(isSlotAvailable,'slotavaiabl')
#             if isSlotAvailable:
#                 bookable = checkBookablity(slot)
#                 if not bookable:
#                     failed_bookings=[]
#                     return custom_response(slot, 'This Booking is not available',message='This Booking is not available',status_code=HTTP_400_BAD_REQUEST)
#                 else:
#                     serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
#                     print(serializer.is_valid(),'serializer.is_valid()')
#                     if serializer.is_valid:
#                         CartInfo.objects.all().delete()
#                         serializer.save()
#                         return custom_response(request.data, '',
#                                         message='Added successfully to Cart!',
#                                         status_code=HTTP_200_OK)
#                     else:
#                         return custom_response(request.data, serializer.errors,
#                                             message='something went wrong please contact Admin',
#                                             status_code=HTTP_400_BAD_REQUEST)
#                         return custom_response(request.data, '',message='This Booking is available',status_code=HTTP_200_OK)      
#             else:
#                 return custom_response(request.data, errors='This Service at this Time Slot is Unavailable at this date; please choose another date for this service', message='Failed slots data is provided', status_code=HTTP_400_BAD_REQUEST)


