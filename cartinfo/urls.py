from django.conf.urls import url
from cartinfo.views import CartInfoList, AddToCart, DeleteCart

app_name = 'cartinfo'

urlpatterns = [
    url(r'^add_to_cart$', AddToCart.as_view()),
    url(r'^delete$', DeleteCart.as_view()),
    url(r'^detail$', CartInfoList.as_view()),
]
