from django.contrib.auth import get_user_model
from django.db import models
from djmoney.models.fields import MoneyField
# from model.models import Service as ModelService
from cameraman.models import Service as CameramanService
# from studio.models import Service as StudioService
from django_mysql.models import JSONField
from product.models import Product
# Create your models here.


User = get_user_model()

SERVICE_TYPE=(
("MODELS","MODELS"),
("PHOTOGRAPHERS","PHOTOGRAPHERS"),
("EQUIPMENTS","EQUIPMENTS"),
("STUDIOS","STUDIOS"),
)

class CartInfo(models.Model):
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    #CHECK IF THIS DOES NOT DELETE THE CART IF PRODUCT IS DELETED
    products = models.ManyToManyField(Product,blank=True)
  
    def __str__(self):
        return '{cart_id} for {user}'.format(user=self.user,cart_id=self.pk)


    