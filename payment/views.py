"""
Adds simple form view, which communicates with Braintree.

There are four steps to finally process a transaction:

1. Create a client token (views.py)
2. Send it to Braintree (js)
3. Receive a payment nonce from Braintree (js)
4. Send transaction details and payment nonce to Braintree (views.py)

"""
import requests 
import braintree
import stripe
import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from config import settings
from config.settings import gateway
from . import forms
from payment.payment_utilities import create_submerchant
from config.jsonrand import custom_response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from accounts.models import Account
from rest_framework.views import APIView
from django.http import JsonResponse
# from vendor.models import Vendor_Info

from django.views.generic import TemplateView
class StandardOnboard(TemplateView):
    template_name="payment/stripe_standard_onboard.html"
    # model = Book
    # def get(self,request,*args,**kwargs):
    #     # return render(request, template_name=)
    #     return custom_response('','','',status_code=HTTP_200_OK)


def afterredirect(request):
    '''
    This one is where user will automatically be transferred once successfully Connected with our stripe account.
        
    
    '''


    if request.method=='GET':
        context = {}
        # print(code,'here is auth_code')
        # vendors = Vendor_Info.objects.all()
        # vendors_username = [vendor.user.username for vendor in vendors]
        # context = {'vendors_username':vendors_username}
        '''
        import requests
        URL = "127.0.0.1:8000/api/bookings/all?model_service=43&date=2019-08-19"
        headers = {'Content-Type':'application/json','Authorization':'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjozOSwidXNlcm5hbWUiOiJkc2luZ2guMThmZWIiLCJleHAiOjE1Njg3Mjc0MjcsImVtYWlsIjoiZHNpbmdoLjE4ZmViQGdtYWlsLmNvbSIsIm9yaWdfaWF0IjoxNTY4NzIxNDI3fQ.r0RTYGtxgbn2arSzUzaP86k4EZOwncTHXsPPOjGPw-Y'}
        r = requests.post(URL, headers=headers)        
        '''

        return render(request,template_name="payment/afterredirect.html",context=context)
    else:
        print(request.POST,'request.POST')
        cvu = request.POST.get('current_vendor_username')
        from django.contrib.auth.models import User
        user = User.objects.get(username=cvu)
        vendor = Vendor_Info.objects.get(user=user)
                    
        URL = "https://connect.stripe.com/oauth/token"
        client_secret="sk_test_wfyoe2hm166zFAA88bGHFEOQ003JA44P1T" 
        code = request.GET.get('code')
        # code='ac_FpQwn1KqIYmpfht4d3VT7M6yeN5FKpvW'#auth_code
        grant_type="authorization_code"
        headers = {'Content-Type':'application/json','Authorization':'Bearer sk_test_wfyoe2hm166zFAA88bGHFEOQ003JA44P1T'}
        print(code,'code')
        payload = {
        "grant_type":"authorization_code",
        "code":code
        }
        r = requests.post(URL, headers=headers,params=payload)
        r = r.json()
        stripe_user_id = r.get('stripe_user_id')
        vendor.strip_custom_ac_id=stripe_user_id
        vendor.save()
        return HttpResponse('<h2>Your Stripe Connect Ac id with us is =></h2><h4>'+vendor.strip_custom_ac_id+'</h4><p>Onboarding successfully done; Completed</p>')


def standardOnboard(request):
    context={}
    return render(request,template_name="payment/stripe_standard_onboard.html",context=context)


class OnboardingProfile(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    # queryset = CartInfo.objects.all()
    # serializer_class = CartInfoLisSerializer

    def get(self,request,*args,**kwargs):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[-1].strip()
        else:
            ip = request.META.get('REMOTE_ADDR')
        print(ip,'here is ip')
        '''
            this one onboards the connect user
            takes atleast country + (few more information depending on type of the account) but better we provide maximum things upfront
            returns ac_id and few more details they can be saved against customer
        '''
        vendor_obj = Vendor_Info.objects.get(user=self.request.user)
        import stripe
        if vendor_obj.strip_custom_ac_id:
            stripe.api_key = "sk_test_wfyoe2hm166zFAA88bGHFEOQ003JA44P1T"
            ac_detail = stripe.Account.retrieve(vendor_obj.strip_custom_ac_id)
            # return custom_response('','user already have stripe connect Account; id is given back in data','User already have stripe connect ac',status_code=HTTP_400_BAD_REQUEST)
            
            # ac_detail['inserted_ip']=ip
            return JsonResponse(ac_detail)
        else:
            return custom_response('','Account does not have stripe_connect_id; Please create one first','Account does not have stripe_connect_id; Please create one first',status_code=HTTP_400_BAD_REQUEST)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

    
class OnboardConnectUser(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    # queryset = CartInfo.objects.all()
    # serializer_class = CartInfoLisSerializer
    def post(self,request,*args,**kwargs):
        user=self.request.user
        '''
            get user from the token in the header
            get the vendor_uid and compare

            this one onboards the connect user
            takes atleast country + (few more information depending on type of the account) but better we provide maximum things upfront
            returns ac_id and few more details they can be saved against customer
            after block it for everyone but sarma
        '''
        vendor_obj = Vendor_Info.objects.get(user=user)
        first_name,last_name,day,month,year=self.request.user.first_name,self.request.user.last_name,vendor_obj.date_of_birth.day,vendor_obj.date_of_birth.month,vendor_obj.date_of_birth.year
        vendor_desc=vendor_obj.description
        if first_name==None or last_name==None or day==None or month==None or year==None or vendor_desc==None:
            return custom_response('','Vendor profile at this point must fill correct details for => first_name, last_name, dob, product_description','Vendor profile at this point must fill correct details for => first_name, last_name, dob, product_description',status_code=HTTP_400_BAD_REQUEST)
        
        if not vendor_obj.strip_custom_ac_id:
            ssn_last_4=None
            tax_id=None
            business_type=None
            if vendor_obj.business_type=='SOLO':
                ssn = vendor_obj.ssn
                ssn_last_4 = ssn.replace('-','')[-4:]
                print('ssn_last_4',ssn_last_4)
                business_type="individual"

            else:
                tax_id = vendor_obj.tax_id
                business_type='company'

            print(tax_id,ssn_last_4,'here it is',business_type)
            if tax_id:
                return custom_response('','for tax_id it is not yet implimented.','for tax_id it is not yet implimented.',status_code=HTTP_400_BAD_REQUEST)
            stripe.api_key = 'sk_test_wfyoe2hm166zFAA88bGHFEOQ003JA44P1T'
            account = stripe.Account.create(
                country='US',
                type='custom',
                requested_capabilities=['transfers'],
                business_type=business_type,
                individual={'first_name':first_name,'last_name':last_name,'ssn_last_4':ssn_last_4,'dob':{'day':day,'month':month,'year':year}},
                business_profile={'product_description':vendor_desc}        
            )
            print('stripe returned object after creation of connect ac',account)
            ac_id = account['id']
            vendor_obj.strip_custom_ac_id=ac_id
            vendor_obj.save()
            return custom_response('','','Strip connect onboarding successfully done; You can now check the status',status_code=HTTP_200_OK)

        else:
            return custom_response('','user already have stripe connect Account; id is given back in data','User already have stripe connect ac',status_code=HTTP_400_BAD_REQUEST)

                # stripe.api_key = 'sk_test_wfyoe2hm166zFAA88bGHFEOQ003JA44P1T'
                # account = stripe.Account.create(
                #     country='US',
                #     type='custom',
                #     requested_capabilities=['transfers'],
                #     business_type='individual',
                #     individual={'first_name':first_name,'last_name':last_name,'ssn_last_4':ssn_last_4,'dob':{'day':day,'month':month,'year':year}},
                #     ## tos_acceptance.date => not for india
                #     ## tos_acceptance.ip => not for india
                #     # business_profile[url]
                #     # business_profile=''
                #     business_profile={'product_description':vendor_desc}
            
                # )
                # print(account,'here is returned account object after crate')
                # if account['id']:
                #     ac_id = account['id']
                #     vendor_obj.strip_custom_ac_id=ac_id
                #     vendor_obj.save()
                #     return custom_response(ac_id,'','Onboarding Sucessfull',status_code=HTTP_200_OK)
                # else:
                #     print(account,'returned ac obj when failed!')
                #     return custom_response('','error encountered while onboarding','Onboarding Unsuccesfull',status_code=HTTP_400_BAD_REQUEST)

class UpdateConnectProfile(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    # queryset = CartInfo.objects.all()
    # serializer_class = CartInfoLisSerializer
    
    def post(self,request,*args,**kwargs):
        '''
        '''
        user = self.request.user
        if user.pk!=9:
            return custom_response('',errors='This only works if user_id=9 as we do not have it open; as we want to make sure that no non usa ip is being register for stripe ac',message='This only works if user_id=9 as we do not have it open; as we want to make sure that no non usa ip is being register for stripe ac',status_code=HTTP_400_BAD_REQUEST)
        vendor_obj = Vendor_Info.objects.get(user=user)
        # vendor_obj = Vendor_Info.objects.get(pk=vendor.pk)

        # if self.request.user.pk==9:
        if vendor_obj.strip_custom_ac_id:
            stripe.api_key = 'sk_test_wfyoe2hm166zFAA88bGHFEOQ003JA44P1T'
           
            import time
            stripe_obj = stripe.Account.modify(
            vendor_obj.strip_custom_ac_id,
            tos_acceptance={
                'date': int(time.time()),
                'ip': get_client_ip(request), # Depends on what web framework you're using
            },
            # external_account={
            #     "object": "bank_account",
            #     "account_holder_name": "Jane Austen",
            #     "account_number":"000123456789",
            #     "account_holder_type": "individual",
            #     "bank_name": "STRIPE TEST BANK",
            #     "country": "US",
            #     "currency": "usd",
            #     "routing_number": "110000000",
            # }
            )
            print(stripe_obj,'here is returned account object after creation')
                # ac_id = account['id']
                # account_obj.strip_custom_ac_id=ac_id
                # account_obj.save()
            return custom_response(vendor_obj.strip_custom_ac_id,'','Stripe Connect account successfully updated; you can check the updated status',status_code=HTTP_200_OK)
       
        else:
            return custom_response('','user does not have any connect stripe ac; please create one; id is given back in data','User already have stripe connect ac',status_code=HTTP_400_BAD_REQUEST)

# Using Django
def handle_webhook_obj(obj):
    vendor = Vendor_Info.objects.get(pk=24)
    'here we can do anything that we want when the event happens'
    vendor.save()

@csrf_exempt
def acceptConnectUpdateWebhook(request):
  payload = request.body
  event = None
  stripe.api_key = 'sk_test_wfyoe2hm166zFAA88bGHFEOQ003JA44P1T'
  try:
    event = stripe.Event.construct_from(
      json.loads(payload), stripe.api_key
    )
  except ValueError as e:
    # Invalid payload
    return HttpResponse(status=400)

  # Handle the event
  if event.type == 'account.updated':
    returned_obj = event.data # 
    handle_webhook_obj(returned_obj)
  elif event.type == 'account.external_account.updated':
    returned_obj = event.data #  
    handle_webhook_obj(returned_obj)

  elif event.type == 'account.external_account.created':
    returned_obj = event.data #  
    handle_webhook_obj(returned_obj)
    
  # ... handle other event types
  else:
    # Unexpected event type
    return HttpResponse(status=400)

  return HttpResponse(status=200)


