import braintree
from config.settings import gateway

def create_submerchant():
    merchant_account_params = {
    'individual': {
        'first_name': "Jane",
        'last_name': "Doe",
        'email': "jane@14ladders.com",
        'phone': "5553334444",
        'date_of_birth': "1981-11-19",
        'ssn': "456-45-4567",
        'address': {
            'street_address': "111 Main St",
            'locality': "Chicago",
            'region': "IL",
            'postal_code': "60622"
        }
    },
    'business': {
        'legal_name': "Jane's Ladders",
        'dba_name': "Jane's Ladders",
        'tax_id': "98-7654321",
        'address': {
            'street_address': "111 Main St",
            'locality': "Chicago",
            'region': "IL",
            'postal_code': "60622"
        }
    },
    'funding': {
        'descriptor': "Blue Ladders",
        'destination': braintree.MerchantAccount.FundingDestination.Bank,
        'email': "funding@blueladders.com",
        'mobile_phone': "5555555555",
        'account_number': "1123581321",
        'routing_number': "071101307",
    },
    "tos_accepted": True,
    "master_merchant_account_id": "wt63whjk6ytd2b2q",
    "id": "blue_ladders_store"
}
    result = gateway.merchant_account.create(merchant_account_params)
    return result