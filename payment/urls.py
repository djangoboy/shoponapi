from django.conf.urls import url

# from .views import (
#     payment_view,checkout,test_js,create_custom_ac,check_profile_status,OnboardConnectUser)
from .views import OnboardConnectUser, OnboardingProfile, UpdateConnectProfile, acceptConnectUpdateWebhook, standardOnboard, afterredirect

app_name = 'payment'

urlpatterns = [
    #Onboarding Sample Urls
    url(r'^stripe/standard/onboard/$', standardOnboard),
    url(r'^stripe/standard/afterredirect/$', afterredirect),
    url(r'^stripe/retrieve_onboarding_profile$', OnboardingProfile.as_view()),
    url(r'^stripe/update_connect_profile$', UpdateConnectProfile.as_view()),
    url(r'^stripe/update_connect_ac_webhook/$', acceptConnectUpdateWebhook),
 
 
 
 
 
 
 






 
 
    # url(r'^stripe/express/onboard_connect_user$', OnboardConnectUser.as_view()),
    # url(r'^stripe/create_custom_ac$', create_custom_ac),
    # url(r'^stripe/check_profile_status$', check_profile_status),
    # url(r'^makepayment$', payment_view),
    # url(r'^checkout$', checkout,name="checkout"),
    # url(r'^test_js$', test_js,name="test_js"),


]
