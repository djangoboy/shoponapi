import copy
import time
from datetime import datetime
from django.contrib.auth import get_user_model
from booking.models import Booking
from django.db.models import Q
# from cameraman.models import service_pricing as CameramanServicePricing
# from model.models import Service as Model
# from model.models import service_WeekdayOpeningPeriod as Model_WeekdayOpeningPeriod, service_WeekdayOpeningExceptionPeriod as Model_WeekdayOpeningExceptionPeriod

from cameraman.models import Service as Cameraman
from cameraman.models import service_WeekdayOpeningPeriod as Cameraman_WeekdayOpeningPeriod, service_WeekdayOpeningExceptionPeriod as Cameraman_WeekdayOpeningExceptionPeriod

# from studio.models import Service as Studio
# from studio.models import service_WeekdayOpeningPeriod as Studio_WeekdayOpeningPeriod, service_WeekdayOpeningExceptionPeriod as Studio_WeekdayOpeningExceptionPeriod



'''
from booking.models import Booking
from model.models import Service as Model
from cameraman.models import Service as Cameraman
from studio.models import Service as Studio

bookings= Booking.objects.all()
for booking in bookings:
    if booking.service_type=='MODELS':
        mb = booking.model_service
        booking.vendor = mb.business_location.vendor_name
        booking.save()
    if booking.service_type=='PHOTOGRAPHERS':
        mb = booking.cameraman_service
        booking.vendor = mb.business_location.vendor_name
        booking.save()
    if booking.service_type=='STUDIOS':
        mb = booking.studio_service
        booking.vendor = mb.business_location.vendor_name
        booking.save()

'''
def pastDateCheckup(data):
    s,e=data['start'],data['end']
    if e<s:
        return True
    else:
        return False


#CHANGE2
def SlotTimeAvailCheck(slot):
    t1 = time.time()
    '''
    returns slot 
    '''
    print('m i called.')
    s,e,w,st=slot['start'],slot['end'],slot['weekday'],slot['service_type']
    
    status = bool()
    date = slot.get('date',None)
    exception_slots = None
    if st=='MODELS':
        si=slot['model_service_id']
        service = Model.objects.get(pk=si)
        exception_slots = Model_WeekdayOpeningExceptionPeriod.objects.filter(service_id=si,exc_date=date,price_type=service.s_price_type)
        if service.s_price_type=='HOURLY':
            available_tslots = Model_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w,price_type=service.s_price_type).order_by('start').order_by('end')
        if service.s_price_type=='DAILY':
            # w=None
            available_tslots = Model_WeekdayOpeningPeriod.objects.filter(service_id=si,price_type=service.s_price_type).order_by('start').order_by('end')
    if st=='PHOTOGRAPHERS':
        si=slot['cameraman_service_id']
        service = Cameraman.objects.get(pk=si)
        exception_slots = Cameraman_WeekdayOpeningExceptionPeriod.objects.filter(service_id=si,exc_date=date,price_type=service.s_price_type)            
        if service.s_price_type=='HOURLY':
            available_tslots = Cameraman_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w,price_type=service.s_price_type).order_by('start').order_by('end')     

        if service.s_price_type=='DAILY':
            available_tslots = Cameraman_WeekdayOpeningPeriod.objects.filter(service_id=si,price_type=service.s_price_type).order_by('start').order_by('end')     
    if st=='STUDIOS':
        si=slot['studio_service_id']
        service = Studio.objects.get(pk=si)
        exception_slots = Studio_WeekdayOpeningExceptionPeriod.objects.filter(service_id=si,exc_date=date,price_type=service.s_price_type)
        if service.s_price_type=='HOURLY':
            available_tslots = Studio_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w,price_type=service.s_price_type).order_by('start').order_by('end')
        if service.s_price_type=='DAILY':
            available_tslots = Studio_WeekdayOpeningPeriod.objects.filter(service_id=si,price_type=service.s_price_type).order_by('start').order_by('end')
    if exception_slots:
        available_tslots=exception_slots
    else:
        pass
    if service.s_price_type == 'HOURLY':
        print('without exception')
        print('weekday=>',w)
        print(available_tslots,'<=here is the avaialbity of the slots')
        if len(available_tslots)>0:
            available_slots = list(available_tslots)
            for timeslot in available_slots:
                if timeslot.start <= datetime.strptime(s, '%H:%M:%S').time() <= timeslot.end and timeslot.start <= datetime.strptime(e, '%H:%M:%S').time() <= timeslot.end:
                    status = True
                    break
                else:
                    status = False
            t2 = time.time()
            print('total time taken in slottimeavailable in Hourly',t2-t1) 
            return status 
        else:
            status = False
            return status
    if service.s_price_type == 'DAILY':
        if len(available_tslots)==1:
            if available_tslots[0].start <= datetime.strptime(s, '%H:%M:%S').time() <= available_tslots[0].end and available_tslots[0].start <= datetime.strptime(e, '%H:%M:%S').time() <= available_tslots[0].end:
                status=True
            else:
                status=False
        return status

    
    
    
    
    
    
    
    
    
    
    
    
    
    # if st=='PHOTOGRAPHERS':
    #     si=slot['cameraman_service_id']
    #     print(si,'here is si incase it breaks') 
    #     service = Cameraman.objects.get(pk=si)
    #     sp = service.s_price_type        # sp = CameramanServicePricing.objects.get(service_id=service)
    #     if sp=='HOURLY':
    #         available_tslots = Cameraman_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w,price_type=service.s_price_type).order_by('start').order_by('end')
    #         #remove the duplicate slots very quickly
    #         print(available_tslots,'availavailable_tslotsable_slots')
    #         if len(available_tslots)>0:
    #             available_slots = list(available_tslots)
    #             for timeslot in available_slots:
    #                 if timeslot.start <= datetime.strptime(s, '%H:%M:%S').time() <= timeslot.end and timeslot.start <= datetime.strptime(e, '%H:%M:%S').time() <= timeslot.end:
    #                     status = True
    #                     break
    #                 else:
    #                     status = False
    #             return status 
    #         else:
    #             status = False
    #             return status
    #     if sp == 'DAILY':
    #         #means not implimented..
    #         status = 11
    #         return status
    #     # if sp.price_type == 'DAILY':
    #         available_tslots = Cameraman_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w,price_type='DAILY').order_by('start').order_by('end')
    #         #remove the duplicate slots very quickly
    #         print(available_tslots,'availavailable_tslotsable_slots')
    #         if len(available_tslots)>0:
    #             available_slots = list(available_tslots)
    #             for i in available_slots:
    #                 if i.weekday==w:
    #                     status = True
    #                     break
    #                 else:
    #                     status = False
    #             return status 
    #         else:
    #             status = False
    #             return status
    
    # if st=='STUDIOS':
    #         si=slot['studio_service_id']
    #         service = Studio.objects.get(pk=si)
    #         sp = service.s_price_type
    #         if sp=='HOURLY':
    #             available_tslots = Studio_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w,price_type=service.s_price_type).order_by('start').order_by('end')
    #             print(available_tslots,'availavailable_tslotsable_slots')
    #             if len(available_tslots)>0:
    #                 available_slots = list(available_tslots)
    #                 for timeslot in available_slots:
    #                     if timeslot.start <= datetime.strptime(s, '%H:%M:%S').time() <= timeslot.end and timeslot.start <= datetime.strptime(e, '%H:%M:%S').time() <= timeslot.end:
    #                         status = True
    #                         break
    #                     else:
    #                         status = False
    #                 return status 
    #             else:
    #                 status = False
    #                 return status
    #         if sp == 'DAILY':
    #             #means not implimented..
    #             status = 11
    #             return status
    #         # if sp.price_type == 'DAILY':
    #             available_tslots = Studio_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w,price_type='DAILY').order_by('start').order_by('end')
    #             #remove the duplicate slots very quickly
    #             print(available_tslots,'availavailable_tslotsable_slots')
    #             if len(available_tslots)>0:
    #                 available_slots = list(available_tslots)
    #                 for i in available_slots:
    #                     if i.weekday==w:
    #                         status = True
    #                         break
    #                     else:
    #                         status = False
    #                 return status 
    #             else:
    #                 status = False
    #                 return status

    # else:
    #     #if exc_date is there 
    #     #going through this
    #     if st=='MODELS':
    #         si=slot['model_service_id']
    #         service = Model.objects.get(pk=si)
    #         sp = service.s_price_type
    #         # sp = ModelServicePricing.objects.get(service_id=service)
    #         print(sp,'spherere')
    #         if sp == 'HOURLY':
    #             print('model has date in the exception table')
    #             #check if the exist
    #             # print(Model_WeekdayOpeningExceptionPeriod.objects.filter(service_id=si,exc_date=date,price_type=service.s_price_type).order_by('start').order_by('end'),'herererehfer')
    #             available_tslots = Model_WeekdayOpeningExceptionPeriod.objects.filter(service_id=si,exc_date=date,price_type=service.s_price_type).order_by('start').order_by('end')
    #             # print('jere are avaialble slots frm heart of exception',available_slots)                
    #             if len(available_tslots)>0:
    #                 available_slots = list(available_tslots)
    #                 for timeslot in available_slots:
    #                     if timeslot.start <= datetime.strptime(s, '%H:%M:%S').time() <= timeslot.end and timeslot.start <= datetime.strptime(e, '%H:%M:%S').time() <= timeslot.end:
    #                         status = True
    #                         break
    #                     else:
    #                         status = False
    #                 return status 
    #             else:
    #                 status = False
    #                 return status
    #         #as long as daily is not implimented #no instructions from client yet
    #         if sp == 'DAILY':
    #             #means not implimented..
    #             status = 11
    #             return status
    #         # if sp.price_type == 'DAILY':
    #             available_tslots = Model_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w,price_type='DAILY').order_by('start').order_by('end')
    #             #remove the duplicate slots very quickly
    #             print(available_tslots,'daily-availavailable_tslotsable_slots')
    #             if len(available_tslots)>0:
    #                 available_slots = list(available_tslots)
    #                 for i in available_slots:
    #                     if i.weekday==w:
    #                         status = True
    #                         break
    #                     else:
    #                         status = False
    #                 return status 
    #             else:
    #                 status = False
    #                 return status

    #     if st=='PHOTOGRAPHERS':
    #         si=slot['cameraman_service_id']
    #         print(si,'here is si incase it breaks') 
    #         service = Cameraman.objects.get(pk=si)
    #         sp = service.s_price_type        # sp = CameramanServicePricing.objects.get(service_id=service)
    #         if sp=='HOURLY':
    #             available_tslots = Cameraman_WeekdayOpeningExceptionPeriod.objects.filter(service_id=si,weekday=w,price_type=service.s_price_type).order_by('start').order_by('end')
    #             #remove the duplicate slots very quickly
    #             print(available_tslots,'availavailable_tslotsable_slots')
    #             if len(available_tslots)>0:
    #                 available_slots = list(available_tslots)
    #                 for timeslot in available_slots:
    #                     if timeslot.start <= datetime.strptime(s, '%H:%M:%S').time() <= timeslot.end and timeslot.start <= datetime.strptime(e, '%H:%M:%S').time() <= timeslot.end:
    #                         status = True
    #                         break
    #                     else:
    #                         status = False
    #                 return status 
    #             else:
    #                 status = False
    #                 return status
    #         if sp == 'DAILY':
    #             #means not implimented..
    #             status = 11
    #             return status
    #         # if sp.price_type == 'DAILY':
    #             available_tslots = Cameraman_WeekdayOpeningExceptionPeriod.objects.filter(service_id=si,weekday=w,price_type='DAILY').order_by('start').order_by('end')
    #             #remove the duplicate slots very quickly
    #             print(available_tslots,'availavailable_tslotsable_slots')
    #             if len(available_tslots)>0:
    #                 available_slots = list(available_tslots)
    #                 for i in available_slots:
    #                     if i.weekday==w:
    #                         status = True
    #                         break
    #                     else:
    #                         status = False
    #                 return status 
    #             else:
    #                 status = False
    #                 return status
        
    #     if st=='STUDIOS':
    #         si=slot['studio_service_id']
    #         service = Studio.objects.get(pk=si)
    #         sp = service.s_price_type
    #         if sp=='HOURLY':
    #             available_tslots = Studio_WeekdayOpeningExceptionPeriod.objects.filter(service_id=si,weekday=w,price_type=service.s_price_type).order_by('start').order_by('end')
    #             print(available_tslots,'availavailable_tslotsable_slots')
    #             if len(available_tslots)>0:
    #                 available_slots = list(available_tslots)
    #                 for timeslot in available_slots:
    #                     if timeslot.start <= datetime.strptime(s, '%H:%M:%S').time() <= timeslot.end and timeslot.start <= datetime.strptime(e, '%H:%M:%S').time() <= timeslot.end:
    #                         status = True
    #                         break
    #                     else:
    #                         status = False
    #                 return status 
    #             else:
    #                 status = False
    #                 return status
    #         if sp == 'DAILY':
    #             #means not implimented..
    #             status = 11
    #             return status
    #         # if sp.price_type == 'DAILY':
    #             available_tslots = Studio_WeekdayOpeningExceptionPeriod.objects.filter(service_id=si,weekday=w,price_type='DAILY').order_by('start').order_by('end')
    #             #remove the duplicate slots very quickly
    #             print(available_tslots,'availavailable_tslotsable_slots')
    #             if len(available_tslots)>0:
    #                 available_slots = list(available_tslots)
    #                 for i in available_slots:
    #                     if i.weekday==w:
    #                         status = True
    #                         break
    #                     else:
    #                         status = False
    #                 return status 
    #             else:
    #                 status = False
    #                 return status

def doesBookingAlreadyExist(data):
    s_id,s_type,s,e,d=data['model_service_id'],data['service_type'],data['start'],data['end'],data['date']
    booking = Booking.objects.filter(model_service_id=s_id,date=d,start=s,end=e)
    if booking:
        return True
    else:
        return False

def bookingnotOverLapping(data):
    '''
    checks if booking is over lapping any existing booking'''
    status = bool()
    s,e,d,si,st=data['start'],data['end'],data['date'],data['model_service_id'],data['service_type']
    booked = Booking.objects.filter(model_service=si,date=d,state='booking-request').order_by('start').order_by('end')
    print(booked,'booking-request',len(booked) >= 2)
    booked = list(booked)
    if len(booked) is 1:
        status = False
    if len(booked) >= 2:
        print('is it going through this')
        for i in range(len(booked)-1):
            fb_slot,sb_slot = booked[i],booked[i+1]
            print(fb_slot.end >=datetime.strptime(s, '%H:%M:%S').time(),fb_slot.end,s,fb_slot.end==datetime.strptime(s, '%H:%M:%S').time())
            print(datetime.strptime(s, '%H:%M:%S').time() > sb_slot.start,sb_slot.start,s)
            print(fb_slot.end <= datetime.strptime(e, '%H:%M:%S').time())
            print(datetime.strptime(e, '%H:%M:%S').time() < sb_slot.start,sb_slot.end,e)
            if fb_slot.end >= datetime.strptime(s, '%H:%M:%S').time() and datetime.strptime(e, '%H:%M:%S').time()<=sb_slot.start:
                status = False
                break
            else:
                print('do i fall in here...')
                status = True
    else:
        status = False
    return status

#CHANGE3
def checkBookablity(slot):
    s,e,d,st=slot['start'],slot['end'],slot['date'],slot['service_type']
    res = {}
    model_si, cameraman_si,studio_si=None,None,None
    # status = bool()
    if st=='MODELS':
        model_si=int(slot['model_service_id'])
        service = Model.objects.get(pk=model_si)
    if st=='PHOTOGRAPHERS':
        cameraman_si=int(slot['cameraman_service_id'])
        service = Cameraman.objects.get(pk=cameraman_si)
    if st=='STUDIOS':
        studio_si=int(slot['studio_service_id'])
        service = Studio.objects.get(pk=studio_si)
    # if service.s_price_type=='HOURLY':
        #state filter will change in future 
        #fix model_service below
    
    try:
        hourly_bookings = Booking.objects.filter(model_service=model_si,cameraman_service=cameraman_si,studio_service=studio_si,date=d,booking_type='HOURLY').filter(Q(state='approved')).order_by('start').order_by('end')
    except Booking.DoesNotExist:
        hourly_bookings =None
    if not hourly_bookings:
        hourly_bookings=None
    try:
        daily_bookings = Booking.objects.filter(model_service=model_si,cameraman_service=cameraman_si,studio_service=studio_si,date=d,booking_type='DAILY').filter(Q(state='approved'))
    except Booking.DoesNotExist:
        daily_bookings=None
    # finally:
        #all_bookings = Booking.objects.filter(model_service=si,date=d,booking_type='HOURLY').order_by('start').order_by('end').filter(Q(state='approved'))
    # hourly_bookings = list(hourly_bookings)
    if hourly_bookings and service.s_price_type=='HOURLY':
        print('m i herere')
        for booking in hourly_bookings:
            if booking.start <= datetime.strptime(s, '%H:%M:%S').time() < booking.end or booking.start < datetime.strptime(e, '%H:%M:%S').time() <= booking.end:
                res['state'] = booking.state
                res['proceed'] = False
                break
            else:
                res['proceed'] = True
    elif hourly_bookings and service.s_price_type=='DAILY':
        res['proceed']=False
        #what do you want here..
        # res.state='HOURLY_BOOKED'
        res['state']=hourly_bookings[0].state
        # res.state='approved'
    elif daily_bookings and service.s_price_type=='DAILY':
        res['proceed']=False
        res['state']=daily_bookings[0].state

    elif daily_bookings and service.s_price_type=='HOURLY':
        res['proceed']=False
        res['state']=daily_bookings[0].state
                
    else:
        res['proceed'] = True

    return res




    # if service.s_price_type=='DAILY':
    #         #which means not yet implimented
    #         all_bookings = Booking.objects.filter(model_service=si,date=d,booking_type='DAILY').order_by('start').order_by('end').filter(Q(state='booking-request') | Q(state='approved'))
    #         #
    #         all_bookings = list(all_bookings)
    #         if all_bookings:
    #             res['state'] = booking.state
    #             res['proceed'] = False
    #         else:
    #             # status = True
    #             res['state'] = booking.state
    #             res['proceed'] = True
    #         return res
        



























    # if st=='PHOTOGRAPHERS':
    #     si=slot['cameraman_service_id']
    #     print(si,'photograph_si')
    #     sp = CameramanServicePricing.objects.get(service_id=si)
    #     if sp.price_type=='HOURLY':
    #         all_bookings = Booking.objects.filter(cameraman_service=si,date=d,booking_type='HOURLY').filter(Q(state='booking-request') | Q(state='approved')).order_by('start').order_by('end')

    #         # all_bookings = Booking.objects.filter(cameraman_service=si,date=d).filter(state='booking-request').order_by('start').order_by('end')
    #         all_bookings = list(all_bookings)
    #         if all_bookings:
    #             print('m i herere')
    #             for booking in all_bookings:
    #                 print(booking.start,booking.end,s,e)
    #                 if booking.start <= datetime.strptime(s, '%H:%M:%S').time() < booking.end or booking.start < datetime.strptime(e, '%H:%M:%S').time() <= booking.end:
    #                     status = booking.state
    #                     break
    #                 else:
    #                     status = True
    #         else:
    #             status = True
    #         return status
    #     if sp.price_type=='DAILY':
    #         status=11
    #         return status
    #         # all_bookings = Booking.objects.filter(model_service=si,date=d,booking_type='DAILY').filter(state='booking-request').order_by('start').order_by('end')
    #         # #
    #         # all_bookings = list(all_bookings)
    #         # #all_bookings is more then one which should not be the case
    #         # if all_bookings:
    #         #     status = False
    #         # else:
    #         #     status = True
    #         # return status
        
    # if st=='STUDIOS':
    #     si=slot['studio_service_id']
    #     sp = StudioServicePricing.objects.get(service_id=si)
    #     if sp.price_type=='HOURLY':
    #         all_bookings = Booking.objects.filter(studio_service=si,date=d,booking_type='HOURLY').filter(Q(state='booking-request') | Q(state='approved')).order_by('start').order_by('end')
    #         # all_bookings = Booking.objects.filter(studio_service=si,date=d).filter(state='booking-request').order_by('start').order_by('end')
    #         all_bookings = list(all_bookings)
    #         if all_bookings:
    #             print('m i herere')
    #             for booking in all_bookings:
    #                 print(booking.start,booking.end,s,e)
    #                 if booking.start <= datetime.strptime(s, '%H:%M:%S').time() < booking.end or booking.start < datetime.strptime(e, '%H:%M:%S').time() <= booking.end:
    #                     status = booking.state
    #                     break
    #                 else:
    #                     status = True
    #         else:
    #             status = True
    #         return status
    #     if sp.price_type=='DAILY':
    #         status=11
    #         return status
    #         # all_bookings = Booking.objects.filter(model_service=si,date=d,booking_type='DAILY').filter(state='booking-request').order_by('start').order_by('end')
    #         # #
    #         # all_bookings = list(all_bookings)
    #         # #all_bookings is more then one which should not be the case
    #         # if all_bookings:
    #         #     status = False
    #         # else:
    #         #     status = True
    #         # return status



# def checkBookablity(slot):
#     s,e,d,st=slot['start'],slot['end'],slot['date'],slot['service_type']
#     status = bool()
#     if st=='MODELS':
#         si=slot['model_service_id']
#         all_bookings = Booking.objects.filter(model_service=si,date=d).filter(state='booking-request').order_by('start').order_by('end')
#         all_bookings = list(all_bookings)
#         print(all_bookings,'here all_bookings')
#         if all_bookings:
#             print('m i herere')
#             for booking in all_bookings:
#                 print(booking.start,booking.end,s,e)
#                 if booking.start < datetime.strptime(s, '%H:%M:%S').time() < booking.end or booking.start < datetime.strptime(e, '%H:%M:%S').time() < booking.end:
#                     status = False
#                     break
#                 else:
#                     status = True
#         else:
#             print('here i am now in else',)
#             status = True
#         return status
#

def addToCartDatacheck(data):
    print(data,'heredta')
    failed_obj = {}
    if data['service_type'] == 'MODELS' and 'model_service_id' not in data:
        print('nothere isit')
        data.update({'status':'Please provide valid models pk as model_service_id'})
        failed_obj = data
        
    if data['service_type'] == 'PHOTOGRAPHERS' and 'cameraman_service_id' not in data:
        data.update({'status':'Please provide valid Photographer pk as cameraman_service_id'})
        failed_obj = data

    if data['service_type'] == 'STUDIOS' and 'studio_service_id' not in data:
        data.update({'status':'Please provide valid Studio pk as studio_service_id'})
        failed_obj = data
    
    else:
        pass

    return failed_obj

from cartinfo.models import CartInfo
def cartsync(datas,user_pk):
    cart_items_list = []
    cart_items = CartInfo.objects.filter(user=user_pk)
    for cart in cart_items:
        if cart.service_type=='MODELS':
            service_id = cart.model_service_id.pk
        if cart.service_type=='PHOTOGRAPHERS':
            service_id = cart.cameraman_service_id.pk
        if cart.service_type=='STUDIOS':
            service_id = cart.cameraman_service_id.pk
        cart_tuple = (cart.start,cart.end,cart.booking_date,cart.service_type,service_id,cart.pk)
        cart_items_list.append(cart_tuple)
    print(cart_items_list,'cart_items_list') 
    actual_data = copy.deepcopy(datas)
    remove_frm_cart= []
    booking_dates = []
    m_items_inboth = []
    for data in datas:
        booking_dates.append(data['booking_date'])
    for data in datas:
        for cart_item in cart_items_list:
            model_counter_obj = 0
            if 'model_service_id' in data:
                if datetime.strptime(data['start'], '%H:%M:%S').time()==cart_item[0] and datetime.strptime(data['end'], '%H:%M:%S').time()==cart_item[1] and datetime.strptime(data['booking_date'],'%Y-%m-%d').date()==cart_item[2] and data['service_type']==cart_item[3] and data['model_service_id']==cart_item[4]:
                    # m_items_inboth.append(cart_item)                        
                    print('i should be there.',data,cart_item,'<==')
                    model_counter_obj=+1
                    # actual_data.remove(data)
                #if some cart items are not in add_to_cart for same date and service
                if datetime.strptime(data['start'], '%H:%M:%S').time()!=cart_item[0] and datetime.strptime(data['end'], '%H:%M:%S').time()!=cart_item[1] and datetime.strptime(data['booking_date'],'%Y-%m-%d').date()==cart_item[2] and data['service_type']==cart_item[3] and data['model_service_id']==cart_item[4]:
                    remove_frm_cart.append({'cart_id':cart_item[5],'service_type':cart_item[3]})
                # if datetime.strptime(data['booking_date'],'%Y-%m-%d').date()==cart_item[2] and data['service_type']==cart_item[3] and data['model_service_id']==cart_item[4]:                    
                    
                    # remove_frm_cart.append(cart_item)
                # print('heredata',datetime.strptime(data['start'], '%H:%M:%S').time()==cart_item[0],datetime.strptime(data['end'], '%H:%M:%S').time()==cart_item[1],datetime.strptime(data['booking_date'],'%Y-%m-%d').date()==cart_item[2],type(datetime.strptime(data['booking_date'],'%Y-%m-%d').date()),cart_item[2],type(cart_item[2]),data['service_type']==cart_item[3],data['model_service_id']==cart_item[4])
            
            if 'cameraman_service_id' in data:
                # print('heredata',datetime.strptime(data['start'], '%H:%M:%S').time()==cart_item[0],datetime.strptime(data['end'], '%H:%M:%S').time()==cart_item[1],datetime.strptime(data['booking_date'],'%Y-%m-%d').date()==cart_item[2],type(datetime.strptime(data['booking_date'],'%Y-%m-%d').date()),cart_item[2],type(cart_item[2]),data['service_type']==cart_item[3],data['model_service_id']==cart_item[4])
                if datetime.strptime(data['start'], '%H:%M:%S').time()==cart_item[0] and datetime.strptime(data['end'], '%H:%M:%S').time()==cart_item[1] and datetime.strptime(data['booking_date'],'%Y-%m-%d').date()==cart_item[2] and data['service_type']==cart_item[3] and data['cameraman_service_id']==cart_item[4]:
                    m_items_inboth.append(cart_item)
                    print('i should be there.')
                    actual_data.remove(data)
                if datetime.strptime(data['start'], '%H:%M:%S').time()!=cart_item[0] and datetime.strptime(data['end'], '%H:%M:%S').time()!=cart_item[1] and datetime.strptime(data['booking_date'],'%Y-%m-%d').date()==cart_item[2] and data['service_type']==cart_item[3] and data['cameraman_service_id']==cart_item[4]:
                    remove_frm_cart.append({'cart_id':cart_item[5],'service_type':cart_item['service_type']})

            if 'studio_service_id' in data:
                # print('heredata',datetime.strptime(data['start'], '%H:%M:%S').time()==cart_item[0],datetime.strptime(data['end'], '%H:%M:%S').time()==cart_item[1],datetime.strptime(data['booking_date'],'%Y-%m-%d').date()==cart_item[2],type(datetime.strptime(data['booking_date'],'%Y-%m-%d').date()),cart_item[2],type(cart_item[2]),data['service_type']==cart_item[3],data['model_service_id']==cart_item[4])
                if datetime.strptime(data['start'], '%H:%M:%S').time()==cart_item[0] and datetime.strptime(data['end'], '%H:%M:%S').time()==cart_item[1] and datetime.strptime(data['booking_date'],'%Y-%m-%d').date()==cart_item[2] and data['service_type']==cart_item[3] and data['studio_service_id']==cart_item[4]:
                    m_items_inboth.append(cart_item)
                    print('i should be there...')

                    actual_data.remove(data)
                if datetime.strptime(data['start'], '%H:%M:%S').time()!=cart_item[0] and datetime.strptime(data['end'], '%H:%M:%S').time()!=cart_item[1] and datetime.strptime(data['booking_date'],'%Y-%m-%d').date()==cart_item[2] and data['service_type']==cart_item[3] and data['studio_service_id']==cart_item[4]:
                    remove_frm_cart.append({'cart_id':cart_item[5],'service_type':cart_item['service_type']})
    
    # if model_counter_obj==len([[].append(i) for i in remove_frm_cart if i['service_type']=='MODELS']):
        #remove_frm_cart-add_to_cart=[]
    return {'actual_data':actual_data,'remove_from_cart':remove_frm_cart,}


'''
from booking.utilities import prior_checks_addtocart
data = {
    "service_type": "MODELS",
    "date": "2019-08-18",		
    "start": "08:00:00",
    "end": "09:00:00",
    "model_service_id": 43,
    "weekday":0
}

res = prior_checks_addtocart(data)

from booking.utilities import prior_checks_addtocart
data = [
{
    "service_type": "MODELS",
    "booking_date": "2019-08-19",		
    "start": "18:00:00",
    "end": "19:00:00",
    "model_service_id": 43,
    "weekday":1
}
,
	{
	    "service_type": "MODELS",
	    "booking_date": "2019-08-19",		
	    "start": "09:00:00",
	    "end": "10:00:00",
	    "model_service_id": 43,
	    "weekday":1
}
,{
    "service_type": "MODELS",
    "booking_date": "2019-08-19",		
    "start": "08:00:00",
    "end": "09:00:00",
    "model_service_id": 43,
    "weekday":1
}
]
res = prior_checks_addtocart(data)


'''

def prior_checks_addtocart(data):
    res = {'status': int(),'proceed':bool()}
    addToCart_data_check = addToCartDatacheck(data)
    if addToCart_data_check:
        res['proceed'] = False
        res['status']= 10
        return res
    else:
        slot_time_availbility = SlotTimeAvailCheck(data)
        print(slot_time_availbility,data,'ooo')
        if not slot_time_availbility:
            # res['reason'] = 'This time slot is not available'
            res['proceed'] = False
            res['status']= 1
            return res
        else:
            bookable = checkBookablity(data)
            if not bookable:
                res['proceed'] = False
                res['status']=2
                return res
            else:
                res['proceed'] = True
                res['status'] = 0
                return res



def prior_booking_checks(data):
    #perfect place to create a log in case of duplicate entry...
    res = {'status': int(),'proceed': bool()}
    #check1
    # pastDatetest = pastDateCheckup(data)
    # if pastDatetest:
    #     # res['reason'] = 'Start Time is More then the End Time'
    #     res['status']=4
    #     res['proceed'] = False
    #     return res
    slot_time_availbility = SlotTimeAvailCheck(data)
    print(slot_time_availbility,data,'ooo')
    if not slot_time_availbility:
        # res['reason'] = 'This time slot is not available'
        res['proceed'] = False
        res['status']= 1
        return res
    exist = doesBookingAlreadyExist(data)
    print('exist',exist)
    if exist:
        # res['reason'] = 'Already Booked for This Time; Please Choose some other time'
        res['proceed'] = False
        res['status']=2
        return res

    bookable = checkBookablity()
    if not bookable:
        res['proceed'] = False
        res['status']=2
        return res

    # over_lapping_test = bookingnotOverLapping(data)
    # print(over_lapping_test,'overlapping_test')
    # if over_lapping_test:
    #     # res['reason'] = 'Booking Timings are overlapping with Existing ones'
    #     status = 3
    #     res['proceed'] = False
    #     return res
   
    else:
        # res['reason']='All checks done for proceed False'
        res['status']=0
        res['proceed']=True
        return res

def createBooking(data):
    # res = prior_booking_checks(data)
    # print(res,'hereisres')
    # if res['proceed']:   
    model_service,cameraman_service,studio_service = None,None,None
    try:
        if data['service_type']=='MODELS':
            model_service = Model.objects.get(pk=data['service_id'])
        if data['service_type']=='PHOTOGRAPHERS':
            cameraman_service = Cameraman.objects.get(pk=data['service_id'])
        if data['service_type']=='STUDIOS':
            studio_service = Studio.objects.get(pk=data['service_id'])
    except Exception as e:
        print(e,'is it here can you see it')
        raise e
    # if data['service_type']=='MODELS':
    print(model_service,'model_ser',studio_service,cameraman_service)
    users = get_user_model()
    user = users.objects.get(
    pk=data['user']
    )
    date = data['date']
    start = data['start']
    end = data['end']
    customer_email = data.get("customer_email", None)
    customer_phone_number = data.get("customer_phone_number", None)
    customer_description = data.get("customer_description", None)
    customer_reminder = data.get("customer_reminder", None)
    customer_name = data.get("customer_name", None)
    customer_agree = data.get("customer_agree", False)
    service_type=data['service_type']
    print(service_type,'service_type')
    b_address_line1 = data.get("b_address_line1", None)
    b_address_line2 = data.get("b_address_line2", None)
    b_city = data.get("b_city", None)
    b_state = data.get("b_state", None)
    b_zip = data.get("b_zip", None)
    s_address_line1 = data.get("s_address_line1", None)
    s_address_line2 = data.get("s_address_line2", None)
    s_city = data.get("s_city", None)
    s_state = data.get("s_state", None)        
    s_zip = data.get("s_zip", None)
    project_desc = data.get("project_description", None)
    reporting_address = data.get("reporting_address", None)
    decline_reason = data.get("decline_reason", None)

    booking = Booking.objects.create(
            user=user,
            customer_name=customer_name,
            date=date,
            model_service=model_service,
            cameraman_service=cameraman_service,
            studio_service=studio_service,
            customer_email=customer_email,
            customer_phone_number=customer_phone_number,
            b_address_line1 = b_address_line1,
            b_address_line2 = b_address_line2,
            b_city = b_city,
            b_state=b_state,
            b_zip = b_zip,
            s_address_line1 = s_address_line1,
            s_address_line2 = s_address_line2,
            s_city = s_city,
            s_state=s_state,
            s_zip = s_zip,
            customer_description=customer_description,
            customer_reminder=customer_reminder,
            customer_agree=customer_agree,
            start=start,
            end=end,
            service_type=service_type,
            project_description=project_desc,
            reporting_address = reporting_address,
            decline_reason = decline_reason)

            
    print(booking.model_service,'booking')
    booking.state='booking-request'
    booking.save()
    data = {
            'id': booking.id,
            'start':start,
            'end':end,
            'date':date,
            'service_type':service_type,
            'service_id':data['service_id'],
            'user':data['user'],
            'created': True
        }
    return data



# def createBooking(data):
#     # res = prior_booking_checks(data)
#     # print(res,'hereisres')
#     # if res['proceed']:    
#     if data['service_type']=='MODELS':
#         service = Model.objects.get(pk=data['service_id'])
#         users = get_user_model()
#         user = users.objects.get(
#             pk=data['user']
#             )
#         date = data['date']
#         start = data['start']
#         end = data['end']
#         customer_email = data.get("customer_email", None)
#         customer_phone_number = data.get("customer_phone_number", None)
#         customer_description = data.get("customer_description", None)
#         customer_reminder = data.get("customer_reminder", None)
#         customer_name = data.get("customer_name", None)
#         customer_agree = data.get("customer_agree", False)
#         service_type=data['service_type']
#         #check if slot is not already booked
#         b_address_line1 = data.get("b_address_line1", None)
#         b_address_line2 = data.get("b_address_line2", None)
#         b_city = data.get("b_city", None)
#         b_state = data.get("b_state", None)
#         b_zip = data.get("b_zip", None)

#         s_address_line1 = data.get("s_address_line1", None)
#         s_address_line2 = data.get("s_address_line2", None)
#         s_city = data.get("s_city", None)
#         s_state = data.get("s_state", None)        
#         s_zip = data.get("s_zip", None)

#         booking = Booking.objects.create(
#         user=user,
#         model_service=service,
#         customer_name=customer_name,
#         date=date,
#         customer_email=customer_email,
#         customer_phone_number=customer_phone_number,
#         b_address_line1 = b_address_line1,
#         b_address_line2 = b_address_line2,
#         b_city = b_city,
#         b_state=b_state,
#         b_zip = b_zip,
#         s_address_line1 = s_address_line1,
#         s_address_line2 = s_address_line2,
#         s_city = s_city,
#         s_state=s_state,
#         s_zip = s_zip,
#         customer_description=customer_description,
#         customer_reminder=customer_reminder,
#         customer_agree=customer_agree,
#         start=start,
#         end=end,
#         service_type=service_type)
#         # slot=service_WeekdayOpeningPeriod.objects.get(pk=slot),
#         # service_type=service_type,
#         booking.state='booking-request'
#         booking.save()
#         data = {
#                 'id': booking.id,
#                 'start':start,
#                 'end':end,
#                 'date':date,
#                 'service_type':service_type,
#                 'service_id':data['service_id'],
#                 'user':data['user'],
#                 'created': True
#             }
        
#         return data



# def createBooking(data):
    # res = prior_booking_checks(data)
    # print(res,'hereisres')
    # if res['proceed']:    
    # if data['service_type']=='MODELS':
    #     service = Model.objects.get(pk=data['service_id'])
    #     users = get_user_model()
    #     user = users.objects.get(
    #         pk=data['user']
    #         )
    #     date = data['date']
    #     start = data['start']
    #     end = data['end']
    #     customer_email = data.get("customer_email", None)
    #     customer_phone_number = data.get("customer_phone_number", None)
    #     customer_description = data.get("customer_description", None)
    #     customer_reminder = data.get("customer_reminder", None)
    #     customer_name = data.get("customer_name", None)
    #     customer_agree = data.get("customer_agree", False)
    #     service_type=data['service_type']
    #     #check if slot is not already booked
    #     b_address_line1 = data.get("b_address_line1", None)
    #     b_address_line2 = data.get("b_address_line2", None)
    #     b_city = data.get("b_city", None)
    #     b_state = data.get("b_state", None)
    #     b_zip = data.get("b_zip", None)

    #     s_address_line1 = data.get("s_address_line1", None)
    #     s_address_line2 = data.get("s_address_line2", None)
    #     s_city = data.get("s_city", None)
    #     s_state = data.get("s_state", None)        
    #     s_zip = data.get("s_zip", None)

    #     booking = Booking.objects.create(
    #     user=user,
    #     model_service=service,
    #     customer_name=customer_name,
    #     date=date,
    #     customer_email=customer_email,
    #     customer_phone_number=customer_phone_number,
    #     b_address_line1 = b_address_line1,
    #     b_address_line2 = b_address_line2,
    #     b_city = b_city,
    #     b_state=b_state,
    #     b_zip = b_zip,
    #     s_address_line1 = s_address_line1,
    #     s_address_line2 = s_address_line2,
    #     s_city = s_city,
    #     s_state=s_state,
    #     s_zip = s_zip,
    #     customer_description=customer_description,
    #     customer_reminder=customer_reminder,
    #     customer_agree=customer_agree,
    #     start=start,
    #     end=end,
    #     service_type=service_type)
    #     # slot=service_WeekdayOpeningPeriod.objects.get(pk=slot),
    #     # service_type=service_type,
    #     booking.state='booking-request'
    #     booking.save()
    #     data = {
    #             'id': booking.id,
    #             'start':start,
    #             'end':end,
    #             'date':date,
    #             'service_type':service_type,
    #             'service_id':data['service_id'],
    #             'user':data['user'],
    #             'created': True
    #         }
        
    #     return data



# def createBooking(data):
#     # res = prior_booking_checks(data)
#     # print(res,'hereisres')
#     # if res['proceed']:
    
#     if data['service_type']=='MODELS':
#         service = Model.objects.get(pk=data['service_id'])
#         users = get_user_model()
#         user = users.objects.get(
#             pk=data['user']
#             )
#         date = data['date']
#         start = data['start']
#         end = data['end']
#         customer_email = data.get("customer_email", None)
#         customer_phone_number = data.get("customer_phone_number", None)
#         customer_description = data.get("customer_description", None)
#         customer_reminder = data.get("customer_reminder", None)
#         customer_name = data.get("customer_name", None)
#         customer_agree = data.get("customer_agree", False)
#         service_type=data['service_type']
#         #check if slot is not already booked

#         booking = Booking.objects.create(
#         user=user,
#         model_service=service,
#         customer_name=customer_name,
#         date=date,
#         customer_email=customer_email,
#         customer_phone_number=customer_phone_number,
#         customer_description=customer_description,
#         customer_reminder=customer_reminder,
#         customer_agree=customer_agree,
#         start=start,
#         end=end,
#         service_type=service_type)
#         # slot=service_WeekdayOpeningPeriod.objects.get(pk=slot),
#         # service_type=service_type,
#         booking.state='booking-request'
#         booking.save()
#         data = {
#                 'id': booking.id,
#                 'start':start,
#                 'end':end,
#                 'date':date,
#                 'service_type':service_type,
#                 'service_id':data['service_id'],
#                 'user':data['user'],
#                 'created': True
#             }
        
#         return data

#     if data['service_type']=='PHOTOGRAPHERS':
#         service = Cameraman.objects.get(pk=data['service_id'])
#         users = get_user_model()
#         user = users.objects.get(
#             pk=data['user']
#             )
#         date = data['date']
#         start = data['start']
#         end = data['end']
#         customer_email = data.get("customer_email", None)
#         customer_phone_number = data.get("customer_phone_number", None)
#         customer_description = data.get("customer_description", None)
#         customer_reminder = data.get("customer_reminder", None)
#         customer_name = data.get("customer_name", None)
#         customer_agree = data.get("customer_agree", False)
#         service_type=data['service_type']
#         #check if slot is not already booked

#         booking = Booking.objects.create(
#         user=user,
#         cameraman_service=service,
#         customer_name=customer_name,
#         date=date,
#         customer_email=customer_email,
#         customer_phone_number=customer_phone_number,
#         customer_description=customer_description,
#         customer_reminder=customer_reminder,
#         customer_agree=customer_agree,
#         start=start,
#         end=end,
#         # slot=service_WeekdayOpeningPeriod.objects.get(pk=slot),
#         service_type=service_type
#         # service_type=service_type,
#         )
#         booking.state='booking-request'
#         booking.save()
#         data = {
#                 'id': booking.id,
#                 'start':start,
#                 'end':end,
#                 'date':date,
#                 'service_type':service_type,
#                 'service_id':data['service_id'],
#                 'user':data['user'],
#                 'created': True
#             }
        
#         return data

#     if data['service_type']=='STUDIOS':
#         service = Studio.objects.get(pk=data['service_id'])
#         users = get_user_model()
#         user = users.objects.get(
#             pk=data['user']
#             )
#         date = data['date']
#         start = data['start']
#         end = data['end']
#         customer_email = data.get("customer_email", None)
#         customer_phone_number = data.get("customer_phone_number", None)
#         customer_description = data.get("customer_description", None)
#         customer_reminder = data.get("customer_reminder", None)
#         customer_name = data.get("customer_name", None)
#         customer_agree = data.get("customer_agree", False)
#         service_type=data['service_type']
#         #check if slot is not already booked

#         booking = Booking.objects.create(
#         user=user,
#         studio_service=service,
#         customer_name=customer_name,
#         date=date,
#         customer_email=customer_email,
#         customer_phone_number=customer_phone_number,
#         customer_description=customer_description,
#         customer_reminder=customer_reminder,
#         customer_agree=customer_agree,
#         start=start,
#         end=end,
#         # slot=service_WeekdayOpeningPeriod.objects.get(pk=slot),
#         service_type=service_type
#         # service_type=service_type,
#         )
#         booking.state='booking-request'
#         booking.save()
#         data = {
#                 'id': booking.id,
#                 'start':start,
#                 'end':end,
#                 'date':date,
#                 'service_type':service_type,
#                 'service_id':data['service_id'],
#                 'user':data['user'],
#                 'created': True
#             }
        
#         return data




# else:
#     data = {
#             'reason':res['reason'] ,
#             'created': False
#         }
#     obj={'data':data}
#     return obj
#



    
        # if len(all_bookings)>=2:
        #     print(all_bookings,'hereallbookingsaretheyordered')
        #     for i in range(len(all_bookings)-1):
        #         fb_slot,sb_slot = all_bookings[i],all_bookings[i+1]
        #         print(fb_slot,sb_slot,'fbsbslot')
        #         print('1st',fb_slot.end <= datetime.strptime(s, '%H:%M:%S').time())
        #         print('2nd',datetime.strptime(s, '%H:%M:%S').time() <= sb_slot.start,s,sb_slot.start)
        #         print('3rd',fb_slot.end <= datetime.strptime(e, '%H:%M:%S').time())
        #         print('4th',datetime.strptime(e, '%H:%M:%S').time() <= sb_slot.start,e,sb_slot.start)
        #         if fb_slot.end <= datetime.strptime(s, '%H:%M:%S').time() <= sb_slot.start and fb_slot.end <= datetime.strptime(e, '%H:%M:%S').time() <= sb_slot.start:
        #             print('if 1 is making it true')                    
        #             status = True
        #             break
        #         print('1st',fb_slot.start > datetime.strptime(s, '%H:%M:%S').time())
        #         print('2nd',datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end)
        #         print('3rd',fb_slot.start > datetime.strptime(s, '%H:%M:%S').time())
        #         print('4th',datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end)
                
        #         # if not fb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end and fb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end:
        #         if not sb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < sb_slot.end and not sb_slot.start > datetime.strptime(e, '%H:%M:%S').time() < sb_slot.end or not fb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end and not fb_slot.start > datetime.strptime(e, '%H:%M:%S').time() < fb_slot.end:
        #             print('if not 2 is making it true')
        #             status = True
        #         # if not sb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < sb_slot.end and not sb_slot.start > datetime.strptime(e, '%H:%M:%S').time() < sb_slot.end:
        #         #     print('if not 3 is making it true')                    
        #         #     status = True
        #         else:
        #             status = False
    
    
    #     si = slot['studio_service_id']
    #     print(slot,'slot')
    #     all_bookings = Booking.objects.filter(model_service=si,date=d).filter(state='booking-request').order_by('start').order_by('end')
    #     print(all_bookings,'all_bookings')
    #     # available_tslots = model_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w).order_by('start').order_by('end')
    #     #remove the duplicate slots very quickly
    #     all_bookings = list(all_bookings)
    #     if not all_bookings:
    #         print('m i there!')
    #         status = True
    #
    #     if len(all_bookings)==1:
    #         print(all_bookings[0],'one booking')
    #         if all_bookings[0].start==datetime.strptime(s, '%H:%M:%S').time() and all_bookings[0].end==datetime.strptime(e, '%H:%M:%S').time():
    #             status = False
    #         else:
    #             status = True
    #
    #
    #     if len(all_bookings)>=2:
    #         for i in range(len(all_bookings)-1):
    #             fb_slot,sb_slot = all_bookings[i],all_bookings[i+1]
    #             if fb_slot.end <= datetime.strptime(s, '%H:%M:%S').time() <= sb_slot.start and fb_slot.end <= datetime.strptime(e, '%H:%M:%S').time() <= sb_slot.start:
    #                 status = True
    #                 break
    #             else:
    #                 status = False
    #
    # return status
        
def checkCoreBookingAv(data):
    print(data,'right data')
    s,e,d,w,service_type,service_id = datetime.strptime(data['start'], '%H:%M:%S').time(),\
                                    datetime.strptime(data['end'], '%H:%M:%S').time(),\
                                    data['date'],\
                                    data['weekday'],\
                                    data['service_type'],\
                                    data['service_id']

    #get all the bookings
    print(service_type,'service_type')
    if service_type=='MODELS':
        model = Model.objects.get(pk=int(service_id))
        available_slots = Model_WeekdayOpeningPeriod.objects.filter(service_id=data['service_id']).filter(weekday=data['weekday'])
        available_slots=list(available_slots)
        print('available_slots',available_slots)
        all_booked = Booking.objects.filter(model_service=data['service_id']).filter(date = data['date']).filter(state='booking-request')
        #get all the available slots
        status = bool()
        if model.s_price_type=='HOURLY':
            if len(available_slots)>0 and len(all_booked)<=0:
                #we check if the given slot is in the available slot then True or 
                for slot in available_slots:
                    if slot.start <= s <= slot.end and slot.start <= e <= slot.end:
                        status = True
                        break
                    else:
                        status = False
                return status

            if len(available_slots)<0:
                status=False
                return status
            
            if len(available_slots)>0 and len(all_booked)>0:    
                print('DID I RAN')
                remain_list=[]
                partial_slots=[]
                remain_list=copy.deepcopy(available_slots)
                # counter=0
                for slot in available_slots:
                    for booking in all_booked:
                        if booking.start==slot.start and booking.end==slot.end:
                            remain_list.remove(slot)
                            break  
                        else:
                            print('critical else should not be running')
                            pass
                    
                    break
                print('remain_list',remain_list,partial_slots)
                if remain_list:
                    for slot in remain_list:
                        print('knockknock')
                        if slot.start <= s <= slot.end and slot.start <= e <= slot.end:
                            status = True
                            print('me')
                            break
                        
                            print('third',slot,booking)
                            remain_list.remove(slot)
                            obj = {'start':slot.start,'end':booking.start}
                            partial_slots.append(obj)
                            break   

                        else:
                            status = False
                if partial_slots:
                    for slot in partial_slots:
                        print('knockknock2',partial_slots,s,e)
                        if slot['start'] <= s <= slot['end'] and slot['start'] <= e <= slot['end']:
                            status = True
                            print('you')
                            break
                        else:
                            status = False
                else:
                    status = False
        if model.s_price_type=='DAILY':
            if len(available_slots)>0 and len(all_booked)<=0:
                status = True
            if len(available_slots)<=0:
                status = False
            if len(available_slots)>0 and len(all_booked)>0:
                print('here you are right!!')
                status = False
        return status
    if service_type=='PHOTOGRAPHERS':
        cameraman_sp = Cameraman_WeekdayOpeningPeriod.objects.get(service_id=service_id)
        available_slots = Cameraman_WeekdayOpeningPeriod.objects.filter(service_id=data['service_id']).filter(weekday=data['weekday'])    
        available_slots=list(available_slots)
        print('available_slots',available_slots)
        all_booked = Booking.objects.filter(cameraman_service=data['service_id']).filter(date = data['date']).filter(state='booking-request')
        #get all the available slots
        status = bool()
        if cameraman_sp.price_type=='HOURLY':
            if len(available_slots)>0 and len(all_booked)<=0:
                #we check if the given slot is in the available slot then True or 
                for slot in available_slots:
                    if slot.start <= s <= slot.end and slot.start <= e <= slot.end:
                        status = True
                        break
                    else:
                        status = False
                return status

            if len(available_slots)<0:
                status=False
                return status
            
            if len(available_slots)>0 and len(all_booked)>0:    
                remain_list=[]
                remain_list=copy.deepcopy(available_slots)
                for slot in available_slots:
                    for booking in all_booked:
                        if booking.start==slot.start:
                            remain_list.remove(slot)
                            break  
                        else:
                            pass
                if remain_list:
                    for slot in remain_list:
                        print('knockknock')
                        if slot.start <= s <= slot.end and slot.start <= e <= slot.end:
                            status = True
                            break
                        else:
                            status = False
                else:
                    status = False
        if cameraman_sp.price_type=='DAILY':
            if len(available_slots)>0 and len(all_booked)<=0:
                status = True
            if len(available_slots)<=0:
                status = False
            if len(available_slots)>0 and len(all_booked)>0:
                print('here you are right!!')
                status = False
        return status
    if service_type=='STUDIOS':
        studio_sp = StudioServicePricing.objects.get(service_id=service_id)
        available_slots = Studio_WeekdayOpeningPeriod.objects.filter(service_id=data['service_id']).filter(weekday=data['weekday'])    
        available_slots=list(available_slots)
        print('available_slots',available_slots)
        all_booked = Booking.objects.filter(studio_service=data['service_id']).filter(date = data['date']).filter(state='booking-request')
        #get all the available slots
        status = bool()
        if studio_sp.price_type=='HOURLY':
            if len(available_slots)>0 and len(all_booked)<=0:
                #we check if the given slot is in the available slot then True or 
                for slot in available_slots:
                    if slot.start <= s <= slot.end and slot.start <= e <= slot.end:
                        status = True
                        break
                    else:
                        status = False
                return status

            if len(available_slots)<0:
                status=False
                return status
            
            if len(available_slots)>0 and len(all_booked)>0:    
                remain_list=[]
                remain_list=copy.deepcopy(available_slots)
                for slot in available_slots:
                    for booking in all_booked:
                        if booking.start==slot.start:
                            remain_list.remove(slot)
                            break  
                        else:
                            pass
                if remain_list:
                    for slot in remain_list:
                        print('knockknock')
                        if slot.start <= s <= slot.end and slot.start <= e <= slot.end:
                            status = True
                            break
                        else:
                            status = False
                else:
                    status = False
        if studio_sp.price_type=='DAILY':
            if len(available_slots)>0 and len(all_booked)<=0:
                status = True
            if len(available_slots)<=0:
                status = False
            if len(available_slots)>0 and len(all_booked)>0:
                print('here you are right!!')
                status = False
        return status


def checkServiceBookingListAvail(datas):
    print('datas',datas)
    result=[]
    for data in datas:
        current_weekday = datetime.strptime(data['date'],'%Y-%m-%d').date().weekday()+1
        print('weekday from date',current_weekday)
        s,e,d,w,service_type = datetime.strptime(data['start'], '%H:%M:%S').time(),\
                                          datetime.strptime(data['end'], '%H:%M:%S').time(),\
                                          data['date'],\
                                          data['weekday'],\
                                          data['service_type'],\
        #create a function that can check ifBooked
        # status = checkCoreBookingAv(data)
        if service_type=='MODELS':
            slot_error = []
            data['model_service_id']=data['service_id']
            si = data['model_service_id']
        if service_type=='PHOTOGRAPHERS':
            slot_error = []
            data['cameraman_service_id']=data['service_id']
            si = data['cameraman_service_id']
        if service_type=='STUDIOS':
            slot_error = []
            data['studio_service_id']=data['service_id']
            si = data['studio_service_id']
        isslotavailable = SlotTimeAvailCheck(data)
        print('arehere',isslotavailable)
    

        if isslotavailable:
            print('slot is available')
            bookable = checkBookablity(data)
            #what if there is not state in bookable
            print(bookable,'here is bookable')
            if not bookable['proceed']:
                if bookable['state']=='approved':
                    status = 2
                else:
                    status= 0
            

            else:
                print('booking is not available')
                # slot_error.append('booking is not available')
                status = 0
            print('isslotavaielse long')
        else:
            print('slot is not available')
            status =1

        result.append({'start': s, 'end': e,'model_service_id':si,'service_type':service_type, 'weekday': data['weekday'], 'date': data['date'], 'status': status})
    # result = {'start': s, 'end': e, 'weekday': data['weekday'], 'date': data['date'], 'status': status}
    return result





# def checkSlotAvailbility(data):
#     '''

#     :param data: is an object.
#     '''
#     service_type,service_id,start,end,date=data['service_type'],data['service_id'],data['start'],data['end'],data['date']

#     return





# def checkBookablity(slot):
#     s,e,d,st=slot['start'],slot['end'],slot['date'],slot['service_type']
#     status = bool()
#     if st=='MODELS':
#         si = slot['model_service_id']
#         print(slot,'slot')
#         all_bookings = Booking.objects.filter(model_service=si,date=d).filter(state='booking-request').order_by('start').order_by('end')
#         print(all_bookings,'all_bookings')
#         # available_tslots = model_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w).order_by('start').order_by('end')
#         #remove the duplicate slots very quickly
#         all_bookings = list(all_bookings)
#         if not all_bookings:
#             print('m i there!')
#             status = True
#         # todo=> this below is not neccessarly true so fix it soon
#         if len(all_bookings)==1:
#             print(all_bookings[0],'one booking')
#             if all_bookings[0].start==datetime.strptime(s, '%H:%M:%S').time() and all_bookings[0].end==datetime.strptime(e, '%H:%M:%S').time():
#                 status = False
#             else:
#                 status = True

#         if len(all_bookings)>=2:
#             print(all_bookings,'hereallbookingsaretheyordered')
#             for i in range(len(all_bookings)-1):
#                 fb_slot,sb_slot = all_bookings[i],all_bookings[i+1]
#                 print(fb_slot,sb_slot,'fbsbslot')
#                 print('1st',fb_slot.end <= datetime.strptime(s, '%H:%M:%S').time())
#                 print('2nd',datetime.strptime(s, '%H:%M:%S').time() <= sb_slot.start,s,sb_slot.start)
#                 print('3rd',fb_slot.end <= datetime.strptime(e, '%H:%M:%S').time())
#                 print('4th',datetime.strptime(e, '%H:%M:%S').time() <= sb_slot.start,e,sb_slot.start)
#                 if fb_slot.end <= datetime.strptime(s, '%H:%M:%S').time() <= sb_slot.start and fb_slot.end <= datetime.strptime(e, '%H:%M:%S').time() <= sb_slot.start:
#                     print('if 1 is making it true')                    
#                     status = True
#                     break
#                 print('1st',fb_slot.start > datetime.strptime(s, '%H:%M:%S').time())
#                 print('2nd',datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end)
#                 print('3rd',fb_slot.start > datetime.strptime(s, '%H:%M:%S').time())
#                 print('4th',datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end)
                
#                 # if not fb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end and fb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end:
#                 if not sb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < sb_slot.end and not sb_slot.start > datetime.strptime(e, '%H:%M:%S').time() < sb_slot.end or not fb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end and not fb_slot.start > datetime.strptime(e, '%H:%M:%S').time() < fb_slot.end:
#                     print('if not 2 is making it true')
#                     status = True
#                 # if not sb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < sb_slot.end and not sb_slot.start > datetime.strptime(e, '%H:%M:%S').time() < sb_slot.end:
#                 #     print('if not 3 is making it true')                    
#                 #     status = True
#                 else:
#                     status = False
    
#         # if len(all_bookings)>=2:
#         #     print(all_bookings,'hereallbookingsaretheyordered')
#         #     for i in range(len(all_bookings)-1):
#         #         fb_slot,sb_slot = all_bookings[i],all_bookings[i+1]
#         #         print(fb_slot,sb_slot,'fbsbslot')
#         #         print('1st',fb_slot.end <= datetime.strptime(s, '%H:%M:%S').time())
#         #         print('2nd',datetime.strptime(s, '%H:%M:%S').time() <= sb_slot.start,s,sb_slot.start)
#         #         print('3rd',fb_slot.end <= datetime.strptime(e, '%H:%M:%S').time())
#         #         print('4th',datetime.strptime(e, '%H:%M:%S').time() <= sb_slot.start,e,sb_slot.start)
#         #         if fb_slot.end <= datetime.strptime(s, '%H:%M:%S').time() <= sb_slot.start and fb_slot.end <= datetime.strptime(e, '%H:%M:%S').time() <= sb_slot.start:
#         #             print('if 1 is making it true')                    
#         #             status = True
#         #             break
#         #         print('1st',fb_slot.start > datetime.strptime(s, '%H:%M:%S').time())
#         #         print('2nd',datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end)
#         #         print('3rd',fb_slot.start > datetime.strptime(s, '%H:%M:%S').time())
#         #         print('4th',datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end)
                
#         #         # if not fb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end and fb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end:
#         #         if not sb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < sb_slot.end and not sb_slot.start > datetime.strptime(e, '%H:%M:%S').time() < sb_slot.end or not fb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < fb_slot.end and not fb_slot.start > datetime.strptime(e, '%H:%M:%S').time() < fb_slot.end:
#         #             print('if not 2 is making it true')
#         #             status = True
#         #         # if not sb_slot.start > datetime.strptime(s, '%H:%M:%S').time() < sb_slot.end and not sb_slot.start > datetime.strptime(e, '%H:%M:%S').time() < sb_slot.end:
#         #         #     print('if not 3 is making it true')                    
#         #         #     status = True
#         #         else:
#         #             status = False
    
#     if st=='PHOTOGRAPHERS':
#         si = slot['cameraman_service_id']
#         print(slot,'slot')
#         all_bookings = Booking.objects.filter(model_service=si,date=d).filter(state='booking-request').order_by('start').order_by('end')
#         print(all_bookings,'all_bookings')
#         # available_tslots = model_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w).order_by('start').order_by('end')
#         #remove the duplicate slots very quickly
#         all_bookings = list(all_bookings)
#         if not all_bookings:
#             print('m i there!')
#             status = True

#         if len(all_bookings)==1:
#             print(all_bookings[0],'one booking')
#             if all_bookings[0].start==datetime.strptime(s, '%H:%M:%S').time() and all_bookings[0].end==datetime.strptime(e, '%H:%M:%S').time():
#                 status = False
#             else:
#                 status = True

            
#         if len(all_bookings)>=2:
#             for i in range(len(all_bookings)-1):
#                 fb_slot,sb_slot = all_bookings[i],all_bookings[i+1]
#                 if fb_slot.end <= datetime.strptime(s, '%H:%M:%S').time() <= sb_slot.start and fb_slot.end <= datetime.strptime(e, '%H:%M:%S').time() <= sb_slot.start:
#                     status = True
#                     break
#                 else:
#                     status = False
#     if st=='STUDIOS':
#         si = slot['studio_service_id']
#         print(slot,'slot')
#         all_bookings = Booking.objects.filter(model_service=si,date=d).filter(state='booking-request').order_by('start').order_by('end')
#         print(all_bookings,'all_bookings')
#         # available_tslots = model_WeekdayOpeningPeriod.objects.filter(service_id=si,weekday=w).order_by('start').order_by('end')
#         #remove the duplicate slots very quickly
#         all_bookings = list(all_bookings)
#         if not all_bookings:
#             print('m i there!')
#             status = True

#         if len(all_bookings)==1:
#             print(all_bookings[0],'one booking')
#             if all_bookings[0].start==datetime.strptime(s, '%H:%M:%S').time() and all_bookings[0].end==datetime.strptime(e, '%H:%M:%S').time():
#                 status = False
#             else:
#                 status = True

            
#         if len(all_bookings)>=2:
#             for i in range(len(all_bookings)-1):
#                 fb_slot,sb_slot = all_bookings[i],all_bookings[i+1]
#                 if fb_slot.end <= datetime.strptime(s, '%H:%M:%S').time() <= sb_slot.start and fb_slot.end <= datetime.strptime(e, '%H:%M:%S').time() <= sb_slot.start:
#                     status = True
#                     break
#                 else:
#                     status = False
    
#     return status
        