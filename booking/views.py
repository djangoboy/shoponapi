import copy
from django.http import Http404
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_204_NO_CONTENT
from rest_framework.views import APIView
from config.jsonrand import custom_response
from .serializers import BookingSerializer# UpdateBookinsPartiallySerializer
from .models import Booking
from booking.utilities import prior_booking_checks, checkCoreBookingAv, createBooking, checkServiceBookingListAvail, \
    checkBookablity, SlotTimeAvailCheck
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.generics import ListAPIView, UpdateAPIView, RetrieveUpdateAPIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
# from vendor.models import Vendor_Info
# from .permissions import UpdateBooking_Permission
from django.core.exceptions import ValidationError
from .validators import validate_slot_availibity
from notification.tasks import booking_onbooking_event_lis


class BookingsList(ListAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('model_service','cameraman_service','studio_service','date','vendor')#,'user')

    def list(self, request, *args, **kwargs):
        print(request.query_params,'here is the dict')
        print(request.META['PATH_INFO'],'suffix_path')
        print(self,type(self),'here is your self')
        path = request.META['PATH_INFO']
        # if path.find("/vendor_bookings/all")<0:
        print('/all ran just now')
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=self.request.user)
        serializer = self.get_serializer(queryset, many=True)
        return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)



class UpdateBookingStatus(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    # permission_classes = [UpdateBooking_Permission,]
    # serializer_class = UpdateBookinsPartiallySerializer
    lookup_field = 'pk'
    
    def patch(self,request,*args,**kwargs):
        pk = kwargs['pk']
        try:
            instance = Booking.objects.get(pk=pk)
        except Booking.DoesNotExist:
            return custom_response(request.data, 'Booking Does not Exist', message="No Booking with This Booking Id Exist", status_code=HTTP_400_BAD_REQUEST)
                
        print(self.check_object_permissions(self.request, instance) ,'hereusee')               
        print(instance.pk,'here is instance',request.data)
        import copy
        #this does not need to copy all the references but for now it does.
        # instance_dict = dict((name, getattr(instance, name)) for name in dir(instance) if not name.startswith('__'))
        ins_dict = instance.__dict__
        instance_dict = copy.deepcopy(ins_dict)
        prev_book_obj = {}
        for k,v in instance_dict.items():
            if not k.startswith('__') or not k.startswith('_'):
                prev_book_obj[k]=v
        serializer = UpdateBookinsPartiallySerializer(instance=instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        # print(serializer.is_valid(raise_exception=True),'serializer.is_valid(raise_exception=True)')
        print(serializer.validated_data,'<- validated data')
        ser_items = serializer.initial_data.items()
        serializer.save()
        print('here is your validated data in views',serializer.initial_data)
        # prev_book_dict = serializer.initial_data
        booking_updated_obj = {}
        for k,v in ser_items:
            if prev_book_obj[k]!=v:
                booking_updated_obj[k]=prev_book_obj[k]
        print(booking_updated_obj,'booking_updated_obj')
        params_obj = {'booking_id':pk,'used_by':self.request.user.username,'booking_updated_obj':booking_updated_obj} 
        booking_onbooking_event_lis.delay(event='ONBOOKINGSTATUSCHANGED',params_obj=params_obj)
        print(serializer.data,'instance.state after',instance.state)        
        return custom_response(request.data, '', message="Booking Successfully Updated", status_code=HTTP_200_OK)


# class UpdateBookingStatus(UpdateAPIView):
#     authentication_classes = [JSONWebTokenAuthentication]
#     serializer_class = UpdateBookinsPartiallySerializer
#     # permission_classes = [ProductPermission]
#     lookup_field = 'pk'
#     queryset = Booking.objects.all()

#     def update(self, request, *args, **kwargs):
#         print(kwargs,'here kwargs')
#         partial = kwargs.pop('partial', False)
#         instance = self.get_object()
#         print(instance.pk,'here is instance',partial)
#         serializer = self.get_serializer(instance, data=request.data, partial=partial)
#         serializer.is_valid(raise_exception=True)
#         print(serializer.is_valid(raise_exception=True),'serializer.is_valid(raise_exception=True)')
#         self.perform_update(serializer)
#         if 's_price_type' in request.data:
#             service_id = kwargs['id']
#             sp = service_pricing.objects.get(service_id=service_id)
#             sp.price_type=request.data['s_price_type']
#             sp.save()
#         print('partial update ran')
#         print(instance.state,'instance.state')
#         #
#         return custom_response(serializer.data, '', message="Successfully Updated", status_code=HTTP_200_OK)

#     # def partial_update(self, request, *args, **kwargs):
#     #     kwargs['partial'] = True
#     #     print('partial update ran')
#     #     return self.update(request, *args, **kwargs)



#     # def partial_update(self, request, *args, **kwargs):
#     #     kwargs['partial'] = True
#     #     return self.update(request, *args, **kwargs)

#     # def perform_update(self, serializer):
#     #     # get the object itself
#     #     instance = self.get_object()
#     #     # modify fields during the update
#     #     modified_instance = serializer.save(model_field=new_value)



class BookingsListForLOUser(ListAPIView):
    # authentication_classes = [JSONWebTokenAuthentication]
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('model_service','date','cameraman_service','studio_service')#,'user')

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        
        # queryset = queryset.filter(user=self.request.user)
        serializer = self.get_serializer(queryset, many=True)
        return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)



# class UserBookingsList(ListAPIView):
#     authentication_classes = [JSONWebTokenAuthentication]
#     queryset = Booking.objects.all()
#     serializer_class = BookingSerializer
#     filter_backends = (filters.SearchFilter, DjangoFilterBackend)
#     filter_fields = ('model_service','cameraman_service','studio_service','date','vendor')

#     def list(self, request, *args, **kwargs):
#         print(request.META['PATH_INFO'],'suffix_path')
#         print(self,type(self),'here is your self')
#         path = request.META['PATH_INFO']
#         if path.find("/vendor_bookings/all")<0:
#             print('/all')
#             queryset = self.filter_queryset(self.get_queryset())
#             serializer = self.get_serializer(queryset, many=True)
#             return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)
#         elif path.find("/vendor_bookings/all")>0:
#             queryset = self.filter_queryset(self.get_queryset())
#             user = self.request.user
#             vendor = Vendor_Info.objects.get(user=user)
#             queryset = queryset.filter(vendor=vendor)
#             print(vendor,queryset,'afdafdafdf')
#             serializer = self.get_serializer(queryset, many=True)
#             return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)


class VendorBookingsList(ListAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('model_service','cameraman_service','studio_service','date','vendor')

    def list(self, request, *args, **kwargs):
        print(request.META['PATH_INFO'],'suffix_path')
        print(self,type(self),'here is your self')
        path = request.META['PATH_INFO']
        queryset = self.filter_queryset(self.get_queryset())
        user = self.request.user
        # vendor = Vendor_Info.objects.get(user=user)
        queryset = queryset.filter()
        # print(vendor,queryset,'afdafdafdf')
        serializer = self.get_serializer(queryset, many=True)
        return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)


# BookingsListForLOUser
# class VendorBookings(ListAPIView):
    
#     authentication_classes = [JSONWebTokenAuthentication]
#     queryset = Booking.objects.all()
#     serializer_class = BookingSerializer
#     filter_backends = (filters.SearchFilter, DjangoFilterBackend)
#     filter_fields = ('model_service','cameraman_service','studio_service','date')

#     def list(self, request, *args, **kwargs):
#         print(self,type(self),'here is your self')
#         queryset = self.filter_queryset(self.get_queryset())
#         serializer = self.get_serializer(queryset, many=True)
#         return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)
class BookingDetail(APIView):
    """
    Retrieve a booking instance.
    """
    authentication_classes = [JSONWebTokenAuthentication]
    def get_object(self, pk):
        try:
            return Booking.objects.get(pk=pk)
        except Booking.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        booking = self.get_object(pk)
        serializer = BookingSerializer(booking)
        return custom_response(serializer.data, '', message='', status_code=HTTP_200_OK)

    def put(self, request, pk, format=None):
        booking = self.get_object(pk)
        serializer = BookingSerializer(booking, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return custom_response(serializer.data, '', message='', status_code=HTTP_200_OK)
        return custom_response('', serializer.errors, message='', status_code=HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        booking = self.get_object(pk)
        booking.delete()
        return custom_response('', '', message='Booking Deleted', status_code=HTTP_204_NO_CONTENT)

def applyPriorDataListChecks(datas):
    # datas=request.data
    r_data_list=[]
    prior_date_data=[]
    from datetime import datetime
    current_date = datetime.date(datetime.now())
    for r_data in datas:
        st,si,s,e,d = r_data['service_type'],r_data['service_id'],\
        r_data['start'],r_data['end'],r_data['date']
        # obj = {'start':s,'end':e,'service_type':st,'service_id':si,'date':d}
        t_obj = (s,e,st,si,d)
        r_data_list.append(t_obj)
        from datetime import datetime
        datetime_object = datetime.strptime(d, '%Y-%m-%d')


        if datetime_object.date() < current_date:
            print('date condition met')
            prior_date_data.append(r_data)

    print(r_data_list,'r_data_list')
    print(set(r_data_list),'set(r_data_list)',len(list(set(r_data_list)))!= len(datas))                
    if len(list(set(r_data_list)))!= len(datas):
        # return custom_response(datas,'Can not be processed; as there are duplicate bookings; Please resolve it first','Duplicate is not allowed',status_code=HTTP_400_BAD_REQUEST)
        return {'error_message':'Duplicate Bookings','error':True}
    if prior_date_data:
        return {'error':True,'error_message':'Past Date Booking is not allowed','error_data':prior_date_data}

    else:
        return {'error':False}
class CreateBooking(APIView):
    """
    CREATE a booking instance.
    """
    authentication_classes = [JSONWebTokenAuthentication]
    def post(self, request, format=None):
        result,model_service_data,cameraman_service_data, studio_service_data, created_bookings_list,b_result_l=list(),list(),list(),list(),list(),list()
        if isinstance(request.data,list):
            datas=request.data
            prior_datalistcheckres = applyPriorDataListChecks(datas)
            if prior_datalistcheckres['error']:
                if 'error_data' in prior_datalistcheckres:
                    return custom_response(prior_datalistcheckres['error_data'],prior_datalistcheckres['error_message'],prior_datalistcheckres['error_message'],status_code=HTTP_400_BAD_REQUEST)                
                else:
                    return custom_response(datas,prior_datalistcheckres['error_message'],prior_datalistcheckres['error_message'],status_code=HTTP_400_BAD_REQUEST)                
            else:
                pass
            #   #this for loop can go below
            # for r_data in datas:
            #     st, s, e, bd = r_data['service_type'], r_data['start'], r_data['end'], r_data['date']
            #     r_data_list.append([st, s, bd])
            failed_bookings=[]
            
            for slot in request.data:
                from datetime import datetime
                current_weekday = datetime.strptime(slot['date'],'%Y-%m-%d').date().weekday()+1
                print('this is the current weekday => ',current_weekday)
                slot['weekday']=current_weekday
                if slot['service_type']=='MODELS':
                    print(slot,'slot')
                    model_slot_obj = {'start':slot['start'],'end':slot['end'],'date':slot['date'],'weekday':slot['weekday'],'service_type':slot['service_type'],'model_service_id':slot['service_id'],'service_id':slot['service_id']}
                    model_service_data.append(model_slot_obj)
                if slot['service_type']=='PHOTOGRAPHERS':
                    print(slot,'slot')
                    cameraman_slot_obj = {'start':slot['start'],'end':slot['end'],'date':slot['date'],'weekday':slot['weekday'],'service_type':slot['service_type'],'cameraman_service_id':slot['service_id'],'service_id':slot['service_id']}
                    cameraman_service_data.append(cameraman_slot_obj)

                if slot['service_type']=='STUDIOS':
                    print(slot,'slot')
                    studio_slot_obj = {'start':slot['start'],'end':slot['end'],'date':slot['date'],'weekday':slot['weekday'],'service_type':slot['service_type'],'studio_service_id':slot['service_id'],'service_id':slot['service_id']}
                    studio_service_data.append(studio_slot_obj)

            m_data_res = checkServiceBookingListAvail(model_service_data)
            c_data_res = checkServiceBookingListAvail(cameraman_service_data)
            s_data_res = checkServiceBookingListAvail(studio_service_data)
            print(m_data_res,c_data_res,s_data_res,'hereress')
            final_list = m_data_res+c_data_res+s_data_res#here add more to this list
            # booked_data_list=[]
            failed_booking_results=[]
            print(final_list,'final_list')
            failed_slots=[]
            for obj in final_list:
                print('objheredfdf',obj)
                if obj['status']!=0:
                    print('how many times')
                    failed_slots.append(obj)    
                
               
            print(failed_slots,'failed_slots')
            if not failed_slots:
                print('m i')
                for data in request.data:
                    print(data,'databeforegoing in cratebooking')
                    data['user']=self.request.user.pk
                    # data['project_description']
                    b_result = createBooking(data)
                    print('b_Result=>',b_result['id'],type(b_result['id']))
                    b_result_l.append(b_result)
            print('created_bookings_list',created_bookings_list)
            for booking in b_result_l:
                if booking['created']:
                    print('m i ever here..')
                    created_bookings_list.append(booking)
            # print(failed_booking_results,'failed_booking_results')
            if created_bookings_list:
                
                return custom_response(created_bookings_list,'','Booked successful',status_code=HTTP_200_OK)
            # if failed_bookings:
            #     return custom_response(failed_bookings,'Booking Aborted','booking they can not be booked are given in data',status_code=HTTP_400_BAD_REQUEST)
            if failed_slots:
                return custom_response(failed_slots, errors='Booking Aborted', message='booking they can not be booked are given in data', status_code=HTTP_400_BAD_REQUEST)



# class CreateBooking(APIView):
#     """
#     CREATE a booking instance.
#     """
#     authentication_classes = [JSONWebTokenAuthentication]
#     def post(self, request, format=None):
#         result = []
#         model_service_data=[]
#         cameraman_service_data=[]
#         studio_service_data=[]
#         b_result_l=[]
#         created_bookings_list = []
#         if isinstance(request.data,list):
#             # failed_slots=[]
#             failed_bookings=[]
#             for slot in request.data:
#                 if slot['service_type']=='MODELS':
#                     print(slot,'slot')
#                     model_slot_obj = {'start':slot['start'],'end':slot['end'],'date':slot['date'],'weekday':slot['weekday'],'service_type':slot['service_type'],'model_service_id':slot['service_id'],'service_id':slot['service_id']}
#                     model_service_data.append(model_slot_obj)
#                 if slot['service_type']=='PHOTOGRAPHERS':
#                     print(slot,'slot')
#                     cameraman_slot_obj = {'start':slot['start'],'end':slot['end'],'date':slot['date'],'weekday':slot['weekday'],'service_type':slot['service_type'],'model_service_id':slot['service_id'],'service_id':slot['service_id']}
#                     cameraman_service_data.append(cameraman_slot_obj)

#                 if slot['service_type']=='STUDIOS':
#                     print(slot,'slot')
#                     studio_slot_obj = {'start':slot['start'],'end':slot['end'],'date':slot['date'],'weekday':slot['weekday'],'service_type':slot['service_type'],'model_service_id':slot['service_id'],'service_id':slot['service_id']}
#                     studio_service_data.append(studio_slot_obj)

#             m_data_res = checkServiceBookingListAvail(model_service_data)
#             c_data_res = checkServiceBookingListAvail(cameraman_service_data)
#             s_data_res = checkServiceBookingListAvail(studio_service_data)

#             final_list = m_data_res+c_data_res+s_data_res#here add more to this list
#             # booked_data_list=[]
#             failed_booking_results=[]
#             print(final_list,'final_list')
#             failed_slots=[]
#             for obj in final_list:
#                 print('objheredfdf',obj)
#                 if not obj['status']==0:
#                     print('how many times')
#                     failed_slots.append(obj)    
        
#             print(failed_slots,'failed_slots')
#             if not failed_slots:
#                 print('m i')
#                 for data in request.data:
#                     b_result = createBooking(data)
#                     b_result_l.append(b_result)
#             print('created_bookings_list',created_bookings_list)
#             for booking in b_result_l:
#                 if booking['created']:
#                     print('m i ever here..')
#                     created_bookings_list.append(booking)
#             # print(failed_booking_results,'failed_booking_results')
#             if created_bookings_list:
#                 return custom_response(created_bookings_list,'Booked successful','booking successfully created',status_code=HTTP_200_OK)
#             if failed_bookings:
#                 return custom_response(failed_bookings,'Booking Aborted','booking they can not be booked are given in data',status_code=HTTP_400_BAD_REQUEST)
#             if failed_slots:
#                 return custom_response(failed_slots, errors='Booking Aborted', message='booking they can not be booked are given in data', status_code=HTTP_400_BAD_REQUEST)

class CheckBookingSlotAvail(APIView):
    def post(self,request):
        print('i am here')
        res = []
        if isinstance(request.data,list):
            model_service_data=[]
            cameraman_service_data=[]
            studio_service_data=[]
            for slot in request.data:
                if slot['service_type']=='MODELS':
                    model_slot_obj = {'start':slot['start'],'end':slot['end']
                    ,'date':slot['date'],'weekday':slot['weekday'],'service_type':slot['service_type'],'service_id':slot['service_id']}
                    model_service_data.append(model_slot_obj)
                
                if slot['service_type']=='PHOTOGRAPHERS':
                    cameraman_slot_obj = {'start':slot['start'],'end':slot['end']
                    ,'date':slot['date'],'weekday':slot['weekday'],'service_id':slot['service_id']}
                    cameraman_service_data.append(cameraman_slot_obj)
            m_data_res = checkServiceBookingListAvail(model_service_data)
            print(m_data_res,'here')
            res.append(m_data_res)
            # res.append(camer)
            # res.append(stud)
            return custom_response(res,'','',HTTP_200_OK)
        else:
            #CHANGE4
            #use bookability can we => here below
            status = checkCoreBookingAv(request.data)
            obj={'start':request.data['start'],'end':request.data['end'],'date':request.data['date'],'status':status}
            return custom_response(obj,'','',HTTP_200_OK)




# class BookingsList(APIView):
#     """
#     List all bookings.
#     """
#     queryset = Booking.objects.all()
#     serializer_class = BookingSerializer
#     filter_backends = [DjangoFilterBackend]
#     filterset_fields = ['date']

#     def get(self, request, format=None):
#         print(self.kwargs,'herekwargs')
#         # bookings = Booking.objects.all()
#         serializer = BookingSerializer(bookings, many=True)
#         return custom_response(serializer.data, '', message='', status_code=HTTP_200_OK)




# class CreateBooking(APIView):
#     """
#     CREATE a booking instance.
#     """
      
#     def post(self, request, format=None):
#         if not isinstance(request.data,list):
#             obj = createBooking(request.data)
#             print(obj,'obj here')
#             if obj['data']['created']:
#                 return custom_response(obj, errors='', message='Successfully booking created', status_code=HTTP_200_OK)
#             else:
#                 data = {'reason': obj['data']['reason'],'created': False}
#                 return custom_response(data, errors=obj['data']['reason'], message=obj['data']['reason'], status_code=HTTP_400_BAD_REQUEST)

#         else:
#             result = []
#             model_service_data=[]
#             cameraman_service_data=[]
#             studio_service_data=[]
#             for slot in request.data:
#                 if slot['service_type']=='MODELS':
#                     model_slot_obj = {'start':slot['start'],'end':slot['end']
#                     ,'date':slot['date'],'weekday':slot['weekday'],'service_type':slot['service_type'],'service_id':slot['service_id']}
#                     model_service_data.append(model_slot_obj)
#             m_data_res = checkServiceBookingListAvail(model_service_data)
#             print('m_data_res',m_data_res)
#             final_list = m_data_res#here add more to this list
#             booked_data_list=[]
#             print(final_list,'final_list')
#             failed_slots=[]
#             for obj in final_list:
#                 print('objhere',obj)
#                 if obj['status']==False:
#                     print('herestatu',obj,obj['status']==False)
#                     failed_slots.append(obj)    
#             print(failed_slots,'failed_slots')
#             if len(failed_slots)<=0:
#                 for data in request.data:
#                     res = prior_booking_checks(data)
#                     if res['data']['proceed']:
#                         booking_data = createBooking(data)
#                         booked_data_list.append(booking_data)
#                         result.append(booking_data)
#                         return custom_response(result, errors='', message='Successfully booking created', status_code=HTTP_200_OK)
#                     else:
#                         return custom_response(res['data'], errors=res['data']['reason'], message=res['data']['reason'], status_code=HTTP_400_BAD_REQUEST)

#             else:
#                 return custom_response(failed_slots, errors='Booking Aborted', message='booking they can not be booked are given in data', status_code=HTTP_200_OK)






























# class CheckBookingSlot(APIView):
#     def post(self,request):
#         data = request.data
#         #get all the bookings for that day for that model
#         s_start, s_end, date, weekday, date, service_id, service_type = data['start'],data['end'],data['date'],data['weekday'],data['date'],data['service_id'],data['service_type']
#         s_start =  datetime.strptime(s_start, '%H:%M:%S').time()
#         if service_type == 'MODEL':
#             # model_service=Model.objects.get(pk=service_id)
#             service_day_slots = model_WeekdayOpeningPeriod.objects.filter(service_id=service_id,weekday=weekday)
#             day_bookings = Booking.objects.filter(date=date,
#                                                   model_service=service_id,
#                                                   service_type=service_type)\
#                 .filter(state='accepted' and 'booking-request')
#             # print('day_bookings',day_bookings)
#             if day_bookings:
#                 bs_list=[]
#                 for booking in day_bookings:
#                     bs_list.append(booking.slot)
#                 if bs_list:
#                     avail_slots = [x for x in service_day_slots if x not in day_bookings]
#                     #put in function
#                     for slot in avail_slots:
#                         print(slot,type(slot),type(slot.start),slot.start)
#                         if s_start > slot.start:
#                             print("b_slot < slot['start']")
#             return custom_response('', '', 'success', status_code=HTTP_204_NO_CONTENT)
#         else:
#             return custom_response('', 'Not Yet Implemented!', 'Not Yet Implemented', status_code=HTTP_200_OK)


# class CheckBookingSlot(APIView):
#     def post(self,request):
#         data = request.data
#         #get all the bookings for that day for that model
#         s_start, s_end, date, weekday, date, service_id, service_type = data['start'],data['end'],data['date'],data['weekday'],data['date'],data['service_id'],data['service_type']
#         s_start =  datetime.strptime(s_start, '%H:%M:%S').time()
#         if service_type == 'MODEL':
#             # model_service=Model.objects.get(pk=service_id)
#             service_day_slots = model_WeekdayOpeningPeriod.objects.filter(service_id=service_id,weekday=weekday)
#             day_bookings = Booking.objects.filter(date=date,
#                                                   model_service=service_id,
#                                                   service_type=service_type)\
#                 .filter(state='accepted' and 'booking-request')
#             # print('day_bookings',day_bookings)
#             if day_bookings:
#                 bs_list=[]
#                 for booking in day_bookings:
#                     bs_list.append(booking.slot)
#                 if bs_list:
#                     avail_slots = [x for x in service_day_slots if x not in day_bookings]
#                     #put in function
#                     for slot in avail_slots:
#                         print(slot,type(slot),type(slot.start),slot.start)
#                         if s_start > slot.start:
#                             print("b_slot < slot['start']")
#             return custom_response('', '', 'success', status_code=HTTP_204_NO_CONTENT)
#         else:
#             return custom_response('', 'Not Yet Implemented!', 'Not Yet Implemented', status_code=HTTP_200_OK)



