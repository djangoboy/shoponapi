from rest_framework import permissions
from booking.models import (
    Booking
    # Vendor_businessLocation
)
from rest_framework.status import HTTP_404_NOT_FOUND
from rest_framework.permissions import BasePermission, SAFE_METHODS
from config.jsonrand import custom_response
from vendor.models import Vendor_Info



class UpdateBooking_Permission(permissions.BasePermission):
    # message = "This booking does not seem to belong to the logged in user hence not authorized to update this booking's data.."
    message = "You are not authorised to update this booking's data.."

    def has_object_permission(self, request, view, obj):
        print(obj,'hereperm',self,view,request,view.kwargs['pk'])
        booking = Booking.objects.get(pk=view.kwargs['pk'])
        # print('--> inside permission classes',booking.user == request.user,'-->',request.user ,booking.vendor,'<--',Vendor_Info.objects.get(user=request.user),'<==')
        if booking.user == request.user or booking.vendor == Vendor_Info.objects.get(user=request.user):
            return True
        else:
            return False 
    

# class UpdateBooking_by_vendor_Permission(permissions.BasePermission):
#     message = "You not authorised vendor for this booking data to update this booking's data.."

#     def has_object_permission(self, request, view, obj):
#         print(obj,'hereperm',self,view,request)
#         booking = Booking.objects.get(pk=view.kwargs['pk'])
#         return booking.vendor == Vendor_Info(user=request.user)
    
    
    
    
    # def has_permission(self, request, view):
    #     # return request.user and request.user.is_staff
    #     return False

