from rest_framework import serializers
from config.jsonrand import custom_response
from rest_framework.status import HTTP_200_OK
from rest_framework.serializers import (
    ValidationError
  
)
from booking.utilities import SlotTimeAvailCheck,checkBookablity
from .models import Booking
# from model.models import Service as Model
from cameraman.models import Service as Cameraman
# from studio.models import Service as Studio


class BookingLimitedInfoSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""
    # user = serializers.SlugRelatedField(
    #                             many=False,
    #                             read_only=True,
    #                             slug_field='username'
    #                           )
    # service = serializers.SlugRelatedField(
    #                             many=False,
    #                             read_only=True,
    #                             slug_field='first_name'
    #                           )

    class Meta:
        model = Booking
        # fields = ['name', 'description', 'image', 'websiteURL', 'price', 'business_location']
        fields = ['id','date']


class BookingSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""
    user = serializers.SlugRelatedField(
                                many=False,
                                read_only=True,
                                slug_field='username'
                              )
    service = serializers.SlugRelatedField(
                                many=False,
                                read_only=True,
                                slug_field='first_name'
                              )

    class Meta:
        model = Booking
        # fields = ['name', 'description', 'image', 'websiteURL', 'price', 'business_location']
        fields = '__all__'


#this is for email notification
class BookingEmailNotifSerializer(serializers.ModelSerializer):
    

    class Meta:
        model = Booking
        # fields = ['name', 'description', 'image', 'websiteURL', 'price', 'business_location']
        fields = ['state','id','user','vendor','model_service','cameraman_service','studio_service','reporting_address','decline_reason']


class UpdateBookinsPartiallySerializer(serializers.ModelSerializer):
    """Updates status, start, end, reporting address"""
    class Meta:
        model = Booking
        fields = ['start','end','state','date','reporting_address','decline_reason','model_service','cameraman_service','studio_service']

    def update(self, instance, validated_data):
        print(validated_data,'--',type(validated_data))
        booking = Booking.objects.get(pk=instance.pk)
        slot = {}
        # if validated_data['model_service'] or  validated_data['model_service'] or  validated_data['model_service']
        # if booking.service_type=='MODELS':
            
        # validated_data.get('start', instance.start)

        model_service = validated_data.get('model_service', None)
        cameraman_service = validated_data.get('cameraman_service', None)
        studio_service = validated_data.get('studio_service', None)
        state = validated_data.get('state', None)
        decline_reason = validated_data.get('decline_reason', None)
        v_start = validated_data.get('start', booking.start)
        v_end = validated_data.get('end', booking.end)
        v_date = validated_data.get('date', booking.date)


        print(cameraman_service,type(cameraman_service),'<== here it is int or')

        b_model_service_id,b_cameraman_service_id,b_studio_service_id=None,None,None
        if not model_service and not cameraman_service and not studio_service:
            if booking.model_service and booking.service_type=='MODELS':
                b_model_service_id = booking.model_service.pk
            if booking.cameraman_service and booking.service_type=='PHOTOGRAPHERS':
                b_cameraman_service_id = booking.cameraman_service.pk
            if booking.studio_service and booking.service_type=='STUDIOS':
                b_studio_service_id = booking.studio_service.pk
        else:
            # instance.state = 'booking-request'
            if model_service and booking.service_type=='MODELS':
                b_model_service_id = model_service.pk
            elif cameraman_service and booking.service_type=='PHOTOGRAPHERS':
                b_cameraman_service_id = cameraman_service.pk
            elif studio_service and booking.service_type=='STUDIOS':
                b_studio_service_id=studio_service.pk
            # booking.state = 'booking-request'
            # booking.save()
            else:
                raise ValidationError('Booking service_type and change service_type does not match; So rejected.')
        # validated_data['start'].strftime("%H:%M:%S") 
        # validated_data['end'].strftime("%H:%M:%S")
        # validated_data['date'].weekday()+1,
        # validated_data['date'],booking.service_type,
        # b_model_service_id,
        # b_cameraman_service_id,
        # b_studio_service_id
        slot['start'],slot['end'],slot['weekday'],slot['date'],slot['service_type'],slot['model_service_id'],slot['cameraman_service_id'],slot['studio_service_id'] =  v_start.strftime("%H:%M:%S"),v_end.strftime("%H:%M:%S"),v_date.weekday()+1,v_date,booking.service_type,b_model_service_id,b_cameraman_service_id,b_studio_service_id
        # print(slot['start'],slot['end'],slot['weekday'],slot['service_type'],slot['model_service_id'],slot['cameraman_service_id'],slot['studio_service_id'],'here are the params')
        print('result of slot avail =>',SlotTimeAvailCheck(slot))
        # if not SlotTimeAvailCheck(slot):
        #     raise ValidationError('this time slot is not available')
        print('result checkbookability==>',checkBookablity(slot))

        if checkBookablity(slot)=='booking-request' and not validated_data['state']=='approved' and not validated_data['state']=='canceled' and not validated_data['state']=='declined':
            raise ValidationError('This Timing is Already Booked')

        if checkBookablity(slot)=='approved' and not validated_data['state']=='canceled' and not validated_data['state']=='declined':
            raise ValidationError('The status is already "booking-request"')

        if state == 'declined':
          if decline_reason:   
              instance.decline_reason = decline_reason
          else:
              raise ValidationError('Please provide a decline reasons')


        instance.start = validated_data.get('start', instance.start)
        instance.end = validated_data.get('end', instance.end)
        instance.date = validated_data.get('date', instance.date)
        instance.state = validated_data.get('state', instance.state)
        instance.reporting_address = validated_data.get('reporting_address', instance.reporting_address)        

        if b_model_service_id:
            instance.model_service = Model.objects.get(pk=b_model_service_id)
            # instance.vendor = instance.model_service
            vendor = instance.model_service.business_location.vendor_name
        if b_cameraman_service_id:
            bc_sid = Cameraman.objects.get(pk=b_cameraman_service_id)
            print('before ==>',bc_sid)
            instance.cameraman_service = bc_sid
            print('after ==>',instance.cameraman_service)
            
            vendor = instance.cameraman_service.business_location.vendor_name

        if b_studio_service_id:
            instance.studio_service = Studio.objects.get(pk=b_studio_service_id)        
            vendor = instance.studio_service.business_location.vendor_name

        instance.save()
        return instance


    
# class UpdateBookinsPartiallySerializer(serializers.ModelSerializer):
#     """Updates status, start, end, reporting address"""
#     class Meta:
#         model = Booking
#         fields = ['start','end','state','date','reporting_address']

#     def update(self, instance, validated_data):
#         print(validated_data,'--',type(validated_data))
#         booking = Booking.objects.get(pk=instance.pk)
#         slot = {}
#         b_model_service_id,b_cameraman_service_id,b_studio_service_id=None,None,None
#         if booking.model_service:
#             b_model_service_id = booking.model_service.pk
#         if booking.cameraman_service:
#             b_cameraman_service_id = booking.cameraman_service.pk
#         if booking.studio_service:
#             b_studio_service_id = booking.studio_service.pk

#         slot['start'],slot['end'],slot['weekday'],slot['date'],slot['service_type'],slot['model_service_id'],slot['cameraman_service_id'],slot['studio_service_id'] = validated_data['start'].strftime("%H:%M:%S"),validated_data['end'].strftime("%H:%M:%S"),validated_data['date'].weekday()+1,validated_data['date'],booking.service_type,b_model_service_id,b_cameraman_service_id,b_studio_service_id
#         # print(slot['start'],slot['end'],slot['weekday'],slot['service_type'],slot['model_service_id'],slot['cameraman_service_id'],slot['studio_service_id'],'here are the params')
#         print('result of slot avail =>',SlotTimeAvailCheck(slot))
#         if not SlotTimeAvailCheck(slot):
#             raise ValidationError('this time slot is not available')
#         print('result checkbookability==>',checkBookablity(slot))

#         if checkBookablity(slot)=='booking-request' and not validated_data['state']=='approved' and not validated_data['state']=='canceled' and not validated_data['state']=='declined':
#             raise ValidationError('This Timing is Already Booked')

#         if checkBookablity(slot)=='approved' and not validated_data['state']=='canceled' and not validated_data['state']=='declined':
#             raise ValidationError('This Timing is Already Booked Confirmed')



#         instance.start = validated_data.get('start', instance.start)
#         instance.end = validated_data.get('end', instance.end)
#         instance.date = validated_data.get('date', instance.date)
#         instance.state = validated_data.get('state', instance.state)
#         instance.reporting_address = validated_data.get('reporting_address', instance.reporting_address)        
#         # if not slot['model_service']:
#         #     instance.model_service = booking.model_service.pk
#         # if not slot['cameraman_service']:
#         #     instance.cameraman_service = booking.cameraman_service.pk
#         # if not slot['model_service']:
#         #     instance.studio_service = booking.studio_service.pk
#         instance.save()

#         return instance