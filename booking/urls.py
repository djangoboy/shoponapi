from django.conf.urls import url
from .views import BookingsList, BookingDetail, CreateBooking,CheckBookingSlotAvail, VendorBookingsList,BookingsListForLOUser, UpdateBookingStatus

app_name = 'booking'

urlpatterns = [
    url(r'^(?P<pk>[0-9]+)$', BookingDetail.as_view()),
    url(r'^user_bookings', BookingsList.as_view()),
    # url(r'^update/booking/(?P<pk>[0-9]+)$', UpdateBookingStatus.as_view()),
    #BookingsListForLoggedOutUser
    url(r'^all', BookingsListForLOUser.as_view()),
    # url(r'^user/all', UserBookingsList.as_view()),
    url(r'^create_booking', CreateBooking.as_view()),
    url(r'^vendor_bookings/all', VendorBookingsList.as_view()),
    # url(r'^check_slot_booking_avail', CheckBookingSlotAvail.as_view()),
]
