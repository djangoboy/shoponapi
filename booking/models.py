import time
import os
from django.db import models
from django.conf import settings
from accounts.models import User
# from model.models import Service, service_WeekdayOpeningPeriod
from utilities import uniqueCodeGenerator
from cameraman.models import Service as Cameraman
# from model.models import Service as Model
# from studio.models import Service as Studio
from django.db.models.signals import post_save
from django.dispatch import receiver
# from model.models import service_pricing as ModelServicePricing
from cartinfo.models import CartInfo
# from vendor.models import Vendor_Info
from django_mysql.models import JSONField
# from booking.tasks import booking_onbooking_event_lis


BOOKING_TYPE=(
    (None,'NONE'),
    ('HOURLY','HOURLY'),
    ('DAILY','DAILY'),
)
BOOKING_STATE_CHOICES = (
    ('booking-request', 'Booking-Request'),#
    # ('booked', 'Booked'),#
    ('declined', 'Declined'),
    ('canceled', 'Canceled'),
    ('approved', 'Approved'))

SERVICE_TYPE=(
("MODELS","MODELS"),
("PHOTOGRAPHERS","PHOTOGRAPHERS"),
("STUDIOS","STUDIOS"),
)

# Create your models here.
VENDOR_EVENT_TYPES = (
    ('ONBOOKING','ONBOOKING'),
    ('ONBOOKINGSTATUSCHANGED','ONBOOKINGSTATUSCHANGED'),
    ('UPCOMING','UPCOMING'),
    ('BOOKINGUPDATED','BOOKINGUPDATED'),
    ('SERVICEPROVIDERINFOUPDATED','SERVICEPROVIDERINFOUPDATED'),
    ('BIZLOCINFOUPDATED','BIZLOCINFOUPDATED'),
    ('SERVICEPROVIDERINFOOUPDATED','SERVICEPROVIDERINFOOUPDATED')
)

USER_EVENT_TYPES = (
    ('ONBOOKING','ONBOOKING'),
    ('UPCOMING','UPCOMING'),
    ('ONBOOKINGSTATUSCHANGED','ONBOOKINGSTATUSCHANGED'),
)

# class CustBillingAdd(models.Model):
#     address_line1 = models.CharField(max_length=255,null=True,blank=True)
#     address_line2 = models.CharField(max_length=255,null=True,blank=True)    
#     city = models.CharField(max_length=100,null=True,blank=True)
#     state = models.CharField(max_length=100,null=True,blank=True)
#     zip = models.CharField(max_length=10,null=True,blank=True)
    

# class CustShippingAdd(models.Model):
#     address_line1 = models.CharField(max_length=255,null=True,blank=True)
#     address_line2 = models.CharField(max_length=255,null=True,blank=True)    
#     city = models.CharField(max_length=100,null=True,blank=True)
#     state = models.CharField(max_length=100,null=True,blank=True)
#     zip = models.CharField(max_length=10,null=True,blank=True)
from rest_framework.serializers import (
    ValidationError
  
)    
class Booking(models.Model):
    booking_uid = models.CharField(unique=True,max_length=100)
    reporting_address = JSONField(null=True, blank=True) 
    decline_reason =  models.CharField(max_length=300,null=True,blank=True) 
    booking_type = models.CharField(max_length=100,choices=BOOKING_TYPE,null=True,blank=True)
    # vendor = models.ForeignKey(Vendor_Info,on_delete = models.SET_NULL,null=True,blank=True)
    user = models.ForeignKey(User, related_name='bookings',null=True, blank=True, on_delete=models.CASCADE)
    service_type = models.CharField(max_length=50,choices=SERVICE_TYPE,default=None)
    # model_service = models.ForeignKey(Model, related_name='model', on_delete=models.SET_NULL,null=True,blank=True)
    cameraman_service = models.ForeignKey(Cameraman, related_name='cameraman', on_delete=models.SET_NULL,null=True,blank=True)
    # studio_service = models.ForeignKey(Studio, related_name='studio', on_delete=models.SET_NULL,null=True,blank=True)
    project_description = models.TextField(null=True,blank=True)
    #CHANGED => booking_time is changed from DateTimeField To DateField
    date = models.DateField(blank=True,null=True)
    state = models.CharField(choices=BOOKING_STATE_CHOICES, max_length=255,null=True,blank=True)
    review_mark = models.FloatField(blank=True, default=0.0)
    customer_name = models.CharField(max_length=50,null=True,blank=True)
    customer_phone_number = models.CharField(max_length=50,null=True,blank=True)
    customer_email = models.EmailField(max_length=100, unique=False,null=True,blank=True)
    
    # customer_email = models.EmailField(max_length=100, unique=True)
    b_address_line1 = models.CharField(max_length=255,null=True,blank=True)
    b_address_line2 = models.CharField(max_length=255,null=True,blank=True)    
    b_city = models.CharField(max_length=100,null=True,blank=True)
    b_state = models.CharField(max_length=100,null=True,blank=True)
    b_zip = models.CharField(max_length=10,null=True,blank=True)

    s_address_line1 = models.CharField(max_length=255,null=True,blank=True)
    s_address_line2 = models.CharField(max_length=255,null=True,blank=True)    
    s_city = models.CharField(max_length=100,null=True,blank=True)
    s_state = models.CharField(max_length=100,null=True,blank=True)
    s_zip = models.CharField(max_length=10,null=True,blank=True)
    # billing_addres = models.OneToOneField(CustBillingAdd,on_delete=models.CASCADE)
    customer_description = models.TextField(null=True,blank=True)
    customer_reminder = models.CharField(max_length=255,null=True,blank=True)
    customer_agree = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    start = models.TimeField(null=True,blank=True)
    end = models.TimeField(null=True,blank=True)


    def __str__(self):
        return '{start} to {end}'.format(
            start=self.start,
            end=self.end
            )
    def save(self, *args, **kwargs):
        #if booking exist on the basis of those five elements:: make sure it's unique
            # raise ValidationError('IS IT VALIATION ERROR')
            # for r_data in datas:
            #     st,si,s,e,d = r_data['service_type'],r_data['service_id'],\
            #     r_data['start'],r_data['end'],r_data['date']
            #     # obj = {'start':s,'end':e,'service_type':st,'service_id':si,'date':d}
            #     t_obj = (s,e,st,si,d)
            #     r_data_list.append(t_obj)
            # print(r_data_list,'r_data_list')
            # print(set(r_data_list),'set(r_data_list)',len(list(set(r_data_list)))!= len(datas))                
            # if len(list(set(r_data_list)))!= len(datas):
    
        self.booking_uid = 'Book-' + uniqueCodeGenerator()
        return super().save(*args, **kwargs)
        

@receiver(post_save, sender=Booking)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        if instance.service_type=='MODELS':
            service = instance.model_service
            print(service,type(service),'here it is')
            sp = service.s_price_type
            instance.booking_type = sp
            instance.vendor = service.business_location.vendor_name
            # instance.project_description = service.description
            instance.save()
            user = instance.user
            booking_date = instance.date
            model_service_id = instance.model_service.pk
            start=instance.start
            end=instance.end
            print('jvendorhere',service.business_location.vendor_name)
            # vendor = instance.
            c1 = CartInfo.objects.filter(user=user,booking_date=booking_date,model_service_id=model_service_id,start=start,end=end)
            c1.delete()
            print('=> here it is before we delete; Cart',c1)

            #delete from cart of the user
        if instance.service_type=='PHOTOGRAPHERS':
            service = instance.cameraman_service
            print(service,type(service),'here it is')
            sp = service.s_price_type
            instance.booking_type = sp
            instance.vendor = service.business_location.vendor_name
            
            print('jvendorhere',service.business_location.vendor_name)

            # instance.project_description = service.description
            instance.save()
            user = instance.user
            booking_date = instance.date
            cameraman_service_id = instance.cameraman_service.pk
            start=instance.start
            end=instance.end
            c2 = CartInfo.objects.filter(user=user,booking_date=booking_date,cameraman_service_id=cameraman_service_id,start=start,end=end)
            c2.delete()
            #delete from cart of the user
        if instance.service_type=='STUDIOS':
            service = instance.studio_service
            print(service,type(service),'here it is')
            sp = service.s_price_type
            instance.vendor = service.business_location.vendor_name
            instance.booking_type = sp
            # instance.project_description = service.description
            instance.save()
            user = instance.user
            booking_date = instance.date
            studio_service_id = instance.studio_service.pk
            print('jvendorhere',service.business_location.vendor_name)
            start=instance.start
            end=instance.end
            c3 = CartInfo.objects.filter(user=user,booking_date=booking_date,studio_service_id=studio_service_id,start=start,end=end)
            c3.delete()
            #delete from cart of the user
        
        # print(instance.user)
        # booking_onbooking_event_lis.delay(instance.pk,instance.state,instance.updated_at,'ONBOOKING',instance.user.email,instance.start,instance.end,instance.date)
     
    
    # else:
        # from test_queries.tasks import create_random_user_accounts
        # if instance.state=='booking-request' or instance.state=='approved':

        # create_random_user_accounts.delay(1)
    #if updated:
        #first check if the slot is available and bookable

'''
from model.models import Service as Model
from studio.models import Service as Studio
from cameraman.models import Service as Cameraman

ms = Model.objects.all()
ss = Studio.objects.all()
cs = Cameraman.objects.all()

for service in ms:
    if not service.business_location:print('ms',service.pk)

for service in ss:
    if not service.business_location:print('ss',service.pk)

for service in cs:
    if not service.business_location:print('cs',service.pk)


bookings = Booking.objects.all()
model = Model.objects.get(pk=45)
for b in bookings:
    print(b.service_type)
    if b.service_type == 'MODELS':
       b.vendor = b.model_service.business_location.vendor_name
       b.save()
    if b.service_type == 'PHOTOGRAPHERS':
        b.vendor = b.cameraman_service.business_location.vendor_name
        b.save()
    if b.service_type == 'STUDIOS':
        b.vendor = b.studio_service.business_location.vendor_name
        b.save()





from model.models import Service as Model
from studio.models import Service as Studio
from cameraman.models import Service as Cameraman
from booking.models import Booking

bookings = Booking.objects.all()
main_list = []
for b in bookings:
    obj_tuple = (b.start,b.end,b.date,b.model_service.pk)
    main_list.append(obj_tuple)




'''
    #     check_slot_availibility_for_service_list
    #     if instance.state=='book'
    #     check if the slot is available
    # if updated:
    #     if instance.state=='book'
    #     check if the slot is available
   