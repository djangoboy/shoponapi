from django.utils.timezone import now
from rest_framework import serializers
from rest_framework.serializers import (
    ModelSerializer,
    Serializer,
    CharField,
    EmailField,
    ValidationError,
    HyperlinkedIdentityField,
    BooleanField,
)

from django.contrib.auth import get_user_model
from django.db.models import Q
# from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from config.jsonrand import custom_response
from .tokens import account_activation_token
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from .utils import generateToken, decodeToken
from config.settings import Base_URL
from accounts.models import Account, DeliveryAdd
import random

# User = get_user_model()
EMAIL_HOST_USER = 'connect.phobiz@gmail.com'


class RegisterSerializer(ModelSerializer):
    first_name = CharField(label='First name')
    # middle_name = CharField(label='Middle name')
    last_name = CharField(label='Last name')
    email = EmailField(label='E-mail')
    email_confirmation = EmailField(label='Confirm E-mail')
    password = CharField(label='Password')
    password_confirmation = CharField(label='Confirm Password')
    tos_accept_confirmation = BooleanField(label='TOS Accept')

    class Meta:
        model = User
        fields = [
            'first_name',
            # 'middle_name',
            'last_name',
            'email',
            'email_confirmation',
            'password',
            'password_confirmation',
            'tos_accept_confirmation',
        ]
        extra_kwargs = {'password':
                            {'write_only': True}
                        }

    def create(self, validated_data):

        first_name = validated_data['first_name']
        last_name = validated_data['last_name']
        email = validated_data['email']
        password = validated_data['password']
        try:
            username = str(email.split('@')[0])
        except:
            username = str(email.split('@')[0]) + '-' + str(random.randint(100000, 992726))
        user = User(
            first_name=first_name,
            last_name=last_name,
            username=username,
            email=email,
        )
        user.set_password(password)
        user.is_active = False
        user.save()
        print('user just got craeted and pk',user.pk)
        # user.account.send_activation_email()
        mail_subject = 'Activate your Phobiz account.'
        obj = {
            'user': user,
            'domain': Base_URL,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
            'token': account_activation_token.make_token(user),
        }

        message = render_to_string('accounts/acc_activation_email.html', obj)
        send_email = EmailMessage(
            mail_subject, message, from_email=EMAIL_HOST_USER, to=[email]
        )
        send_email.send()
        validated_data['token_info']={'token':obj['token'],'status':'unused','user_pk':user.pk}
        return validated_data

    def validate_email_confirmation(self, email_confirmation):
        print('validate_email_confirmation ran')
        data = self.get_initial()
        email = data.get('email')
        # email_confirmation = data.get('email_confirmation')
        email_confirmation = email_confirmation
        if email != email_confirmation:
            raise ValidationError('Email dose not matched!')
        qs = User.objects.filter(email__iexact=email_confirmation)
        if qs.exists():
            raise ValidationError('This email is already registered before!')
        return email_confirmation



    def validate_password_confirmation(self, password_confirmation):
        print('did it run after email confirmation method above or after')
        data = self.get_initial()
        password = data.get('password')
        password_confirmation = password_confirmation
        if password != password_confirmation:
            raise ValidationError('Passwords dose not matched!')
        return password_confirmation


    def checktokenval(token):
        return 'knowledge!'


'''
# test token making for a user
from django.contrib.auth import get_user_model
from accounts.tokens import account_activation_token
users = get_user_model()
u1 = users.objects.first()
account_activation_token.make_token(u1)
account_activation_token.check_token(u1, token)
'''


class LoginSerializer(ModelSerializer):
    email = EmailField(label='E-mail', required=False, allow_blank=True)
    token = CharField(allow_blank=True, read_only=True)

    class Meta:
        model = User
        fields = [
            'email',
            'password',
            'token',

        ]

    def validate(self, data):
        user = None
        email = data.get('email', None)
        password = data["password"]
        if not email:
            raise serializers.ValidationError({
                'email': 'Email is required'
            })
        user = User.objects.filter(
            Q(email=email)
        ).distinct()

        if user.exists() and user.count() == 1:
            user = user.first()
        else:
            raise serializers.ValidationError({
                'Errors': "Incorrect credentials, please try again"
            })
        if user:
            if not user.check_password(password):
                raise serializers.ValidationError({
                    'Errors': "Incorrect credentials, please try again"
                })
                # raise ValidationError("Incorrect credentials, please try again")
            if user.is_active is False:
                raise serializers.ValidationError({
                    'Errors': "Mail not Verified, please Verify and try again"
                })
                # raise ValidationError("errors:"+"Mail not Verified, please Verify and try again")
            token = generateToken(user)
            data['token'] = token

        return data

class UserBaseSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id','first_name','last_name']


class EachUserSerializer(serializers.ModelSerializer):
#     # username = serializers.CharField(source='user.username')

    class Meta:
        model = DeliveryAdd
        # fields = ['id','street_address','landmark','city','state','pincode','contact_no','email']
        fields='__all__'



class ProfileSerializer(ModelSerializer):
    # user = UserBaseSerializer(many=True)
    # user = UserBaseSerializer()
    # # test = serializers.SerializerMethodField()
    # user_id = serializers.SerializerMethodField()
    # def get_user_id(self, obj):
    #     print(dir(obj),'--',obj,'here is misterious obj',self,'--',dir(self))
    #     return obj.user_id
    # del_detail = EachUserSerializer(many=True,read_only=True)

    class Meta:
        model = Account
        fields = [
            'first_name',
            'last_name',
            'street_address',
            'landmark',
            'city',
            'state',
            'image',
        ]
        # fields = '__all__'

    def from_native(self, data, files):
        data['vendor_id'] = self.context['vendor_id']
        return super(ProfileSerializer, self).from_native(data, files)

        # fields = ('username',)
# class ProfileSerializer(ModelSerializer):
#     # user = UserBaseSerializer(many=True)
#     user = UserBaseSerializer()
#     # test = serializers.SerializerMethodField()
#     user_id = serializers.SerializerMethodField()
#     def get_user_id(self, obj):
#         print(dir(obj),'--',obj,'here is misterious obj',self,'--',dir(self))
#         return obj.user_id
#     del_detail = EachUserSerializer(many=True,read_only=True)

#     class Meta:
#         model = Account
#         fields = [
#             # 'first_name',
#             'middle_name',
#             'username',
#             # 'is_vendor',
#             'user_id',
#             'user',
#             'del_detail'
#         ]
#         # fields = '__all__'

#     def from_native(self, data, files):
#         data['vendor_id'] = self.context['vendor_id']
#         return super(ProfileSerializer, self).from_native(data, files)

class UpdateAccountSerializer(ModelSerializer):
    class Meta:
        model = Account
        # fields = ['registered_ip',]
        # fields = "__all__"
        fields = [
            'first_name',
            'last_name',
            'street_address',
            'landmark',
            'city',
            'state',
            'image',
        ]

# class UpdateSerializer(ModelSerializer):
#     first_name = CharField(read_only=True)
#     last_name = CharField(read_only=True)
#     email = CharField(read_only=True)

#     class Meta:
#         model = Account
#         fields = [
#             'first_name',
#             'last_name',
#             'middle_name',
#             'email',
#             'gender',
#             'country',
#             'phone_number',
#             'image',
#             'address',
#             'address_opt',
#             'address2',
#             'zip_code',
#             'updated_on',
#             'comments',
#             'updated_by',
#             'registered_ip'
#         ]

#     def validate(self, data):
#         gender = data.get('gender')
#         country = data.get('country')
#         address = data.get('address')
#         address_opt = data.get('address_opt')
#         zip_code = data.get('zip_code')
#         phone_number = data.get('phone_number')
#         image = data.get('image')
#         if not phone_number:
#             raise ValidationError('Please enter your phone number!')
#         if not gender:
#             raise ValidationError('Please select your gender!')
#         if not country:
#             raise ValidationError('Please enter your country!')
#         if not address:
#             raise ValidationError('Please enter your address!')
#         if not address_opt:
#             raise ValidationError('Please select your Type!')
#         if not address:
#             raise ValidationError('Please enter your address!')
#         if not zip_code:
#             raise ValidationError('Please enter your Zip Code!')
#         if not image:
#             raise ValidationError('You should upload an image!')
#         return data


class ChangePasswordSerializer(Serializer):
    old_password = CharField(label='Old Password', required=True)
    new_password = CharField(label='New Password', required=True)
    new_password_confirmation = CharField(label='Confirm Password', required=True)

    def validate_new_password_confirmation(self, new_password_confirmation):
        data = self.get_initial()
        new_password = data.get('new_password')
        new_password_confirmation = new_password_confirmation
        if new_password != new_password_confirmation:
            raise ValidationError('Passwords dose not matched!')
        return new_password_confirmation


class GeneralUserSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""
    vendor = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='title'
    )

    class Meta:
        model = User
        fields = ['email', 'username', 'first_name', 'last_name', 'model', 'image', 'phone_number']




# from django.utils.timezone import now
# from rest_framework import serializers
# from rest_framework.serializers import (
#     ModelSerializer,
#     Serializer,
#     CharField,
#     EmailField,
#     ValidationError,
#     HyperlinkedIdentityField,
#     BooleanField,
# )

# from django.contrib.auth import get_user_model
# from django.db.models import Q
# # from django.contrib.contenttypes.models import ContentType
# from django.contrib.sites.shortcuts import get_current_site
# from django.utils.encoding import force_bytes, force_text
# from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
# from django.template.loader import render_to_string
# from rest_framework.status import HTTP_200_OK

# from config.jsonrand import custom_response
# from .tokens import account_activation_token
# from django.contrib.auth.models import User
# from django.core.mail import EmailMessage
# from .utils import generateToken, decodeToken
# from config.settings import Base_URL
# from accounts.models import *
# import random

# # User = get_user_model()
# EMAIL_HOST_USER = 'connect.shopon@gmail.com'


# class RegisterSerializer(ModelSerializer):
#     first_name = CharField(label='First name')
#     # middle_name = CharField(label='Middle name')
#     last_name = CharField(label='Last name')
#     email = EmailField(label='E-mail')
#     email_confirmation = EmailField(label='Confirm E-mail')
#     password = CharField(label='Password')
#     password_confirmation = CharField(label='Confirm Password')
#     tos_accept_confirmation = BooleanField(label='TOS Accept')

#     class Meta:
#         model = User
#         fields = [
#             'first_name',
#             # 'middle_name',
#             'last_name',
#             'email',
#             'email_confirmation',
#             'password',
#             'password_confirmation',
#             'tos_accept_confirmation',
#         ]
#         extra_kwargs = {'password':
#                             {'write_only': True}
#                         }

#     def create(self, validated_data):

#         first_name = validated_data['first_name']
#         last_name = validated_data['last_name']
#         email = validated_data['email']
#         password = validated_data['password']
#         try:
#             username = str(email.split('@')[0])
#         except:
#             username = str(email.split('@')[0]) + '-' + str(random.randint(100000, 992726))
#         user = User(
#             first_name=first_name,
#             last_name=last_name,
#             username=username,
#             email=email,
#         )
#         user.set_password(password)
#         user.is_active = False
#         user.save()
#         print('user just got craeted and pk',user.pk)
#         # user.account.send_activation_email()
#         mail_subject = 'Activate your Shopon account.'
#         obj = {
#             'user': user,
#             'domain': Base_URL,
#             'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
#             'token': account_activation_token.make_token(user),
#         }

#         message = render_to_string('accounts/acc_activation_email.html', obj)
#         send_email = EmailMessage(
#             mail_subject, message, from_email=EMAIL_HOST_USER, to=[email]
#         )
#         send_email.send()
#         validated_data['token_info']={'token':obj['token'],'status':'unused','user_pk':user.pk}
#         return validated_data

#     def validate_email_confirmation(self, email_confirmation):
#         print('validate_email_confirmation ran')
#         data = self.get_initial()
#         email = data.get('email')
#         # email_confirmation = data.get('email_confirmation')
#         email_confirmation = email_confirmation
#         if email != email_confirmation:
#             raise ValidationError('Email dose not matched!')
#         qs = User.objects.filter(email__iexact=email_confirmation)
#         if qs.exists():
#             raise ValidationError('This email is already registered before!')
#         return email_confirmation



#     def validate_password_confirmation(self, password_confirmation):
#         print('did it run after email confirmation method above or after')
#         data = self.get_initial()
#         password = data.get('password')
#         password_confirmation = password_confirmation
#         if password != password_confirmation:
#             raise ValidationError('Passwords dose not matched!')
#         return password_confirmation


#     def checktokenval(token):
#         return 'knowledge!'


# '''
# # test token making for a user
# from django.contrib.auth import get_user_model
# from accounts.tokens import account_activation_token
# users = get_user_model()
# u1 = users.objects.first()
# account_activation_token.make_token(u1)
# account_activation_token.check_token(u1, token)
# '''


# class LoginSerializer(ModelSerializer):
#     email = EmailField(label='E-mail', required=False, allow_blank=True)
#     token = CharField(allow_blank=True, read_only=True)

#     class Meta:
#         model = User
#         fields = [
#             'email',
#             'password',
#             'token',

#         ]

#     def validate(self, data):
#         user = None
#         email = data.get('email', None)
#         password = data["password"]
#         if not email:
#             raise serializers.ValidationError({
#                 'email': 'Email is required'
#             })
#         user = User.objects.filter(
#             Q(email=email)
#         ).distinct()

#         if user.exists() and user.count() == 1:
#             user = user.first()
#         else:
#             raise serializers.ValidationError({
#                 'Errors': "Incorrect credentials, please try again"
#             })
#         if user:
#             if not user.check_password(password):
#                 raise serializers.ValidationError({
#                     'Errors': "Incorrect credentials, please try again"
#                 })
#                 # raise ValidationError("Incorrect credentials, please try again")
#             if user.is_active is False:
#                 raise serializers.ValidationError({
#                     'Errors': "Mail not Verified, please Verify and try again"
#                 })
#                 # raise ValidationError("errors:"+"Mail not Verified, please Verify and try again")
#             token = generateToken(user)
#             data['token'] = token

#         return data

# class UserBaseSerializer(ModelSerializer):
#     class Meta:
#         model = User
#         fields = ['id','first_name','last_name']

# class EachUserSerializer(serializers.ModelSerializer):
#     # username = serializers.CharField(source='user.username')

#     class Meta:
#         model = DeliveryAdd
#         fields = '__all__'

#         # fields = ('username',)

# class ProfileSerializer(ModelSerializer):
#     # user = UserBaseSerializer(many=True)
#     user = UserBaseSerializer()
#     # test = serializers.SerializerMethodField()
#     # del_detail = EachUserSerializer(many=True,read_only=True)

#     # user_id = serializers.SerializerMethodField()
#     # def get_user_id(self, obj):
#     #     print(dir(obj),'--',obj,'here is misterious obj',self,'--',dir(self))
#     #     return obj.user_id

#     class Meta:
#         model = Account
#         fields = [
#             # 'first_name',
#             'middle_name',
#             'username',
#             'is_vendor',
#             'user',
#             'del_detail'
#         ]
#         # fields = '__all__'

#     def from_native(self, data, files):
#         data['vendor_id'] = self.context['vendor_id']
#         return super(ProfileSerializer, self).from_native(data, files)


# class UpdateSerializer(ModelSerializer):
#     first_name = CharField(read_only=True)
#     last_name = CharField(read_only=True)
#     email = CharField(read_only=True)

#     class Meta:
#         model = Account
#         fields = [
#             'id',
#             'first_name',
#             'last_name',
#             'middle_name',
#             'email',
#             'gender',
#             'country',
#             'phone_number',
#             'image',
#             'address',
#             'address_opt',
#             'address2',
#             'zip_code',
#             'updated_on',
#             'comments',
#             'updated_by',
#         ]

#     def validate(self, data):
#         gender = data.get('gender')
#         country = data.get('country')
#         address = data.get('address')
#         address_opt = data.get('address_opt')
#         zip_code = data.get('zip_code')
#         phone_number = data.get('phone_number')
#         image = data.get('image')
#         if not phone_number:
#             raise ValidationError('Please enter your phone number!')
#         if not gender:
#             raise ValidationError('Please select your gender!')
#         if not country:
#             raise ValidationError('Please enter your country!')
#         if not address:
#             raise ValidationError('Please enter your address!')
#         if not address_opt:
#             raise ValidationError('Please select your Type!')
#         if not address:
#             raise ValidationError('Please enter your address!')
#         if not zip_code:
#             raise ValidationError('Please enter your Zip Code!')
#         if not image:
#             raise ValidationError('You should upload an image!')
#         return data


# class ChangePasswordSerializer(Serializer):
#     old_password = CharField(label='Old Password', required=True)
#     new_password = CharField(label='New Password', required=True)
#     new_password_confirmation = CharField(label='Confirm Password', required=True)

#     def validate_new_password_confirmation(self, new_password_confirmation):
#         data = self.get_initial()
#         new_password = data.get('new_password')
#         new_password_confirmation = new_password_confirmation
#         if new_password != new_password_confirmation:
#             raise ValidationError('Passwords dose not matched!')
#         return new_password_confirmation


# class GeneralUserSerializer(serializers.ModelSerializer):
#     """Serializers registration requests and creates a new user."""
#     vendor = serializers.SlugRelatedField(
#         many=False,
#         read_only=True,
#         slug_field='title'
#     )

#     class Meta:
#         model = User
#         fields = ['email', 'username', 'first_name', 'last_name', 'model', 'image', 'phone_number']
