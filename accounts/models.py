from django.db import models
from django.conf import settings
from django.db.models.signals import post_save, pre_delete
from django.urls import reverse
from lookup.models import lookupItem
from django.core.mail import send_mail
from django_mysql.models import JSONField

import os
from utilities import uniqueCodeGenerator

User = settings.AUTH_USER_MODEL


GENDER_TYPE= (
    ('MALE','MALE'),
    ('FEMALE','FEMALE'),
)

PICKUPDELIVERYTYPE=(
    ('PICKUP','PICKUP',),
    ('DELIVERY','DELIVERY',),

)
class PickUpAdd(models.Model):
    street_address = models.CharField(max_length=100)
    landmark = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    # landmark = JSONField(default=None,null=True,blank=True)
    pincode = models.IntegerField(max_length=6)

    def __str__(self):
        return self.street_address

class DeliveryAdd(models.Model):
    street_address = models.CharField(max_length=100)
    landmark = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    # landmark = JSONField(default=None,null=True,blank=True)
    pincode = models.IntegerField(max_length=6)

    def __str__(self):
        return self.street_address
ADD_DEFAULT = {"street_address": "", "landmark": "", "city": "","state":"","country":"India"}
class Account(models.Model):
    #for stripe webhook test
    # pickup_add=models.ManyToManyField(PickUpAdd,null=True,blank=True,default=None)
    #change del_add ==> address(plural :))
    # address = JSONField(default=None,null=True,blank=True)
    # del_add=models.ManyToManyField(DeliveryAdd,related_name='del_details',null=True,blank=True,default=None)
    # ac_uid = models.CharField(max_length=15, unique=False)
    user = models.OneToOneField(User,null=True, blank=True, on_delete=models.CASCADE)
    username = models.CharField(max_length=255)
    first_name = models.CharField(max_length=255,null=True, blank=True)
    middle_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255,null=True, blank=True)
    email = models.CharField(max_length=255)
    street_address = models.CharField(max_length=255,null=True, blank=True)
    landmark= models.CharField(max_length=255,null=True, blank=True)
    city= models.CharField(max_length=55,null=True, blank=True)
    state= models.CharField(max_length=55,null=True, blank=True)
    pincode = models.IntegerField(max_length=6,null=True, blank=True)
    image = models.ImageField(default='default.png', blank=True, null=True)
    # gender = models.CharField(max_length=100, choices=GENDER_TYPE, null=True, blank=True)
    # country = models.CharField(max_length=255, blank=True, null=True)
    # address = models.CharField(max_length=255, blank=True, null=True)
    # address2 = models.CharField(max_length=255, blank=True, null=True)
    # address_opt = models.CharField(max_length=255, blank=True, null=True)
    # phone_number = models.CharField(max_length=20, blank=True, null=True)
    # zip_code = models.IntegerField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    tos_accepted_on = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=False)
    is_vendor = models.BooleanField(default=False)
    registered_ip = models.CharField(max_length=255, null=True)
    block_review = models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True)
    added_by = models.CharField(max_length=255)
    updated_on = models.DateTimeField(auto_now_add=True)
    updated_by = models.CharField(max_length=255)
    reset_token_rid = JSONField(default=None,null=True,blank=True)
    register_token_status = JSONField(default=None)

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('accounts:update', kwargs={'pk': self.id})

    # def save(self, *args, **kwargs):
    #     self.ac_uid = 'Ac-'+uniqueCodeGenerator()
    #     return super().save(self, *args, **kwargs)
    #TODO
    #it was giving me some force insert option error so will do it here later so we can put a logic to make it unique
    #But till then we are doint it below in signal


def post_save_user_receiver(sender, instance, created, *args, **kwargs):
    if created:
        print(instance,sender,'sender_instance')
        profile, is_created = Account.objects.get_or_create(
            user=instance,
            # first_name=instance.first_name,
            # middle_name=instance.middle_name,
            # last_name=instance.last_name,
            username=instance.username,
            email=instance.email,
            # ac_uid=uniqueCodeGenerator(),
            register_token_status=True
        )


def pre_delete_account_img(sender, instance, *args, **kwargs):
    if instance.image:
        if instance.image == 'default.png':
            pass
        else:
            if os.path.isfile(instance.image.path):
                os.remove(instance.image.path)


pre_delete.connect(pre_delete_account_img, sender=Account)
post_save.connect(post_save_user_receiver, sender=User)
