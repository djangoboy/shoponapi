from django.conf.urls import url

from .views import (
    Register,
    Login,
    Logout,
    Profile,
    UpdateAccount,
    ProfileDeactivate,
    ChangePassword,
    ForgotPassword,
    reset_password_by_token,
    CheckTokenValidity,Confirm_reset_pwd_token_validity,Create_stripe_custom_ac)
app_name = 'accounts'

urlpatterns = [
    url(r'^register', Register.as_view(), name='register_api'),
    url(r'^confirm_token_validity', CheckTokenValidity.as_view(), name='check_token_val'),
    url(r'^confirm_reset_pwd_token_validity', Confirm_reset_pwd_token_validity.as_view(), name='check_token_val'),
    url(r'^login$', Login.as_view(), name='login_api'),
    url(r'^logout', Logout.as_view(), name='logout_api'),
    url(r'^change_password$', ChangePassword.as_view(), name='change_password_api'),
    url(r'^profile', Profile.as_view(), name='profile_api'),
    url(r'^update', UpdateAccount.as_view(), name='update_api'),
    url(r'^delete/(?P<pk>\d+)$', ProfileDeactivate.as_view(), name='delete_api'),
    url(r'^forgot_password', ForgotPassword, name='forgot-password'),
    url(r'^reset_password_by_token', reset_password_by_token, name='reset-password-token'),
    url(r'^create_stripe_custom_ac', Create_stripe_custom_ac.as_view(), name='create-custom-ac'),

]
