from django.contrib import admin

from .models import Account, PickUpAdd,DeliveryAdd

admin.site.register(Account)
admin.site.register(PickUpAdd)
admin.site.register(DeliveryAdd)
