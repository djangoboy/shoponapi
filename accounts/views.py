import datetime
import stripe

import jwt
from django.conf import settings
from django.contrib.auth import get_user_model, update_session_auth_hash
from django.contrib.auth import login, logout
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import Http404
from django.shortcuts import HttpResponse
from django.template.loader import render_to_string
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.shortcuts import get_object_or_404
from jwt import DecodeError, InvalidTokenError, ExpiredSignatureError


from django.core.mail import send_mail
from requests import Response
from rest_framework import status


from rest_framework.decorators import api_view
from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    UpdateAPIView,
    DestroyAPIView,
    ListAPIView)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
)
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from accounts.models import Account
from config.jsonrand import custom_response
from config.settings import Base_URL
# from vendor.models import Vendor_Info
from .permissions import IsOwnerOrReadOnly
from .serializers import (
    RegisterSerializer,
    LoginSerializer,
    ProfileSerializer,
    UpdateAccountSerializer,
    ChangePasswordSerializer,
    GeneralUserSerializer,
)
from .tokens import account_activation_token
from config.settings.utilities import randomString

User = get_user_model()

class Create_stripe_custom_ac(APIView):    
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = ProfileSerializer
    
    def post(self,request, *args, **kwargs):
        user = self.request.user
        try:
            user_ac = Account.objects.get(user=user)
        except Account.DoesNotExist as e:
            return custom_response('',str(e),str(e),status_code=HTTP_400_BAD_REQUEST)

        scai = user_ac.strip_custom_ac_id
        if not scai:
            stripe.api_key = "sk_test_wfyoe2hm166zFAA88bGHFEOQ003JA44P1T"
            stripe.Account.create(
            type="custom",
            country="US",
            email="bob@example.com",
            requested_capabilities=["transfers"]
            )
            print('here create an custom stripe id and save')
            user_ac.strip_custom_ac_id = 'dummy_stripeid6868'
            user_ac.save() 
            return custom_response(user_ac.strip_custom_ac_id,'strip_custom_ac_id successfully created','strip_custom_ac_id successfully created',status_code=HTTP_200_OK)
        else:
            # except user_ac.stripe_custom_ac_id.DoesNotExist as e:
            #     return custom_response('',str(e),str(e),status_code=HTTP_400_BAD_REQUEST)
            return custom_response(scai,'Already have the strip_custom_ac_id','strip_custom_ac_id',status_code=HTTP_200_OK)




@api_view(['POST'])
def ForgotPassword(request):
    """
        1.generates a token for the user user making the request
        2.returns that token in the response if everything goes otherwise raise appropriate exception.

    """
    email = request.data['email']

    try:
        user = User.objects.get(email=email)
    except User.DoesNotExist:
        return custom_response('', 'Email is not found', message='No user exists with this email', status_code=HTTP_200_OK)


    id = user.id
    exp = datetime.datetime.utcnow() + datetime.timedelta(days=1)
    token_ref = randomString()
    account = Account.objects.get(user=user)
    account.reset_token_rid = {'token':token_ref,"used":False}
    account.save()
    token = jwt.encode({"ID":id,"exp":exp,"jti":token_ref}, settings.SECRET_KEY)
    html_content = render_to_string('accounts/test_email_template.html',{'user':user.username,'token':token.decode(),'base_url':Base_URL})
    from django.utils.html import strip_tags
    text_content = strip_tags(html_content)
    if token:
        send_mail('Shopon Password Reset', text_content, 'connect.shopon@gmail.com', [email])

        return custom_response({"token": token,"used":False}, '', message='token generated. email will be sent in production with this link!', status_code=HTTP_200_OK)
    else:
        return custom_response('', 'Something went wrong token could not be generated.',
                               message='Please try to regenerate if problem persist please contact the Admin',
                               status_code=500)


@api_view(['POST'])
def reset_password_by_token(request):
    """
        this one resets the password for a given hash

        token is expired ==>
            genrate a random string
            attach to the db user table
            and change it after 1st reset

    """
    token=''
    password=''
    password_confirmation=''

    try:
        password = request.data['password']
        password_confirmation=request.data['password_confirmation']
        token = request.data['token']

    except MultiValueDictKeyError as e:
        raise Http404(e)

    finally:
        if password != password_confirmation:
            return custom_response('', errors="password mismatch", message='Password did not match')
        try:
            data = jwt.decode(token, settings.SECRET_KEY)
            user = get_object_or_404(User, id=data['ID'])
            ref_token = data['jti']
            account = get_object_or_404(Account, user=user)
            if account and account.reset_token_rid['token'] == ref_token:
                if account.reset_token_rid['used']==False:
                    user.set_password(password)
                    user.save()
                    account.reset_token_rid['used']=True
                    account.save()
                    return custom_response('', '', message='Successfully reset password, You can try to login',status_code=HTTP_200_OK)
                else:
                    return custom_response('', 'Token is already used; Password could not be reset', message='Token is already used;Password could not be reset',status_code=HTTP_400_BAD_REQUEST)

            else:
                return custom_response('', 'This token is not valid', message='this token is not valid',
                                       status_code=403)

        except InvalidTokenError:
            raise InvalidTokenError('Please provide a Valid token and Password. It will reported to Admin')

        # except InvalidSignatureError:
        #     raise InvalidSignatureError('Invalid Signature Found. It will reported to Admin')

        except (DecodeError, KeyError):
            raise DecodeError('Invalid Signature Found. It will reported to Admin')

        except (ExpiredSignatureError):
            raise ExpiredSignatureError('Looks like Signature is Expired. Please contact Admin')



class Register(CreateAPIView):
    serializer_class = RegisterSerializer
    queryset = ''
    permission_classes = [AllowAny, ]

    def post(self, request):
        user = {
            'email': request.data.get('email'),
            'email_confirmation': request.data.get('email_confirmation'),
            'password': request.data.get('password'),
            'password_confirmation': request.data.get('password_confirmation'),
            'username': request.data.get('username'),
            'tos_accept_confirmation': request.data.get('tos_accept_confirmation'),
            'first_name': request.data.get('first_name'),
            'middle_name': request.data.get('middle_name',None),
            'last_name': request.data.get('last_name'),

        }
        # The create serializer, validate serializer, save serializer pattern
        # below is common and you will see it a lot throughout this course and
        # your own work later on. Get familiar with it.
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        ser_res = serializer.save()
        print(ser_res,'ser_res')
        user_ac = Account.objects.get(user=ser_res['token_info']['user_pk'])
        # user_ac.middle_name=ser_res['middle_name']
        user_ac.register_token_status=ser_res['token_info']
        print('user_ac',user_ac)
        user_ac.save()
        # Create talent
        # user_instance = User.objects.get(username=user['username'])
        # if not user_instance:
        #     talent = Talent.objects.create(user=user_instance.id)


        return custom_response(ser_res['token_info'], '', message='User successfully registered. A verification link will be send to the email, please verify the to activate the account.',
                               status_code=HTTP_200_OK)


class CheckTokenValidity(APIView):
    def post(self,request, *args, **kwargs):
        #to check if a token is used and valid or not
        token_in_req = request.data['token']
        try:
            user_in_req = User.objects.get(pk=request.data['user_pk'])
        except User.DoesNotExist as e:
            return custom_response('',errors=str(e),message='User with this pk does not exist')
        user_ac = Account.objects.get(user=user_in_req)
        db_token = user_ac.register_token_status['token']
        if db_token==token_in_req and user_in_req:
            token_status = user_ac.register_token_status['status']
            return custom_response(token_status,'','this token is '+user_ac.register_token_status['status'],status_code=HTTP_200_OK)
        else:
            return custom_response(token_in_req,'User have updated the password / link has been broken','User have updated the password / link has been broken',status_code=HTTP_400_BAD_REQUEST)



class Confirm_reset_pwd_token_validity(APIView):
    def post(self,request, *args, **kwargs):
        try:
            token = request.data['token']

        except MultiValueDictKeyError as e:
            raise Http404(e)
        try:
            data = jwt.decode(token, settings.SECRET_KEY)
        except (DecodeError) as e:
            return custom_response('', 'decode: token related error', message='decode: token related error', status_code=HTTP_400_BAD_REQUEST)

        except (ExpiredSignatureError) as e:
            return custom_response('', 'ExpiredSignatureError: token has expired', message='ExpiredSignatureError: token has expired', status_code=HTTP_400_BAD_REQUEST)

        except (InvalidTokenError) as e:
            return custom_response('', 'Invalid Token Error', message='Invalid Token Error', status_code=HTTP_400_BAD_REQUEST)

        try:
            user = User.objects.get(id=data['ID'])
            account = Account.objects.get(user=user)
            ref_token = data['jti']
            print('here isall',account.reset_token_rid,account.reset_token_rid['token'],ref_token)
            token_status = account.reset_token_rid['used']
            if account and account.reset_token_rid['token'] == ref_token:
                print('did i run')
                if not account.reset_token_rid['used']:
                    return custom_response(token_status, '', 'this token is valid and Usable', status_code=HTTP_200_OK)
                else:
                    return custom_response(token_status, 'User have updated the password / link has been broken', 'User have updated the password / link has been broken', status_code=HTTP_400_BAD_REQUEST)
            else:
                return custom_response(token_status, 'This token is not valid. Please contact Admin',
                                       'This token is not valid. Please contact Admin',status_code=HTTP_400_BAD_REQUEST)



                # return custom_response('', '', 'this token is ' + '', status_code=HTTP_200_OK)
        except User.DoesNotExist as e:
            return custom_response('',errors=str(e),message=str(e),status_code=HTTP_400_BAD_REQUEST)

        except Account.DoesNotExist as e:
            return custom_response('', errors=str(e), message=str(e), status_code=HTTP_400_BAD_REQUEST)


        #
        # to check if a token is used and valid or not
        # token_in_req = request.data['token']
        # get the token varify it
        #
        #
        #     return custom_response(token_status,'','this token is '+user_ac.register_token_status['status'],status_code=HTTP_200_OK)
        # else:
        #     return custom_response(token_in_req,'User have updated the password / link has been broken','User have updated the password / link has been broken',status_code=HTTP_400_BAD_REQUEST)
        #




        #here catch the token
        # check if that is valid or not and return the response accordingly
        # token = request.data['token']
        # user = User.objects.get(pk=request.data['user_pk'])
        # user_ac = Account.objects.get(user=user)
        # if user_ac:
        #     return custom_response(user_ac, 'Token is Used. Please contact Admin')
        # else:
        #     return custom_response(user_ac, 'Token is not yet Used')

        # if user_ac.register_token_status:
        #     # token_status = account_activation_token.check_token(user, token)
        #
        #     # print(kwargs,args,'here are parameters passed to this function')
        #     # if token_status==True:
        #     return custom_response(, '','')
        # else:
        #     return custom_response(token_status, 'Token is not valid, contact Admin','Token is not valid, contact Admin')


class Login(APIView):
    serializer_class = LoginSerializer
    permission_classes = [AllowAny,]

    def post(self, request, *args, **kwargs):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            print(new_data)
            user = User.objects.get(email=new_data['email'])
            # vendor_ids_list = []
            # for vendor in Vendor_Info.objects.filter(user=user):
            #     vendor_ids_list.append(vendor.pk)
            id_data = {'user_id': user.pk,'first_name':user.first_name,'last_name':user.last_name,'email':user.email}
            print('id_data', id_data)
            return custom_response({"token": new_data['token'],"user_id_details":id_data}, '', message='Successfully LoggedIn',
                                   status_code=HTTP_200_OK)

        return custom_response('', serializer.errors, message='UnSuccessfully LoggedIn',
                               status_code=HTTP_400_BAD_REQUEST)


class Profile(ListAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = ProfileSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True,context={"auth_token_id": '56555'})
        # serializer.data.append='test'
        print(serializer.data,'serialzier_data')
        return custom_response(serializer.data, '', message='User Info', status_code=HTTP_200_OK)

    def get_queryset(self):
        user_list = Account.objects.filter(
            user=self.request.user,
        )
        return user_list



class UpdateAccount(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = UpdateAccountSerializer
    # permission_classes = [AllowAny,]

    def post(self, request, *args, **kwargs):
        instance = Account.objects.get(user=self.request.user)
        print('here look',instance,self.request.user)
        serializer = UpdateAccountSerializer(instance=instance, data=request.data,partial=True)
        if serializer.is_valid(raise_exception=True):
            ser_items = serializer.initial_data.items()
            print(ser_items,serializer.validated_data,'<- validated data')
            serializer.save()
            #here u just save this data to the right ac

            # user = User.objects.get(email=new_data['email'])
            
            return custom_response({"updateddata": 'updated data'}, '', message='Successfully Updated Ac Info',
                                   status_code=HTTP_200_OK)

        return custom_response('', serializer.errors, message='Unsuccessfull attampt to Update the Ac Info',
                               status_code=HTTP_400_BAD_REQUEST)


class ProfileDeactivate(APIView):
    '''
        this one deactivates the the profile.
    '''
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = ProfileSerializer

    # lookup_field = 'username'

    def get_object(self):
        pk = self.kwargs["pk"]
        return get_object_or_404(User, pk=pk)
    #SOFT DELETE
    def get(self, request, *args, **kwargs):
        # return self.delete(request, *args, **kwargs)
        print('self.get_object',self.get_object())
        obj = self.get_object()
        ac_obj = Account.objects.get(user=obj)
        if ac_obj.is_active:
            ac_obj.is_active=False
            ac_obj.save()
        return custom_response('', '', message="user's Account is successfully deactivated; Soft Deleted the user successfully", status_code=HTTP_200_OK)

    # def get_queryset(self, *args, **kwargs):
    #     queryset = User.objects.filter(username=self.request.user, is_active=True)
    #     return queryset

    
        # return custom_response(queryset, '', message='profile Deleted', status_code=HTTP_200_OK)


class ChangePassword(UpdateAPIView):
    serializer_class = ChangePasswordSerializer
    model = User
    authentication_classes = [JSONWebTokenAuthentication]
    # permission_classes = [IsOwnerOrReadOnly, IsAuthenticated]
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return custom_response(self.request.user, '', message='', status_code=HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        # self.object = self.get_object()
        user = request.user
        print(user,'userhere')
        serializer = self.get_serializer(data=request.data)
        # serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            if not user.check_password(serializer.data.get("old_password")):
                return custom_response({"old_password": ["Wrong password"]}, '', message='Wrong Password',
                                       status_code=HTTP_400_BAD_REQUEST)

            user.set_password(serializer.data.get("new_password"))
            user.save()
            update_session_auth_hash(request, user)
            return custom_response('', '', message='Successfully Changed', status_code=HTTP_200_OK)

        return custom_response('', serializer.errors, message='Password Not Updated', status_code=HTTP_400_BAD_REQUEST)


def register_view(request, uidb64, token):
    print(uidb64,token,request.user,'here it is ',force_text(urlsafe_base64_decode(uidb64)))
    uid=''
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        print(uid,'uidhere',User.objects.get(pk=uid))
        user = User.objects.get(pk=uid)
        print(user,'userhere')
        account = Account.objects.get(user=user)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist, Account.DoesNotExist):
        user = None
        account = None

    print(user,uidb64, token,'all here')

    if user is not None and account is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        account.is_active = True
        #unused =True & used = False
        account.register_token_status={'token':token,'status':'used','user_pk':uid}
        # account.reset_token_rid={'token':token,'status':'used'}
        account.save()
        user.save()
        login(request, user)
        return HttpResponse('verified')
    else:
        return HttpResponse('failed')


class Logout(APIView):
    """
    Calls Django logout method and delete the Token object
    assigned to the current User object.
    Accepts/Returns nothing.
    """
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        if getattr(settings, 'ACCOUNT_LOGOUT_ON_GET', False):
            response = self.logout(request)
        else:
            response = self.http_method_not_allowed(request, *args, **kwargs)

        return self.finalize_response(request, response, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.logout(request)

    def logout(self, request):
        try:
            request.user.auth_token.delete()
        except (AttributeError, ObjectDoesNotExist):
            pass

        logout(request)

        return custom_response('', '', message='Successfully LoggedOUT', status_code=HTTP_200_OK)
