from django.shortcuts import render

# Create your views here.
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_204_NO_CONTENT
from config.jsonrand import custom_response
from rest_framework.permissions import AllowAny

from rest_framework.views import APIView

from .serializers import ReminderSerializer
from .models import Reminder


class RemindersList(APIView):
    """
    List all reminders.
    """
    def get(self, request, format=None):
        reminders = Reminder.objects.all()
        serializer = ReminderSerializer(reminders, many=True)
        return custom_response(serializer.data, '', message='', status_code=HTTP_200_OK)


class ReminderDetail(APIView):
    """
    Retrieve a reminder instance.
    """
    def get_object(self, pk):
        try:
            return Reminder.objects.get(pk=pk)
        except Reminder.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        reminder = self.get_object(pk)
        serializer = ReminderSerializer(reminder)
        return custom_response(serializer.data, '', message='', status_code=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        reminder = self.get_object(pk)
        serializer = ReminderSerializer(reminder, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return custom_response(serializer.data, '', message='', status_code=HTTP_200_OK)
        return custom_response('', serializer.errors, message='', status_code=HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        reminder = self.get_object(pk)
        reminder.delete()
        return custom_response('', '', message='Reminder Deleted', status_code=HTTP_204_NO_CONTENT)
