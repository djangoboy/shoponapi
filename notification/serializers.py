from rest_framework import serializers
from notification.models import UserBookingNotification, VendorBookingNotification

class UserBookingNotifSerializer(serializers.ModelSerializer):
    def get_event_message(self,vendor_notification_obj):
        message = None
        if vendor_notification_obj.event_type == 'ONBOOKING':
            message = 'Booking got created'
        elif vendor_notification_obj.event_type == 'UPCOMING':
            message = 'There is an Upcoming Booking'
        else:
            message = 'no registered message for this event'
        return message 
    message = serializers.SerializerMethodField('get_event_message')
    type = serializers.CharField(default='user') 
    message = serializers.SerializerMethodField('get_event_message')

    
    class Meta:
        model = UserBookingNotification
        fields = '__all__'


#  ('ONBOOKING','ONBOOKING'),
#     ('UPCOMING','UPCOMING'),
#     ('BOOKINGUPDATED','BOOKINGUPDATED'),
#     ('SERVICEPROVIDERINFOUPDATED','SERVICEPROVIDERINFOUPDATED'),
#     ('BIZLOCINFOUPDATED','BIZLOCINFOUPDATED'),
#     ('SERVICEPROVIDERINFOOUPDATED','SERVICEPROVIDERINFOOUPDATED')
# )

class VendorBookingNotifSerializer(serializers.ModelSerializer):
    def get_event_message(self,vendor_notification_obj):
        message = None
        if vendor_notification_obj.event_type == 'ONBOOKING':
            message = 'Booking got created'
        elif vendor_notification_obj.event_type == 'UPCOMING':
            message = 'There is an Upcoming Booking'
        elif vendor_notification_obj.event_type =='BOOKINGUPDATED':
            message = 'Booking Got Updated'
        elif vendor_notification_obj.event_type =='SERVICEPROVIDERINFOUPDATED':
            message = 'Service Provider Info Updated'
        elif vendor_notification_obj.event_type =='BIZLOCINFOUPDATED':
            message = 'Business Location Updated'
        else:
            message = 'no registered message for this event'

        return message 

    # def get_type(self,vendor_notification_obj):
    #     type = 'vendor'
    #     return message 
    # type = serializers.SerializerMethodField('get_type')
    type = serializers.CharField(default='vendor') 
    message = serializers.SerializerMethodField('get_event_message')

    class Meta:
        model = UserBookingNotification
        fields = '__all__'

# VENDOR_EVENT_TYPES = (
#     ('ONBOOKING','ONBOOKING'),
#     ('UPCOMING','UPCOMING'),
#     ('BOOKINGUPDATED','BOOKINGUPDATED'),
#     ('SERVICEPROVIDERINFOUPDATED','SERVICEPROVIDERINFOUPDATED'),
#     ('BIZLOCINFOUPDATED','BIZLOCINFOUPDATED'),
#     ('SERVICEPROVIDERINFOOUPDATED','SERVICEPROVIDERINFOOUPDATED')
# )

# USER_EVENT_TYPES = (
#     ('ONBOOKING','ONBOOKING'),
#     ('ONBOOKINGSTATUSCHANGED','ONBOOKINGSTATUSCHANGED'),
# )



class UpdteUserBookingNotifSerializer(serializers.ModelSerializer):
    def get_event_message(self,vendor_notification_obj):
        message = None
        # type = 'user'
        if vendor_notification_obj.event_type == 'ONBOOKING':
            message = 'Booking got created'
        elif vendor_notification_obj.event_type == 'UPCOMING':
            message = 'There is an Upcoming Booking'
        else:
            message = 'no registered message for this event'
        return message
   
    type = serializers.CharField(default='user') 
    message = serializers.SerializerMethodField('get_event_message')
    class Meta:
        model = VendorBookingNotification
        read_only_fields = [
            'id',
            'event_type',
            'created_on',
            'updated_on',
            'booking',
            'message',
            'type'
        ]
        fields = '__all__'
    

class UpdteVendorBookingNotifSerializer(serializers.ModelSerializer):
    # message =  serializers.ChoiceField(choices=USER_EVENT_TYPES,default=USER_EVENT_TYPES[0][0])
    def get_event_message(self,vendor_notification_obj):
        message = None
        if vendor_notification_obj.event_type == 'ONBOOKING':
            message = 'Booking got created'
        elif vendor_notification_obj.event_type == 'UPCOMING':
            message = 'There is an Upcoming Booking'
        elif vendor_notification_obj.event_type =='BOOKINGUPDATED':
            message = 'Booking Got Updated'
        elif vendor_notification_obj.event_type =='SERVICEPROVIDERINFOUPDATED':
            message = 'Service Provider Info Updated'
        elif vendor_notification_obj.event_type =='BIZLOCINFOUPDATED':
            message = 'Business Location Updated'
        else:
            message = 'no registered message for this event'

        return message 
 
    type = serializers.CharField(default='vendor') 
    message = serializers.SerializerMethodField('get_event_message')
    class Meta:
        model = VendorBookingNotification
        read_only_fields = [
            'id',
            'event_type',
            'created_on',
            'updated_on',
            'booking',
            'message',
            'type'
        ]
        fields = '__all__'
