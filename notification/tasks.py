import os
import celery
import subprocess
import string
from celery import shared_task 
from django.core.mail import send_mail
from time import sleep
import datetime

from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from notification.models import UserBookingNotification, VendorBookingNotification
from celery import shared_task
from booking.models import Booking
from django.template.loader import render_to_string

# from booking.models import Booking
# from notification.tasks import createUserNotification
def LimitBookingData(booking_data_obj):
    # obj = {'current_book_obj':}
    short_booking_dict = {}
    show_fileds = ['start','end','date','model_service','cameraman_service','studio_service','state','booking_type','booking_uid','user','vendor','id','pk']

    for i,k in booking_data_obj.items():
        if i in show_fileds:
            short_booking_dict[i]=k
    return short_booking_dict
    

@shared_task
def sendUserEventEmail(event,params_obj=None):
    # print('booking_Data=>',booking_data,type(booking_data),type(booking_data['start']))
    booking = Booking.objects.get(pk=int(params_obj['booking_id']))
    user = booking.user
    user_email = user.email
    v_email = booking.vendor.user.email
    # v_email = booking.vendor.business_email
    print(user.email,'<== user email')
    subject,message = None,None
    from_email = 'connect.shopon@gmail.com'
    if event == 'ONBOOKING':
        subject = 'Booking Confirmation'
        message = 'Booking Request You need to wait for Provider Response'
        recipient_list = [user_email]

        booking_dict = booking.__dict__
        booking_data = LimitBookingData(booking_dict)
        obj = {'current_book_obj':booking_data.items(),'booking':booking,}        
        
        html_msg = render_to_string('notification/onbooking_to_user.html', obj)
        send_mail(subject=subject,message=message,recipient_list=recipient_list,from_email=from_email,html_message=html_msg)   #send email with right template and to and from

    if event == 'ONBOOKINGSTATUSCHANGED':
        subject = 'Booking Updated'
        message = 'Booking Request You need to wait for Provider Response'
        recipient_list = [user_email]
        booking_updated_obj=params_obj['booking_updated_obj']

        booking_dict = booking.__dict__
        booking_data = LimitBookingData(booking_dict)
        obj = {'updated_by':params_obj['used_by'],'current_book_obj':booking_data.items(),'booking':booking,'booking_updated_obj':booking_updated_obj.items()}        
        html_msg = render_to_string('notification/booking_updates_to_user.html', obj)
        send_mail(subject=subject,message=message,recipient_list=recipient_list,from_email=from_email,html_message=html_msg)   #send email with right template and to and from
        
    
    return '{} Email sent to the User'.format(event)



@shared_task
def sendBusinessEventEmail(event,params_obj=None):
    # print('booking_Data=>',booking_data,type(booking_data),type(booking_data['start']))
    booking = Booking.objects.get(pk=int(params_obj['booking_id']))
    user = booking.user
    user_email = user.email
    # v_email = booking.vendor.business_email
    v_email = booking.vendor.user.email
    subject,message = None,None
    from_email = 'connect.shopon@gmail.com'
    show_fileds = ['start','end','date','model_service','cameraman_service','studio_service','state','booking_type','booking_uid','user','vendor','id','pk']
    if event == 'ONBOOKING':
        subject = 'Booking Request'
        message = 'New Booking Requested Needs to be confirmed asap'
        recipient_list = [v_email]
        booking_dict = booking.__dict__
        booking_data = LimitBookingData(booking_dict)
        obj = {'current_book_obj':booking_data.items(),'booking':booking,}        
        
        html_msg = render_to_string('notification/onbooking_to_vendor.html', obj)
        send_mail(subject=subject,message=message,recipient_list=recipient_list,from_email=from_email,html_message=html_msg)   #send email with right template and to and from


    if event == 'ONBOOKINGSTATUSCHANGED':
        subject = 'Booking Got Updated'
        message = 'Booking Request You need to wait for Provider Response'
        recipient_list = [user_email]
        booking_updated_obj=params_obj['booking_updated_obj']

        booking_dict = booking.__dict__
        booking_data = LimitBookingData(booking_dict)
        obj = {'updated_by':params_obj['used_by'],'current_book_obj':booking_data.items(),'booking':booking,'booking_updated_obj':booking_updated_obj.items()}        
        html_msg = render_to_string('notification/booking_updates_to_business.html', obj)
        send_mail(subject=subject,message=message,recipient_list=recipient_list,from_email=from_email,html_message=html_msg)   #send email with right template and to and from    
    return '{} Email sent to to the Business Conernens'.format(event)


@shared_task
def booking_onbooking_event_lis(event,params_obj=None):
    from booking.models import Booking
    b_pk = params_obj['booking_id']
    used_by = params_obj['used_by']
    # booking_updated_obj = params_obj['booking_updated_obj']
    booking = Booking.objects.get(pk=int(b_pk))

    UserBookingNotification.objects.create(booking=booking,event_type=event)
    VendorBookingNotification.objects.create(booking=booking,event_type=event)
    
    from booking.serializers import BookingEmailNotifSerializer#, BookingLimitedInfoSerializer
    b_obj = Booking.objects.get(pk=int(b_pk))
    booking_serializer = BookingEmailNotifSerializer(b_obj)
    b_obj = booking_serializer.data

    # sendUserEventEmail.delay(event,b_obj,used_by)
    # sendBusinessEventEmail.delay(event,b_obj,used_by)
    sendUserEventEmail.delay(event,params_obj)
    sendBusinessEventEmail.delay(event,params_obj)
    return '{} Task excecuted'.format(event)


# @shared_task
# def booking_onbooking_event_lis(event,params_obj=None):
#     from booking.models import Booking
#     b_pk = params_obj['booking_pk']
#     used_by = params_obj['used_by']
#     booking_updated_obj = params_obj['booking_updated_obj']
#     booking = Booking.objects.get(pk=int(b_pk))

#     res='created onbooking notification'
#     if event == 'ONBOOKING':
#         # booking = Booking.objects.get(pk=b_pk)
#         # booking_data = {'start':booking.start,'end':booking.end,'date':booking.date,}

#         UserBookingNotification.objects.create(booking=booking,event_type='ONBOOKING')
#         # createUserNotification(event,booking_data)
#         VendorBookingNotification.objects.create(booking=booking,event_type='ONBOOKING')

#         from booking.serializers import BookingEmailNotifSerializer#, BookingLimitedInfoSerializer
#         b_obj = Booking.objects.get(pk=int(b_pk))
#         booking_serializer = BookingEmailNotifSerializer(b_obj)
#         b_data = booking_serializer.data
#         params_obj = {'b_data':b_data,'used_by':used_by}
#         sendUserEventEmail.delay(event,params_obj)
#         sendBusinessEventEmail.delay(event,params_obj)
#         return res  
#     elif event == 'ONBOOKINGSTATUSCHANGED':
#         booking_updated_obj = params_obj['booking_updated_obj']
#         UserBookingNotification.objects.create(booking=booking,event_type='ONBOOKINGSTATUSCHANGED')
#         # createUserNotification(event,booking_data)
#         VendorBookingNotification.objects.create(booking=booking,event_type='ONBOOKINGSTATUSCHANGED')

#         from booking.serializers import BookingEmailNotifSerializer#, BookingLimitedInfoSerializer
#         b_obj = Booking.objects.get(pk=int(b_pk))
#         booking_serializer = BookingEmailNotifSerializer(b_obj)
#         b_obj = booking_serializer.data

#         sendUserEventEmail.delay(event,b_obj,used_by)
        
#         sendBusinessEventEmail.delay(event,b_obj,used_by,booking_updated_obj)
#         res='created booking updated notification'
#         return res
#     elif event == 'SERVICEPROVIDERINFOUPDATED':
#         UserBookingNotification.objects.create(booking=booking,event_type='SERVICEPROVIDERINFOUPDATED')
#         # createUserNotification(event,booking_data)
#         VendorBookingNotification.objects.create(booking=booking,event_type='SERVICEPROVIDERINFOUPDATED')

#         from booking.serializers import BookingEmailNotifSerializer#, BookingLimitedInfoSerializer
#         b_obj = Booking.objects.get(pk=int(b_pk))
#         booking_serializer = BookingEmailNotifSerializer(b_obj)
#         b_obj = booking_serializer.data

#         sendUserEventEmail.delay(event,b_obj,used_by)
#         sendBusinessEventEmail.delay(event,b_obj,used_by)
#         res='created service provider updated notification'
#         return res
    
#     else:
#         pass



@shared_task(name='upcoming_booking') 
def send_import_summary():
    print('periodic task ran')
    #check if there is any upcoming bookings within 24 hrs range with booking start point
    all_bs = Booking.objects.filter(booking_type='HOURLY').filter(state='approved')
    # datetime object containing current date and time
    now = datetime.datetime.now()
    # if True:
    #     print('sending email for upcoming booking')
        
    for b in all_bs:
        d,s = b.date, b.start
        booking_start_obj = datetime.datetime.combine(d,s) 
        #create one booking date and time object

        td = booking_start_obj - now
        ts = td.total_seconds 
        #ISSUE td some error oh heroku
        if 82800 > td < 86400:
        # if True:
            print('sending email for upcoming booking')
            #create the notification for upcoming booking
            # booking_onbooking_event_lis.delay(event_type='UPCOMING',pk=b.pk)
        else:
            pass

    return 'periodic task ran' 


@shared_task(name='delete_notifications') 
def delete_notifications_func():
    seen_obj = VendorBookingNotification.objects.filter(seen_status=True)
    if seen_obj:
        seen_obj.delete()
    else:
        pass
    return 'Deleted Seen Notifications' 



# def booking_onbooking_event_lis(pk,state,updated_at,event,user_email,start,end,date):
#     # to = user's email
#     #send the right email
#     #create the right notification
#     recipient_list = []
#     if event =='ONBOOKING':
#         email_template = 'email/user_onbooking.html'
#         subject = 'Booking Confirmation'
#         message = 'Your Booking is successfully done for Timings: from Start-time - {} till End-time {} for Date: {}. We will keep you posted with the status. Thank you '.format(start,end,date)
#         from_email='connecting.deepaksingh@gmail.com'
    
#     elif event=='ONBOOKINGSTATUSCHANGED':
#         email_template = 'email/user_onbookingstatuschange.html'
#         subject = 'Booking status is updated'
#         current_state,updated_at = state,
#         message = 'Your current booking status is updated: to {} at {}.'.format(current_state,updated_at)
#         from_email='connecting.deepaksingh@gmail.com'

#     #send some booking details
#     send_mail(subject=subject,message=message,recipient_list=[user_email],from_email=from_email)   #send email with right template and to and from
#     UserBookingNotification.objects.create(booking=Booking.objects.get(pk=book_obj.pk),event_type=event)
#     return 'created onbooking user notification'
