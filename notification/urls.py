from django.conf.urls import url
from notification.views import *

app_name='notification'
urlpatterns = [
    #this is all user Notification
    # UserBookingNotificationsList => UserNotificationsList
    url(r'^user/all/', UserNotificationsList.as_view()),
    url(r'^vendor/all/', VendorNotificationsList.as_view()),
    
    # url(r'^user/all/', UserNotificationsList.as_view()),
    # url(r'^vendor/all/', VendorNotificationsList.as_view()),

    # url(r'^(?P<pk>[0-9]+)$', ServiceDetail.as_view()),    
    url(r'^update/user/(?P<pk>\d+)$', Update_User_Notification.as_view()),
    url(r'^update/vendor/(?P<pk>\d+)$', Update_Vendor_Notification.as_view()),

    # url(r'^user/update/notification/^(?P<pk>[0-9]+)$', Update_User_Notification.as_view()),

]
