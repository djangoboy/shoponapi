from django.shortcuts import render
from config.jsonrand import custom_response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView
from notification.models import UserBookingNotification,VendorBookingNotification
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from .serializers import UserBookingNotifSerializer,VendorBookingNotifSerializer,UpdteUserBookingNotifSerializer, UpdteVendorBookingNotifSerializer
from rest_framework.generics import ListAPIView, RetrieveUpdateAPIView



    
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

class Update_User_Notification(RetrieveUpdateAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    # serializer_class = UpdateRegisterBusinessLocationSerializer
    serializer_class = UpdteUserBookingNotifSerializer
    lookup_field = 'pk'
    queryset = UserBookingNotification.objects.all()

    def update(self, request, *args, **kwargs):
        print('kwargs',kwargs)
        # return custom_response('','','here is a mess',HTTP_200_OK)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        # serializer.data = 'user'
        modified_ser_data = serializer.data
        print(type(serializer.data),'serizlizer.data')
        modified_ser_data['type']='user'
        return custom_response(modified_ser_data, '', message="Notification Updated successfully", status_code=HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    # def update(self, request, *args, **kwargs):
    #     super(UpdateBusinesslocation, self).update(request, *args, **kwargs)
    #     return custom_response('', '', message="Business Location updated successfully", status_code=HTTP_200_OK)
    #
    # def partial_update(self, request, *args, **kwargs):
    #     super(UpdateBusinesslocation, self).update(request, *args, **kwargs)
    #     return custom_response('', '', message="Business Location updated successfully", status_code=HTTP_200_OK)


class Update_Vendor_Notification(RetrieveUpdateAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    # serializer_class = UpdateRegisterBusinessLocationSerializer
    serializer_class = UpdteVendorBookingNotifSerializer
    lookup_field = 'pk'
    queryset = VendorBookingNotification.objects.all()

    def update(self, request, *args, **kwargs):
        print('kwargs',kwargs)
        # return custom_response('','','here is a mess',HTTP_200_OK)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        modified_ser_data = serializer.data
        modified_ser_data['type']='vendor'

        return custom_response(modified_ser_data, '', message="Notification Updated", status_code=HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    # def update(self, request, *args, **kwargs):
    #     super(UpdateBusinesslocation, self).update(request, *args, **kwargs)
    #     return custom_response('', '', message="Business Location updated successfully", status_code=HTTP_200_OK)
    #
    # def partial_update(self, request, *args, **kwargs):
    #     super(UpdateBusinesslocation, self).update(request, *args, **kwargs)
    #     return custom_response('', '', message="Business Location updated successfully", status_code=HTTP_200_OK)



# def CreateOnbooking_UserNotification(event_type,):
#     print('CreateOnbooking_UserNotification ran')
    
    
#     pass


class UserNotificationsList(ListAPIView):
    # authentication_classes = [JSONWebTokenAuthentication]
    queryset = UserBookingNotification.objects.all()
    serializer_class = UserBookingNotifSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('seen_status','booking','event_type','booking__booking_type','booking__user','booking__state','booking__model_service','booking__cameraman_service','booking__studio_service','booking__date')

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)

class VendorNotificationsList(ListAPIView):
    # authentication_classes = [JSONWebTokenAuthentication]
    queryset = VendorBookingNotification.objects.all()
    serializer_class = VendorBookingNotifSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('seen_status','booking','event_type','booking__booking_type','booking__user','booking__state','booking__model_service','booking__cameraman_service','booking__studio_service','booking__date')

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)
