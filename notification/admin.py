from django.contrib import admin
from notification.models import VendorBookingNotification, UserBookingNotification
# Register your models here.

admin.site.register(VendorBookingNotification)
admin.site.register(UserBookingNotification)
