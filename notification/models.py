from django.db import models
from booking.models import Booking
# # Create your models here.


VENDOR_EVENT_TYPES = (
    ('ONBOOKING','ONBOOKING'),
    ('UPCOMING','UPCOMING'),
    ('ONBOOKINGSTATUSCHANGED','ONBOOKINGSTATUSCHANGED'),
    ('BOOKINGUPDATED','BOOKINGUPDATED'),
    ('SERVICEPROVIDERINFOUPDATED','SERVICEPROVIDERINFOUPDATED'),
    ('BIZLOCINFOUPDATED','BIZLOCINFOUPDATED'),
    # ('SERVICEPROVIDERINFOOUPDATED','SERVICEPROVIDERINFOOUPDATED')
)

USER_EVENT_TYPES = (
    ('ONBOOKING','ONBOOKING'),
    ('UPCOMING','UPCOMING'),
    ('ONBOOKINGSTATUSCHANGED','ONBOOKINGSTATUSCHANGED'),
)
class VendorBookingNotification(models.Model):
    #may be you wanna delete that in future
    booking = models.ForeignKey(Booking, related_name='vendor_booking', on_delete=models.SET_DEFAULT,default=None,null=True,blank=True)
    seen_status = models.BooleanField(default=False)
    event_type = models.CharField(max_length=70, choices = VENDOR_EVENT_TYPES,default=None)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


class UserBookingNotification(models.Model):
    #may be you wanna delete that in future
    booking = models.ForeignKey(Booking, related_name='user_booking', on_delete=models.SET_DEFAULT,default=None,null=True,blank=True)
    seen_status = models.BooleanField(default=False)    
    event_type = models.CharField(max_length=70, choices=USER_EVENT_TYPES,default=None)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
