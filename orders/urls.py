from django.conf.urls import url
from .views import *

app_name='orders'
urlpatterns = [
    # url(r'^(?P<pk>[0-9]+)$', OrderDetail.as_view()),
    url(r'^all', OrdersList.as_view()),
    # # url(r'^update/booking/(?P<pk>[0-9]+)$', UpdateBookingStatus.as_view()),
    # #BookingsListForLoggedOutUser
    # url(r'^all', BookingsListForLOUser.as_view()),
    # url(r'^user/all', UserOrdersList.as_view()),
    url(r'^create_user_order', AddUserOrder.as_view()),
    # url(r'^vendor_bookings/all', VendorBookingsList.as_view()),
    # url(r'^check_slot_booking_avail', CheckBookingSlotAvail.as_view()),
]
