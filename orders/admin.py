from django.contrib import admin

# Register your models here.
from .models import Order, OrderedProductDes

admin.site.register(Order)

admin.site.register(OrderedProductDes)
