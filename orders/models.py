from django.db import models
from django_mysql.models import JSONField
from product.models import Product
from accounts.models import User
from utilities import uniqueCodeGenerator

# Create your models here.
PAYMENT_CHOICES = (
    ('COD','COD'),
    ('NETBANKING','NETBANKING')

)
#STOP This Order Creation to be peformed from admin


class OrderedProductDes(models.Model):
    product_color = models.CharField(max_length=100)
    product_size = models.CharField(max_length=100)
    product_uid = models.CharField(max_length=30)

    def __str__(self):
        return 'OrderUid - {product_uid} for {product_color}'.format(
            product_uid=self.product_uid,
            product_color=self.product_color,
            product_size=self.product_size,            
            )

class Order(models.Model):
    order_uid = models.CharField(unique=True,max_length=100)
    user = models.ForeignKey(User, related_name='orders',null=True, blank=True, on_delete=models.CASCADE)
    paid_ammount = models.IntegerField(default=0,null=True,blank=True)
    ordered_products = models.ManyToManyField(OrderedProductDes,null=True,blank=True)
    delivery_address = JSONField(null=True, blank=True)
    tentative_del_date = models.DateField(null=True,blank=True)
    payment_mode = models.CharField(max_length=30,choices=PAYMENT_CHOICES,default=None) 
    delivered = models.BooleanField(default=False)
    def __str__(self):
        return 'OrderUid - {order_uid}'.format(
            order_uid=self.order_uid,
            )
    def save(self, *args, **kwargs):
        self.order_uid = 'Order-' + uniqueCodeGenerator()
        return super().save(*args, **kwargs)
        
