from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.shortcuts import get_object_or_404

from django.core.mail import send_mail
from requests import Response
from rest_framework import status


from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    UpdateAPIView,
    DestroyAPIView,
    ListAPIView)

from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from accounts.models import Account
from config.jsonrand import custom_response
from .serializers import *

class OrdersList(ListAPIView):
    '''
        this one returns the order of the logged in user with JWT & Doesnot work with JWT
        BUT for NOW it doesnot through error when accessed without JWT so WILLFIX.
    '''
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = OrderSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        print(serializer.data,'serialzier_data')
        return custom_response(serializer.data, '', message="User's Order Info", status_code=HTTP_200_OK)

    def get_queryset(self):
        order_list = Order.objects.filter(
            user=self.request.user,
        )
        return order_list

class AddUserOrder(APIView):
    serializer_class = OrderSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    # queryset = CartInfo.objects.all()
    # def get_queryset(self):
    #     print('self.kwargs.pk',self.request.user)
    #     queryset = CartInfo.objects.filter(user=self.request.user.pk)
    #     print(queryset,'here queryset')
    #     return queryset

    def post(self,request,*args,**kwargs):
        choosen_product_obj = request.data.ordered_products
        product_desc_ser = OrderedProductDesSerializer
        print(self.request,dir(self.request),'self.request')
        #check if anything outofStock with the given description
        for pro_obj in choosen_product_obj:
            prod_des_ser = product_desc_ser(data=choosen_product_obj)
            if prod_des_ser.is_valid():    
                p = prod_des_ser.save()
                p_id = p.id
            return custom_response('','','Created sucessfully',status_code=HTTP_200_OK)
            
        else:
            return custom_response('','Seems like there is some issue please check with Admin','Created sucessfully',status_code=HTTP_400_BAD_REQUEST)




# # Photographer
# class AddUserOrder(CreateAPIView):
#     authentication_classes = [JSONWebTokenAuthentication]
#     queryset = Order.objects.all()
#     serializer_class = OrderListSerializer

#     def create(self, request, *args, **kwargs):
#         serializer = self.get_serializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         # self.perform_create(serializer)
#         return custom_response('', '', message='User Order Successfully Created', status_code=HTTP_200_OK)

#     # def perform_create(self, serializer):
#     #     parent_vendor = Vendor_Info.objects.get(user=self.request.user)
#     #     serializer.save(manager_id=parent_vendor)
