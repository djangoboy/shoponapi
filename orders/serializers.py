from .models import *
from rest_framework import serializers


class OrderedProductDesSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderedProductDes
        fields = ['product_color','product_size','product_uid']


class OrderListSerializer(serializers.ModelSerializer):
    # can_be_deleted = serializers.BooleanField(default=True, read_only=True)
    # publish = serializers.BooleanField(default=True, read_only=True)
    # vendor_response_time = serializers.ReadOnlyField(source='business_location.vendor_response_time')
    # name = serializers.ReadOnlyField(source='alias_name')

    class Meta:
        model = Order
        fields = '__all__'
        # exclude = ('status', )



class OrderSerializer(serializers.ModelSerializer):
    ordered_products=OrderedProductDesSerializer(many=True)
    class Meta:
        model = Order
        fields = '__all__'
