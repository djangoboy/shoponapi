this is backup with the lookup relationship problem as it is. just put some data and check.
Python/Django + JWT auth backend
================================

### Technical Stack

	- Python/Django: python 3.6,

	- Database: Mysql

	- JWT auth based RESTful API

### Setting up the server
    ```
    #dev environment
        1. if environment is heroku please add these two the requirments.txt [django-storages==1.7.1, django-heroku]
        2. pip install -r requirements.txt
        3. Make sure not to push local_settings.py to the git repo as we it will use dev_settings.py if local is not found
        4. set DEBUG in base_settings.py and ENV ('DEV')
        5. run migrations and migrate
        6. Finally run the server you should be good to go.
    ```
    ```
    #local environment
        1. pip install -r requirments.txt
        2. set DEBUG AND ENV ('LOCAL')
        3. run migrations and migrate "python manage.py makemigrations * && python manage.py migrate"
        4.Finally run the server "python manage.py runserver". you should be good to go.

    ```
  
 
Get static files
    ```
    $ ./manage.py collectstatic
    ```

Migrate models into database
    ```
    $ ./manage.py makemigrations
    ```

Create admin user on server
    ```
    $ ./manage.py migrate
    
    $ ./manage.py createsuperuser
    ```

Pre-requirement
    1. Install GDAL library for GeoIP/GeoLocation support

    - MacOS
        ```
        $ brew install postgis
        $ brew install gdal
        $ brew install libgeoip
        ```
    
    - Ubuntu
        ```
        $ sudo apt-get install binutils libproj-dev gdal-bin
    	```

Run server
    ```
    $ ./manage.py runserver
    ```
