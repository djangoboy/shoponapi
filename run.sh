#!/usr/bin/env bash
DEFAULT_USER="admin"
DEFAULT_EMAIL="admin@mail.com"
DEFAULT_PASS="password"

case $1 in

  install)
    python3 -m pip install --user -r requirements.txt
   ;;

   migrate)
   ./manage.py makemigrations
   ./manage.py migrate
    ;;

   flush)
   ./manage.py flush
    ;;

   user)
   ./manage.py createsuperuser
    ;;

    reset)
    ######## MAIN CLEAN #########
    find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
    find . -path "*/migrations/*.pyc"  -delete
    pip uninstall django -y
    pip install django==2.0.2
    ./manage.py makemigrations
    ./manage.py migrate
    ;;

   run)
   ./manage.py runserver
  if [-z $2]; then
     ./manage.py runserver 0.0.0.0:$2
  else
   ./manage.py runserver
  fi
    ;;

      *)
         echo "
         1.Use  ./run.sh install for first time setup\n
         2.Use ./run.sh migrate to run makemigrations, migrate\n
         3.Use ./run.sh user to create a super user\n
         4.Use ./run.sh run to runserver\n
         5.Use ./run.sh run <port> to runserver at particular port\n
         6.Use ./run.sh flush to run destroy all data in tables\n
";;
esac
