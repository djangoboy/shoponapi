import os

from django.contrib.auth.models import User
from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.utils.safestring import mark_safe
from django_mysql.models import JSONField
from djmoney.models.fields import MoneyField
# from service_category.models import ServiceCategory
from config.validators import validate_document_extension, validate_image_extension
# from vendor.models import Vendor_businessLocation, Vendor_Info
from lookup.models import lookupItem, weekday_choices
from django.db.models.signals import post_save

PRICE_TYPE=(
    ('HOURLY','HOURLY'),
    ('DAILY','DAILY'),
)

CAMERAMAN_TYPES=(
    ('CANDID','CANDID'),
    ('WEDDING','WEDDING'),
    ('WILDLIFE','WILDLIFE')
)
GENDER_TYPE= (
    ('MALE','MALE'),
    ('FEMALE','FEMALE'),
)


LOCAL_WORK_LOCATION_CHOICES=(
    (0,0),
    (1,1),
)

class Service(models.Model):
    status = models.BooleanField(default=True)    
    s_price_type = models.CharField(max_length=100, choices=PRICE_TYPE,default='HOURLY')
    category = models.CharField(max_length=20,blank=False,null=False, editable=False, default='PHOTOGRAPHER')
    # price_type = models.CharField(max_length=100, choices=PRICE_TYPE,default='HOURLY')
    #could not found dob so probably needs to be done.
    # date_of_birth = models.DateField(blank=True,null=True)    
    created_at = models.DateTimeField(auto_now_add=True)
    screen_name = models.CharField(max_length=255, null=True, blank=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    city = models.CharField(max_length=20, null=True, blank=True)
    state = models.CharField(max_length=50, null=True, blank=True)
    country = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(max_length=50, null=True, blank=True)
    phone_number = models.CharField(max_length=50, null=True, blank=True)
    aptsteunit = models.CharField(max_length=50, null=True, blank=True)
    address1 = models.CharField(max_length=200, null=True, blank=True)
    address2 = models.CharField(max_length=200, null=True, blank=True)
    zipcode = models.CharField(max_length=7, null=True, blank=True)
    image = models.ImageField(upload_to='media/cameraman', blank=True, null=True)
    prefer_to_travel = models.BooleanField(default=True)
    websiteURL = models.URLField(blank=True)
    hourly_price = MoneyField(max_digits=14, decimal_places=2, default_currency='USD',null=True,blank=True)
    daily_price = MoneyField(max_digits=14, decimal_places=2, default_currency='USD',null=True,blank=True)
    additional_hours_price = MoneyField(max_digits=14, decimal_places=2, default_currency='USD',null=True,blank=True)
    min_duration = models.IntegerField(blank=False, default=30)
    travel_time = models.IntegerField(blank=False, default=30)
    start = models.TimeField(null=True, blank=True)
    end = models.TimeField(null=True, blank=True)
    cameraman_type = models.CharField(max_length=50, null=True, blank=True,choices=CAMERAMAN_TYPES)
    is_videographer = models.BooleanField(default=False, blank=True)
    social_links = JSONField(null=True, blank=True)
    rating = models.FloatField(blank=True, default=0.0)
    # business_location = models.ForeignKey(Vendor_businessLocation, related_name='businesslocation_photographer',on_delete=models.SET_DEFAULT, null=True, blank=True, default=None)
    # manager_id = models.ForeignKey(Vendor_Info, related_name='vendor_cameraman',on_delete=models.SET_DEFAULT, null=True, blank=True, default=None)
    # gender = models.ForeignKey(lookupItem, related_name='cameraman_gender', on_delete=models.CASCADE,
    #                            null=True, blank=True)
    local_work_location = models.IntegerField(choices=LOCAL_WORK_LOCATION_CHOICES,null=True,blank=True)
    gender = models.CharField(max_length=100,choices=GENDER_TYPE, null=True, blank=True)

    delivery_mode = models.ForeignKey(lookupItem, related_name='lookuptype_cameraman_delivery', on_delete=models.SET_DEFAULT, null=True, blank=True, default=None)
    is_publish = models.BooleanField(default=True, editable=False)
    can_be_deleted = models.BooleanField(default=True, editable=False)
    copyrightrules = models.CharField(max_length=1000, blank=True, null=True)
    specialinstructions = models.CharField(max_length=1000, blank=True, null=True)
    terms_and_conditions = models.CharField(max_length=1000, blank=True, null=True)
    no_of_previous_bookings = models.IntegerField(blank=False, editable=False, default=0)
    type = models.ForeignKey(lookupItem, related_name='lookuptype_cameraman',
                             on_delete=models.SET_DEFAULT, null=True, blank=True, default=None)
    # type = models.CharField(max_length=50, null=True, blank=True,choices=CAMERMAN_TYPES)

    def business_name(self):
        try:
            business_name = self.business_location.business_name
            return business_name
        except (AttributeError):
            return None


    def alias_name(self):
        return self.screen_name

    def url(self):
        return os.path.join('/', settings.MEDIA_URL, 'cameraman/', os.path.basename(str(self.image)))

    def image_tag(self):
        return mark_safe('<img src="{src}" width="{width}" height="{height}" />'.format(
            src=self.url(),
            width=150,
            height=(150 / self.image.width * self.image.height)
        ))

    image_tag.short_description = 'Image'

    def __str__(self):
        return str(self.pk)


    def save(self,*args,**kwargs):
        if self.s_price_type == 'HOURLY':
            self.daily_price = None
        else:
            self.hourly_price = None
        return super().save(*args,**kwargs)


    class Meta:
        ordering = ('hourly_price',)
        managed = True


class service_documents(models.Model):
    service_id = models.ForeignKey(Service, null=True, blank=True,
                                   on_delete=models.SET_NULL, related_name='service_cameraman_documents')
    document_name = models.CharField(max_length=30, blank=True)
    document_file = models.FileField(upload_to='media/cameraman/service_documents',
                                     validators=[validate_document_extension],
                                     blank=True,
                                     null=True)
    document_type = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return '{service_id} -> {document_name}:  {document_type} '.format(
            service_id=self.service_id.screen_name,
            document_name=self.document_name,
            document_type=self.document_type
        )

    class Meta:
        ordering = ('service_id', 'document_type')


PRICE_TYPE=(
    ('HOURLY','HOURLY'),
    ('DAILY','DAILY'),
)

class service_WeekdayOpeningPeriod(models.Model):
    service_id = models.ForeignKey(Service, null=True, blank=True,
                                   on_delete=models.SET_NULL, related_name='service_cameraman_timings')
    weekday = models.PositiveIntegerField(choices=weekday_choices,null=True,blank=True)
    #date = models.DateField(null=True,blank=True)
    start = models.TimeField(null=True)
    end = models.TimeField(null=True)
    price_type = models.CharField(max_length=50,default='HOURLY',choices=PRICE_TYPE)
    o_l_h = models.BooleanField(default=False)  # long hours

    class Meta:
        ordering = ('weekday', 'start')

    def __str__(self):
        return u'%s: %s - %s' % (self.get_weekday_display(),
                                 self.start, self.end)


class service_WeekdayOpeningExceptionPeriod(models.Model):
    service_id = models.ForeignKey(Service, null=True, blank=True,on_delete=models.CASCADE)
    exc_date = models.DateField()
    start = models.TimeField(null=True)
    end = models.TimeField(null=True)
    price_type = models.CharField(max_length=50,default='HOURLY',choices=PRICE_TYPE)

    class Meta:
        ordering = ('exc_date', 'start')

    def save(self, *args,**kwargs):
        self.price_type = self.service_id.s_price_type
        return super().save(*args,**kwargs)

    def __str__(self):
        return u'%s: %s - %s' % (self.exc_date,
                                 self.start, self.end)

    # def checkifAlreadySlot_daily(self):
    #     print('validate data',self)
    #     obj = {}
    #     if True:
    #         obj['errors']='One Time slot fot this day Already Exist; Your service price type is Daily'
    #     return obj


class service_Gallery(models.Model):
    service_id = models.ForeignKey(Service, null=True, blank=True,
                                   on_delete=models.SET_NULL, related_name='service_model_gallery')
    title = models.CharField(max_length=100, blank=True, null=True)
    image = models.ImageField(blank=True, upload_to='media/cameraman/service_gallery', null=True)

    def __str__(self):
        return '{service_id} -> {title}:  {image} '.format(
            service_id=self.service_id.screen_name,
            title=self.title,
            image=self.image
        )

    class Meta:
        ordering = ('id',)


class service_pricing(models.Model):
    s_price_type = models.CharField(max_length=100, choices=PRICE_TYPE,default='HOURLY')
    service_id = models.OneToOneField(Service, null=True, blank=True, on_delete=models.SET_NULL, related_name='price_type')
    price_type = models.CharField(max_length=30, blank=True,null=True,choices=PRICE_TYPE,default='HOURLY')
    rules = JSONField(null=True, blank=True)
    dis_price = MoneyField(max_digits=14, decimal_places=2, default_currency='USD')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return '{service_id}-> {price_type}'.format(
            service_id=self.service_id.screen_name,
            price_type=self.price_type,
            # dis_price=self.dis_price
        )

@receiver(post_save, sender=Service)
def create_service(sender, instance, created, **kwargs):
    if created:
        service_pricing.objects.create(service_id=instance,price_type=instance.s_price_type)
        
    