from django.db.models import Q
from django.http import Http404
from django.shortcuts import render

# Create your views here.
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import CreateAPIView, RetrieveUpdateAPIView, ListAPIView
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_204_NO_CONTENT
from rest_framework.permissions import AllowAny
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from config.helpers import modify_input_for_multiple_files, modify_input_for_multiple
from config.jsonrand import custom_response
from rest_framework.views import APIView

from .serializers import *
from cameraman.models import *

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters


class ServicesList(ListAPIView):
    # authentication_classes = [JSONWebTokenAuthentication]
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    # filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    # filter_fileds = ('business_location')
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    # filter_fields = ('model_service','cameraman_service','studio_service','date','vendor')
    filter_fields = ('business_location','status')

    def list(self, request, *args, **kwargs):
            queryset = self.filter_queryset(self.get_queryset())
            # user = self.request.user
            # vendor = Vendor_Info.objects.get(user=user)
            # queryset = queryset.filter(vendor=vendor)
            # print(vendor,queryset,'afdafdafdf')
            serializer = self.get_serializer(queryset, many=True)
            return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)
   
    # def list(self, request, *args, **kwargs):
    #     business_location = self.request.query_params.get('business_location', None)
    #     print(business_location,'cameraman here is your self business_name')
    #     if business_location:
    #         print('this is this')
    #         services = Service.objects.all().filter(business_location=business_location)
    #     else:
    #         print('hereerererere')
    #         services = Service.objects.all()
    #     serializer = self.get_serializer(services, many=True)
    #     return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)


# class ServicesList(ListAPIView):
#     # authentication_classes = [JSONWebTokenAuthentication]
#     queryset = Service.objects.all()
#     serializer_class = ServiceSerializer
#     # filter_backends = (filters.SearchFilter, DjangoFilterBackend)
#     # filter_fileds = ('business_location')

#     def list(self, request, *args, **kwargs):
#         business_location = self.request.query_params.get('business_location', None)
#         print(business_location,'here is your self business_name')
#         filtered_q = self.get_queryset().filter(business_location=business_location)
        
#         serializer = self.get_serializer(filtered_q, many=True)
#         return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)
    


# class ServicesList(APIView):
#     # authentication_classes = [JSONWebTokenAuthentication]
#     serializer_class = ServiceSerializer
#     filter_backends = [SearchFilter, OrderingFilter]
#     search_fields = ['business_location__name', 'business_location_zipcode']
#     queryset = Service.objects.all()
    
#     """
#     List all services.
#     """

#     # def get_queryset(self, *args, **kwargs):
#     #     print('m i ever here!!!')
#     #     queryset = Service.objects.filter(publish=True).order_by('-id')
#     #     query = self.request.GET.get('search')
#     #     if query:
#     #         queryset = Service.objects.filter(
#     #             Q(business_location__zipcode__icontains=query), publish=True).distinct()
#     #     return queryset
#     # def get_queryset(self):
#     #     return super().get_queryset()
    
    
#     def get(self, request, format=None):
#         business_location = self.request.query_params.get('business_location', None)
#         print(business_location,'here is your self business_name')
#         # filtered_q = self.get_queryset().filter(business_location=business_location)
        
#         # serializer = self.get_serializer(filtered_q, many=True)
#         business_location = self.request.query_params.get('business_location', None)
#         print(business_location,'here is your self business_name')
#         filtered_q = self.queryset.filter(business_location=business_location)
#         # services = Service.objects.all()
#         serializer = ServiceSerializer(filtered_q, many=True)
#         return custom_response(serializer.data, '', message='', status_code=HTTP_200_OK)

#     # def get(self, request, format=None):
#     #     print('user',self.request.user,type(self.request.user))
#     #     current_vendor = Vendor_Info.objects.get(user=self.request.user.pk)
#     #     all_biz_loc = current_vendor.test_vendors.all()
#     #     l = Service.objects.none()
#     #     for biz in all_biz_loc:
#     #         all_cameraman = biz.businesslocation_photographer.all()
#     #         l=l.union(all_cameraman)
#     #     print(len(l),'lenl_cameraman')
#     #     serializer = ServiceSerializer(l, many=True)
#     #     return custom_response(serializer.data, '', message='', status_code=HTTP_200_OK)


class ServiceDetail(APIView):
    """
    Retrieve a model instance.
    """

    def get_object(self, pk):
        try:
            return Service.objects.get(pk=pk)
        except Service.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        service = self.get_object(pk)
        serializer = ProductSerializer(service)
        return custom_response(serializer.data, '', message='', status_code=HTTP_200_OK)


# Photographer
class AddPhotographer(CreateAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    queryset = Service.objects.all()
    serializer_class = Services_Serializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return custom_response('', '', message='Successfully Registered', status_code=HTTP_200_OK)

    def perform_create(self, serializer):
        parent_vendor = Vendor_Info.objects.get(user=self.request.user)
        serializer.save(manager_id=parent_vendor)


class DeletePhotographer(APIView):
    authentication_classes = [JSONWebTokenAuthentication]

    def get(self, request, *args, **kwargs):
        try:
            cameraman_obj = Service.objects.get(
                id=self.kwargs['id']
            )
            if not (cameraman_obj.business_location.vendor_name.user == self.request.user):
                return custom_response('', "You are not authorised to access this Businesslocation's data", message="You are not authorised to access this Businesslocation's data",
                                       status_code=HTTP_400_BAD_REQUEST)
            cameraman_obj.status = False
            print(cameraman_obj.status,'hereisstatus')
            cameraman_obj.save()
            return custom_response('', '', message="Success", status_code=HTTP_200_OK)

        except Service.DoesNotExist:
            return custom_response('', '', message="Cameraman Does not exist", status_code=HTTP_400_BAD_REQUEST)


class UpdatePhotographer(RetrieveUpdateAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = ServiceSerializer
    # permission_classes = [ProductPermission]
    lookup_field = 'id'
    queryset = Service.objects.all()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        # if 's_price_type' in request.data:
        #     service_id = kwargs['id']
        #     sp = service_pricing.objects.get(service_id=service_id)
        #     sp.price_type=request.data['s_price_type']
        #     sp.save()
        return custom_response(serializer.data, '', message="Saved successfully", status_code=HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class AddService_documents(CreateAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    # parser_classes = (MultiPartParser, FormParser)
    queryset = service_documents.objects.all()
    serializer_class = service_DocumentSerializer

    def create(self, request, *args, **kwargs):
        property_id = dict((request.data).lists())['service_id']
        d_name = dict((request.data).lists())['document_name']
        d_file = dict((request.data).lists())['document_file']
        d_type = dict((request.data).lists())['document_type']
        arr = []
        for (service, tile_name, file_name, file_type) in zip(property_id, d_name, d_file, d_type):
            data = modify_input_for_multiple(service, tile_name, file_name, file_type)
            serializer = self.get_serializer(data=data)
            if serializer.is_valid(raise_exception=True):
                self.perform_create(serializer)
                arr.append(serializer.data)

        return custom_response(arr, '', message='Successfully Documents Uploaded', status_code=HTTP_200_OK)

    def perform_create(self, serializer):
        serializer.save()


class DispService_documents(ListAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    queryset = service_documents.objects.all()
    serializer_class = service_DocumentSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return custom_response(serializer.data, '', message='Documents Successfully Loaded', status_code=HTTP_200_OK)


class AddService_gallery(CreateAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    # parser_classes = (MultiPartParser, FormParser)
    queryset = service_Gallery.objects.all()
    serializer_class = service_GallerySerializer

    def create(self, request, *args, **kwargs):
        property_id = dict((request.data).lists())['service_id']
        images = dict((request.data).lists())['image']
        titles = dict((request.data).lists())['title']
        arr = []
        for (service, tile_name, img_name) in zip(property_id, titles, images):
            data = modify_input_for_multiple_files(service, tile_name, img_name)
            serializer = self.get_serializer(data=data)
            if serializer.is_valid(raise_exception=True):
                self.perform_create(serializer)
                arr.append(serializer.data)

        return custom_response(arr, '', message='Successfully Images Registered', status_code=HTTP_200_OK)

    def perform_create(self, serializer):
        serializer.save()


class DispService_gallery(ListAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    queryset = service_Gallery.objects.all()
    serializer_class = service_GallerySerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return custom_response(serializer.data, '', message='Images Successfully Loaded', status_code=HTTP_200_OK)



'''



'''
class DispService_timings(ListAPIView):
    authentication_classes = [JSONWebTokenAuthentication]
    queryset = service_WeekdayOpeningPeriod.objects.all()
    serializer_class = service_WeekdayOpeningPeriodSerializer

    def list(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        print(pk, 'found2')

        print('list ran')
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return custom_response(serializer.data, '', message='Cameraman Timings', status_code=HTTP_200_OK)


    def get_queryset(self):
        pk = self.kwargs['pk']
        print(pk, 'found')
        vendor_id = Vendor_Info.objects.only('id').get(user=self.request.user).id
        print(vendor_id,'vendor_id',type(vendor_id))
        try:
            #instead of passing vendor_id we need service obj
            # service = Service.objects.get(pk=pk)
            vendor_val = service_WeekdayOpeningPeriod.objects.filter(service_id=pk)
            print(vendor_val,'vendor_val')
            if vendor_val.exists():
                return vendor_val
        except service_WeekdayOpeningPeriod.DoesNotExist:
            return custom_response('', '', message='Please check the Vendor field', status_code=HTTP_400_BAD_REQUEST)


class AddService_timings(CreateAPIView):
    '''
    check if they all have same service_id or tell them
    take all the inputs
    create availibitlity_time_slots
    check if any slot are overlapping
    create the time slots
    '''
    authentication_classes = [JSONWebTokenAuthentication]
    queryset = service_WeekdayOpeningPeriod.objects.all()
    serializer_class = service_WeekdayOpeningPeriodSerializer

    def create(self, request, *args, **kwargs):
        #put below to prior_check function
        if isinstance(request.data, list):
            
            print(request.data, type(request.data[0]), '---->', request.data[0], 'request.data', type(request.data))
            no_of_slots = len(request.data)
            service_id=request.data[0].get('service_id',None)
            s_price_type = Service.objects.get(pk=service_id).s_price_type
            slot_dates_li = [] 
            weekdays_li = []
            exc_slot_dates_li=[]
            if s_price_type=='DAILY':
                if len(request.data)>1:
                    if not 'exc_date' in request.data[0] and not 'weekday' in request.data[0]:
                        return custom_response(request.data, 'You can not add more then two slots for As your Price Type is "Daily".', message=' This makes you available on this time Everyday. You Can Add New Exception Timings by using exc_date', status_code=HTTP_400_BAD_REQUEST)
                    else:
                        pass
                else:
                    pass

            for slot in request.data:
                if slot['service_id'] != request.data[0]['service_id']:
                    return custom_response('', 'All service_ids should be the same','You shall add timings for one single model not for multiple.',status_code=HTTP_400_BAD_REQUEST)
                #adding the price_type as per the model not taking it from front end 
                # check if overlopinng in slots
                
                if 'exc_date' in slot:
                    if s_price_type=='DAILY' and slot['exc_date'] in exc_slot_dates_li:
                        exc_slot_dates_li.append(slot['exc_date'])
                        # if len(request.data)>1:
                        return custom_response(request.data, 'You can not add more then two slots for a Same Exception Date the same date As your Price', message='You can not add more then two slots for As your Price Type is "Daily".', status_code=HTTP_400_BAD_REQUEST)
                    else:
                        pass
                
            

                if 'weekday' in slot:
                    if s_price_type=='DAILY' and slot['weekday'] in weekdays_li:
                        weekdays_li.append(slot['weekday'])
                        return custom_response(request.data, 'You can not add more then two slots for the same date As your Price Type is "Daily".', message='You can not add more then two slots for the same Day. As your Price Type is "Daily".', status_code=HTTP_400_BAD_REQUEST)
                
                    else:
                        pass
        

        user = self.request.user
        # print(service_id,'service_id.. request.data',request.data)


        try:
            vendor = Vendor_Info.objects.get(user=user)
            service = Service.objects.get(pk=service_id)
           
        except Vendor_Info.DoesNotExist:
            return custom_response('', '', message='No Vendor Exist with this service_id',
                                   status_code=HTTP_400_BAD_REQUEST)
        except Service.DoesNotExist:
            return custom_response('', '', message='No Model found with the given service_id',
                                   status_code=HTTP_400_BAD_REQUEST)
        finally:
            print('service=>',service,type(service))
            if not service.business_location:
                return custom_response('', '', message='No business Location found for this Service; Please report to Admin',
                                    status_code=HTTP_400_BAD_REQUEST)

            if service.business_location.vendor_name!=vendor:
                return custom_response('', 'looks like atleast one of the timing belongs to the Model that is not under the logged in vendor, Please contact admin', message='Unable to add timings', status_code=HTTP_400_BAD_REQUEST)
        # service = Service.objects.get(pk=int(service_id))

        if not 'exc_date' in request.data[0]:
            try:
                swop = service_WeekdayOpeningPeriod.objects.filter(service_id=service_id,price_type=service.s_price_type)
                
            except service_WeekdayOpeningPeriod.DoesNotExist:
                return custom_response('', '', message='Please check the Service ID field',
                                    status_code=HTTP_400_BAD_REQUEST)
            finally:
                if swop.exists():
                    service_WeekdayOpeningPeriod.objects.filter(service_id=service_id,price_type=service.s_price_type).delete()
               
                for slot in request.data:
                    slot['price_type'] = service.s_price_type
                print(request.data,'<-- here it is')
                serializer = service_WeekdayOpeningPeriodSerializer(data=request.data, many=isinstance(request.data, list))
                print('seraializer',serializer.is_valid(),serializer.errors)
                serializer.is_valid(raise_exception=True)
                if serializer.is_valid():
                    self.perform_create(serializer)
                    headers = self.get_success_headers(serializer.data)
                    return custom_response(serializer.data, '', message='Timings got added', status_code=HTTP_200_OK)

                else:
                    return custom_response('', serializer.errors, message='Timings could not updated', status_code=HTTP_401_UNAUTHORIZED)

          
        else:
            for data in request.data:
                if 'exc_date' not in data:
                    return custom_response('', 'Please Make Sure All slots have "exc_date"', message='Please Make Sure All slots have "exc_date"',status_code=HTTP_400_BAD_REQUEST)
                if data['exc_date']:
                    from booking.models import Booking
                    bookings = Booking.objects.filter(date=data['exc_date'],cameraman_service=data['service_id'],state='approved')
                    if bookings.exists():
                        return custom_response(data, 'Exception can not be added if booking for the date already Exist', message='Booking already exist on the given exception date',status_code=HTTP_400_BAD_REQUEST)

            try:

                print(service.s_price_type,'<-- here is price_type')
                swop = service_WeekdayOpeningExceptionPeriod.objects.filter(service_id=service_id,price_type=service.s_price_type)
            except service_WeekdayOpeningExceptionPeriod.DoesNotExist:
                return custom_response('', '', message='Please check the Service ID field',
                                    status_code=HTTP_400_BAD_REQUEST)
        
            finally:
                if swop.exists():
                    swop.delete()
                for slot in request.data:
                    slot['price_type'] = service.s_price_type
                    # slot['price_type'] = service.s_price_type
            
                serializer = service_WeekdayOpeningExceptionPeriodSerializer(data=request.data, many=isinstance(request.data, list))
                print('seraializer',serializer.is_valid(),serializer.errors)
                serializer.is_valid(raise_exception=True)
                if serializer.is_valid():
                    self.perform_create(serializer)
                
                    headers = self.get_success_headers(serializer.data)
                    return custom_response(serializer.data, '', message='Timings for Exception dates are added', status_code=HTTP_200_OK)

                else:
                    return custom_response('', serializer.errors, message='Timings could not be added', status_code=HTTP_401_UNAUTHORIZED)



























        # if not 'exc_date' in request.data[0]:
        #     serializer = service_WeekdayOpeningPeriodSerializer(data=request.data, many=isinstance(request.data, list))
        #     print('seraializer',serializer.is_valid())
        #     serializer.is_valid(raise_exception=True)
        #     if serializer.is_valid():
        #         self.perform_create(serializer)
        #         headers = self.get_success_headers(serializer.data)
        #         return custom_response(serializer.data, '', message='Timings added for a Model', status_code=HTTP_200_OK)

        #     else:
        #         return custom_response('', serializer.errors, message='Timings not added for a Model', status_code=HTTP_401_UNAUTHORIZED)

        # else:
        #     serializer = service_WeekdayOpeningExceptionPeriodSerializer(data=request.data, many=isinstance(request.data, list))
        #     print('seraializer',serializer.is_valid(),serializer.errors)
        #     serializer.is_valid(raise_exception=True)
        #     if serializer.is_valid():
        #         self.perform_create(serializer)
        #         headers = self.get_success_headers(serializer.data)
        #         return custom_response(serializer.data, '', message='Timings for Exception dates are added', status_code=HTTP_200_OK)

        #     else:
        #         return custom_response('', serializer.errors, message='Timings for Exception dates are added', status_code=HTTP_401_UNAUTHORIZED)



# class AddService_timings(CreateAPIView):
#     authentication_classes = [JSONWebTokenAuthentication]
#     queryset = service_WeekdayOpeningPeriod.objects.all()
#     serializer_class = service_WeekdayOpeningPeriodSerializer

#     def create(self, request, *args, **kwargs):
#         if isinstance(request.data, list):
#             print(request.data, type(request.data[0]), '---->', request.data[0], 'request.data', type(request.data))
#             for slot in request.data:
#                 if slot['service_id'] != request.data[0]['service_id']:
#                     return custom_response('', 'All service_ids should be the same',
#                                            'You shall add timings for one single model not for multiple.',status_code=HTTP_400_BAD_REQUEST)
            
#             # check if overlopinng in slots

#                 else:
#                     pass
#             else:
#                 pass

#         if isinstance(request.data, list):
#             service_id = request.data[0]['service_id']
#         else:
#             service_id = request.data['service_id']
#         s_price_type = Service.objects.get(pk=service_id).s_price_type
#         if s_price_type=='DAILY':
#                 if len(request.data)>1:
#                     return custom_response(request.data, 'You can not add more then two slots for As your Price Type is "Daily".', message='You can not add more then two slots for As your Price Type is "Daily".', status_code=HTTP_400_BAD_REQUEST)


#         print(service_id,'service_id..')
#         user = self.request.user
#         vendor = Vendor_Info.objects.get(user=user)
      
#         try:
#             service = Service.objects.get(pk=service_id)
#             bl = service.business_location
#             print(bl,'bldfdf',bl.vendor_name,type(bl.vendor_name),vendor,type(vendor),bl.vendor_name!=vendor)
#             vendor_val = service_WeekdayOpeningPeriod.objects.filter(service_id=service_id)
#             if bl.vendor_name!=vendor:
#                 return custom_response('', 'looks like atleast one of the timing belongs to the cameraman that is not under the  logged in vendor, Please contact admin', message='Unable to add timings', status_code=HTTP_400_BAD_REQUEST)

#             if vendor_val.exists():
#                 service_WeekdayOpeningPeriod.objects.filter(service_id=service_id).delete()

#             for item in request.data:
#                 item['service_id']=item['service_id']
#             serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
#             print('seraializer',serializer.is_valid())
#             serializer.is_valid(raise_exception=True)
#             if serializer.is_valid:
#                 print(serializer.errors,'serializer.errors')
#                 self.perform_create(serializer)
#                 headers = self.get_success_headers(serializer.data)
#                 return custom_response(serializer.data, '', message='Timings added', status_code=HTTP_200_OK)
#             else:
#                 return custom_response('', serializer.errors, message='Timings added', status_code=HTTP_400_BAD_REQUEST)

#         except service_WeekdayOpeningPeriod.DoesNotExist:
#             return custom_response('', '', message='Please check the Process ID field',
#                                    status_code=HTTP_400_BAD_REQUEST)
#         except Service.DoesNotExist as e:
#             # print('--->',e,'<---','Service.DoesNotExist',type(e))
#             # return custom_response('', str(e), message='No Photographer found with the given service_id',
#             #                        status_code=HTTP_400_BAD_REQUEST)
#             #
#             return custom_response('', 'No Photographer found with the given service_id', message='No Photographer found with the given service_id',
#                                    status_code=HTTP_400_BAD_REQUEST)


#     # def perform_create(self, serializer,*args):
#     #     print('perform create ran..',self.request.user,args)
#     #     # user = self.request.user
#     #     # vendor = Vendor_Info.objects.get(user=user)
#     #     print(serializer.data,'serailizer.data')
#     #     service = service_WeekdayOpeningPeriod.objects.get(service_id=args[0])
#     #     serializer.save(service_id=service)


# class DispService_timings():pass
# class AddService_timings(CreateAPIView):
#     authentication_classes = [JSONWebTokenAuthentication]
#
#     #Changinged queryset and serializer both were point to wrong i guess.. will check again if that's correct
#     # queryset = vendor_WeekdayOpeningPeriod.objects.all()
#     # serializer_class = vendor_WeekdayOpeningPeriodSerializer
#
#     queryset = service_WeekdayOpeningPeriod.objects.all()
#     serializer_class = service_WeekdayOpeningPeriodSerializer
#
#     def create(self, request, *args, **kwargs):
#         user = self.request.user
#         vendor = Vendor_Info.objects.get(user=user)
#
#         #we must check if the logged in user has this location to him
#         # request.data[0]['service_id']
#
#         try:
#             vbl = Vendor_businessLocation.objects.get(pk=request.data[0]['service_id'])
#             print(vbl.vendor_name,type(vbl.vendor_name),'vbl.vendor_name',vbl.vendor_name==vendor)
#             vendor_val = bl_WeekdayOpeningPeriod.objects.filter(service_id=vbl)
#             if vbl.vendor_name!=vendor:
#                 return custom_response('', 'This business location is not under logged in Vendor, Please contact Admin', message='Timings added', status_code=HTTP_200_OK)
#
#             if vendor_val.exists():
#                 bl_WeekdayOpeningPeriod.objects.filter(service_id=vbl).delete()
#             serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
#             serializer.is_valid(raise_exception=True)
#             self.perform_create(serializer)
#             #below line not being used.
#             # headers = self.get_success_headers(serializer.data)
#             return custom_response(serializer.data, '', message='Timings added', status_code=HTTP_200_OK)
#         except Vendor_businessLocation.DoesNotExist:
#             return custom_response('', '', message='This business does not exist', status_code=HTTP_400_BAD_REQUEST)
#
#     def perform_create(self, serializer):
#         print('perform create ran..',self.request.user)
#         user = self.request.user
#         vendor = Vendor_Info.objects.get(user=user)
#         vbl = Vendor_businessLocation.objects.get(vendor_name=vendor)
#         serializer.save(service_id=vbl)



# class ServicesList(APIView):
#     # authentication_classes = [JSONWebTokenAuthentication]
#     serializer_class = ServiceSerializer
#     filter_backends = [SearchFilter, OrderingFilter]
#     search_fields = ['business_location__name', 'business_location_zipcode']
#     """
#     List all services.
#     """
#
#     def get_queryset(self, *args, **kwargs):
#         print('m i ever here!!!')
#         queryset = Service.objects.filter(publish=True).order_by('-id')
#         query = self.request.GET.get('search')
#         if query:
#             queryset = Service.objects.filter(
#                 Q(business_location__zipcode__icontains=query), publish=True).distinct()
#         return queryset
#
#     def get(self, request, format=None):
#         services = Service.objects.all()
#         serializer = ServiceSerializer(services, many=True)
#         return custom_response(serializer.data, '', message='', status_code=HTTP_200_OK)
