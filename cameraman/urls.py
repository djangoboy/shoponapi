from django.conf.urls import url
from .views import *

urlpatterns = [
    # url(r'^(?P<pk>[0-9]+)/cameraman/$', ServiceDetail.as_view()),
    url(r'^all', ServicesList.as_view()),
    url(r'^(?P<pk>[0-9]+)$', ServiceDetail.as_view()),
    url(r'^add$', AddPhotographer.as_view(), name='add-model'),
    url(r'^update/(?P<id>\d+)$', UpdatePhotographer.as_view(), name='update-model'),
    url(r'^delete/(?P<id>\d+)$', DeletePhotographer.as_view(), name='delete-model'),
    # url(r'^publish', set_Cameraman_publish, name='Publish Vendor'),
    url(r'^addtimings', AddService_timings.as_view(), name='Add Vendor Timings'),
    url(r'^disptimings/(?P<pk>[0-9]+)$', DispService_timings.as_view(), name='Display Timings'),
    url(r'^addgallery', AddService_gallery.as_view(), name='Add Gallery'),
    url(r'^dispgallery/(?P<pk>[0-9]+)$', DispService_gallery.as_view()),
    url(r'^adddocuments', AddService_documents.as_view(), name='Add Documents'),

]
