from django.contrib import admin
from .models import *


# class ServiceAdmin(admin.ModelAdmin):
#     # explicitly reference fields to be shown, note image_tag is read-only
#     fields = ('name', 'description', 'image_tag', 'image', 'websiteURL', 'price', 'min_duration', 'business_location')
#     readonly_fields = ('image_tag',)
#
#
# admin.site.register(Service, ServiceAdmin)
admin.site.register(Service)
admin.site.register(service_WeekdayOpeningExceptionPeriod)
admin.site.register(service_WeekdayOpeningPeriod)
admin.site.register(service_documents)
admin.site.register(service_Gallery)
admin.site.register(service_pricing)