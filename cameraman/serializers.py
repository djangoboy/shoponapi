from rest_framework import serializers

from .models import *
class price_type_Serializer(serializers.ModelSerializer):
    class Meta:
        model = service_pricing
        fields = '__all__'

class service_GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = service_Gallery
        fields = '__all__'


class service_WeekdayOpeningPeriodSerializer(serializers.ModelSerializer):
    class Meta:
        model = service_WeekdayOpeningPeriod
        fields = '__all__'


class service_DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = service_documents
        fields = '__all__'


class ServiceSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""
    #price_type = price_type_Serializer()
    # business_location = serializers.SlugRelatedField(queryset=Vendor_businessLocation.objects.all(),
    #                                                  many=False,
    #                                                  read_only=False,
    #                                                  slug_field='business_name'
    #                                                  )
    business_location_name = serializers.ReadOnlyField(source='business_name')
    vendor_response_time = serializers.ReadOnlyField(source='business_location.vendor_response_time')
    name = serializers.ReadOnlyField(source='alias_name')
    gallery = service_GallerySerializer(many=True, allow_null=True)
    opening_timings = service_WeekdayOpeningPeriodSerializer(many=True, allow_null=True)
    documents = service_DocumentSerializer(many=True, allow_null=True)

    class Meta:
        model = Service
        # fields = ['name', 'description', 'image', 'websiteURL', 'price', 'business_location']
        # fields = '__all__'
        exclude = ('status', )



class service_WeekdayOpeningExceptionPeriodSerializer(serializers.ModelSerializer):
    class Meta:
        model = service_WeekdayOpeningExceptionPeriod
        fields = '__all__'



class ServiceSerializerShortInfo(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""
    #price_type = price_type_Serializer()
    # business_location = serializers.SlugRelatedField(queryset=Vendor_businessLocation.objects.all(),
    #                                                  many=False,
    #                                                  read_only=False,
    #                                                  slug_field='business_name'
    #                                                  )
    
    class Meta:
        model = Service
        # fields = ['name', 'description', 'image', 'websiteURL', 'price', 'business_location']
        fields = '__all__'
        # exclude = ('status', )

class s_model_timings_serializer_field(serializers.RelatedField):
    def get_queryset(self):
        return self.get_queryset()
    def to_representation(self, value):
        print('value',value)
        # duration = time.strftime('%M:%S', time.gmtime(value.duration))
        '''

             {
                "weekday": 0,
                "start": "06:15:07",
                "end": "07:04:08",
                "o_l_h": false,
                "service_id": 43
            }
        '''
        #findme16_07_001
        print(value.start,'value.start',type(value.start))
        obj = {
            "weekday":value.weekday,
            "start":value.start.strftime('%H'),
            "end":value.end.strftime('%H'),
        }
        return obj




class ProductSerializer(serializers.ModelSerializer):
    #price_type = price_type_Serializer()
    name = serializers.ReadOnlyField(source='alias_name')
    vendor_response_time = serializers.ReadOnlyField(source='business_location.vendor_response_time')
    service_model_timings = s_model_timings_serializer_field(many=True)

    class Meta:
        model = Service
        # fields = ['name', 'description', 'image', 'websiteURL', 'price', 'business_location']
        # fields = ['name', 'description', 'city', 'zipcode', 'state', 'image', 'prefer_to_travel', 'hourly_price',
        #           'min_duration', 'is_videographer', 'vendor_response_time', 'copyrightrules', 'specialinstructions',
        #           'rating', 'no_of_previous_bookings', 'terms_and_conditions']

        fields = '__all__'

class Services_Serializer(serializers.ModelSerializer):
    can_be_deleted = serializers.BooleanField(default=True, read_only=True)
    publish = serializers.BooleanField(default=True, read_only=True)
    vendor_response_time = serializers.ReadOnlyField(source='business_location.vendor_response_time')
    name = serializers.ReadOnlyField(source='alias_name')

    class Meta:
        model = Service
        exclude = ('status', )
        # fields = '__all__'


