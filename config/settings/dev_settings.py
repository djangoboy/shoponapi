import os
import django_heroku
import datetime
# import dj_database_url
# db_from_env = dj_database_url.config()

from celery.schedules import crontab   

from config import settings

print('dev setting ran')
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
Base_URL = 'https://testphobizapi.herokuapp.com'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
# Honor the 'X-Forwarded-Proto' header for request.is_secure()

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

CELERY_BROKER_URL  = 'redis://h:p5d7687414751e21817f5c167c0fa007e6694f8a143864b2ea2c441c37de02a79@ec2-3-227-28-169.compute-1.amazonaws.com:16279'

# CELERY_BROKER_URL = 'redis://h:p9a73c0517a4f5c5a3a0ab563e35fddb3fcf1a4412c4db9af6c35d90820438588@ec2-34-238-13-119.compute-1.amazonaws.com:14719'
# CELERY_RESULT_BACKEND = 'redis://redis:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

CELERY_BEAT_SCHEDULE = {
 'check-for-upcoming-booking': {
       'task': 'upcoming_booking',
        # There are 4 ways we can handle time, read further 
        #runs every 4 hours
       'schedule': 14400 * 1.0,
        # If you're using any arguments
    },
 'delete-seen-notification': {
       'task': 'delete_notifications',
        # There are 4 ways we can handle time, read further 
        #runs every 4 hours
       'schedule': 3600 * 4.0,
        # If you're using any arguments
    }
    }


DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Disable Django's own staticfiles handling in favour of WhiteNoise, for
    # greater consistency between gunicorn and `./manage.py runserver`. See:
    # http://whitenoise.evans.io/en/stable/django.html#using-whitenoise-in-development
    'storages',
)

THIRD_PARTY_APPS = (
    'rest_framework',
    'rest_framework.authtoken',
    'corsheaders',
    'rest_framework_swagger',
    'rest_framework_gis',
    'djmoney',
    # 'braintree',
    # 'widget_tweaks',
    'django_extensions',
    # 'django_doc_view',
    # 'crispy_forms',
)




LOCAL_APPS = (
    'accounts',
    # 'vendor',
    'booking',
    'reminder',
    'cameraman',
    # 'studio',
    # 'model',
    'lookup',
    'cartinfo',
    'payment',
    'notification',
    'services',

    # 'test_queries'



)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'


#this is a remote db i am using for now just to make sure we don't effect the current dev db.
# if everything is well down the road we will put real dev db there below.

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'OPTIONS': {
#             'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
#         },
#         'NAME': 'Yw8VHdbpVP',
#         'HOST': 'remotemysql.com',
#         'PORT': '3306',
#         'USER': 'Yw8VHdbpVP',
#         'PASSWORD': 'fmUwU0lmwU',
#     }
# }
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'OPTIONS': {
#             'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
#         },
#         'NAME': '0vjXiC7riN',
#         'HOST': 'remotemysql.com',
#         'PORT': '3306',
#         'USER': '0vjXiC7riN',
#         'PASSWORD': 'OGiY7jZYrk',
#     }
# }


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'OPTIONS': {
#             'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
#         },
#         'NAME': 'sSju3vqwS4',
#         'HOST': 'remotemysql.com',
#         'PORT': '3306',
#         'USER': 'sSju3vqwS4',
#         'PASSWORD': 'uR5DwCGZEb',
#     }
# }

# DATABASES['default'].update(db_from_env)
# DATABASES['default']['CONN_MAX_AGE'] = 500

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

# Account

from .base_settings import DEBUG as BASE_ENV

if settings.ENV == 'DEV':

    AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
    AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
    AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
    AWS_S3_CUSTOM_DOMAIN = AWS_STORAGE_BUCKET_NAME + '.s3.amazonaws.com'
    # AWS_PRELOAD_METADATA = True
    # AWS_QUERYSTRING_AUTH = False
    AWS_S3_OBJECT_PARAMETERS = {
        'CacheControl': 'max-age=86400',
    }
    AWS_LOCATION = 'static'
    STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
    STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, AWS_LOCATION)
    DEFAULT_FILE_STORAGE = 'config.settings.storage_backends.MediaStorage'

    # CELERY_BROKER_URL = 'redis://h:p9a73c0517a4f5c5a3a0ab563e35fddb3fcf1a4412c4db9af6c35d90820438588@ec2-34-238-13-119.compute-1.amazonaws.com:14719'
    CELERY_BROKER_URL  = 'redis://h:p5d7687414751e21817f5c167c0fa007e6694f8a143864b2ea2c441c37de02a79@ec2-3-227-28-169.compute-1.amazonaws.com:16279'


    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_USE_TLS = True
    EMAIL_PORT = 587
    EMAIL_HOST_USER = 'connect.shopon@gmail.com'
    EMAIL_HOST_PASSWORD = 'Shopon@123'

    DEFAULT_FROM_EMAIL = 'connect.shopon@gmail.com'

    ADMINS = (
        ('Deepak Singh', 'connect.shopon@gmail.com'),
    )
    MANAGERS = ADMINS

    CORS_REPLACE_HTTPS_REFERER      = True
    HOST_SCHEME                     = "https://"
    SECURE_PROXY_SSL_HEADER         = ('HTTP_X_FORWARDED_PROTO', 'https')
    SECURE_SSL_REDIRECT             = True
    SESSION_COOKIE_SECURE           = True
    CSRF_COOKIE_SECURE              = True
    SECURE_HSTS_INCLUDE_SUBDOMAINS  = True
    SECURE_HSTS_SECONDS             = 1000000
    SECURE_FRAME_DENY               = True
# AWS_ACCESS_KEY_ID = 'AKIAXP7ALGJFZU265HEO'
    # AWS_SECRET_ACCESS_KEY = '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'
    # AWS_STORAGE_BUCKET_NAME = 'shopon-test1'
    # AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
    # AWS_S3_OBJECT_PARAMETERS = {
    #     'CacheControl': 'max-age=86400',
    # }
    # AWS_LOCATION = 'static'
    # STATICFILES_DIRS = [
    #     os.path.join(os.path.dirname(BASE_DIR), 'static_backend'), ]
    # STATIC_URL = 'https://%s/%s/' % (AWS_S3_CUSTOM_DOMAIN, AWS_LOCATION)
    # STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
    # DEFAULT_FILE_STORAGE = 'config.storage_backends.MediaStorage'  # <-- here is where we reference it


    # EMail settings
    # EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    # EMAIL_HOST = 'smtp.gmail.com'
    # EMAIL_USE_TLS = True
    # EMAIL_PORT = 587
    # EMAIL_HOST_USER = 'connecting.deepaksingh@gmail.com'
    # EMAIL_HOST_PASSWORD = 'dJANGO@321'

    # DEFAULT_FROM_EMAIL = 'connecting.deepaksingh@gmail.com'

    # ADMINS = (
    #     ('Deepak Singh', 'connecting.deepaksingh@gmail.com'),
    # )
    # MANAGERS = ADMINS
    
#
# CORS_REPLACE_HTTPS_REFERER      = True
# HOST_SCHEME                     = "https://"
# SECURE_PROXY_SSL_HEADER         = ('HTTP_X_FORWARDED_PROTO', 'https')
# SECURE_SSL_REDIRECT             = True
# SESSION_COOKIE_SECURE           = True
# CSRF_COOKIE_SECURE              = True
# SECURE_HSTS_INCLUDE_SUBDOMAINS  = True
# SECURE_HSTS_SECONDS             = 1000000
# SECURE_FRAME_DENY               = True
#this below one is written for dev but not being used will take a look on this after for now using what is being used
    #using default one in base
    # REST_FRAMEWORK = {
    #     # Add custom renderer.
    #     'DEFAULT_METADATA_CLASS': 'rest_framework.metadata.SimpleMetadata',
    #     'DEFAULT_PERMISSION_CLASSES': (
    #         'config.permissions.DisableOptionsPermission',
    #     ),
    #     'DEFAULT_RENDERER_CLASSES': ('rest_framework.renderers.JSONRenderer',
    #                                  # 'config.renders.CustomJSONRenderer',
    #                                  'rest_framework.renderers.BrowsableAPIRenderer'
    #                                  ),
    #     # Add custom exception handler
    #     'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',),
    #     'EXCEPTION_HANDLER': 'config.custom_exception_handler.custom_exception_handler',
    #     'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    #     'PAGE_SIZE': 10,
    # }



CORS_ORIGIN_ALLOW_ALL = True

django_heroku.settings(locals())
# del DATABASES['default']['OPTIONS']['sslmode']



