import os

from django.conf import settings
import braintree
print('local setting ran')
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
# Honor the 'X-Forwarded-Proto' header for request.is_secure()
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

ALLOWED_HOSTS = ['*']
Base_URL = 'http://127.0.0.1:8000'

# dummy = os.environ['local']
CRISPY_TEMPLATE_PACK = 'bootstrap3'
# Application definition

import braintree

gateway = braintree.BraintreeGateway(
    braintree.Configuration(
        braintree.Environment.Sandbox,
        merchant_id="wt63whjk6ytd2b2q",
        public_key="y2bxcw3q36592txk",
        private_key="9293124facfa39f1ca81839be6bc1a82"
    )
)

DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'debug_toolbar',
    'storages',
    # 'celery'

)

THIRD_PARTY_APPS = (
    'rest_framework',
    'rest_framework.authtoken',
    'corsheaders',
    'rest_framework_swagger',
    'rest_framework_gis',
    'djmoney',
    'django_extensions',
    'django_filters',
    'braintree'


)

LOCAL_APPS = (
    'accounts',
    'vendor',
    'booking',
    'reminder',
    'cameraman',
    'studio',
    'model',
    'lookup',
    'cartinfo',
    'payment',
    'notification',
    # 'test_queries'
)

INSTALLED_APPS = DJANGO_APPS +LOCAL_APPS +THIRD_PARTY_APPS


# BRAINTREE_PRODUCTION = False  # We'll need this later to switch between the sandbox and live account
# BRAINTREE_MERCHANT_ID = "wt63whjk6ytd2b2q"
# BRAINTREE_PUBLIC_KEY = "y2bxcw3q36592txk"
# BRAINTREE_PRIVATE_KEY = "9293124facfa39f1ca81839be6bc1a82"

# braintree.Configuration.configure(braintree.Environment.Sandbox,
#     merchant_id=settings.BRAINTREE_MERCHANT_ID,
#     public_key=settings.BRAINTREE_PUBLIC_KEY,
#     private_key=settings.BRAINTREE_PRIVATE_KEY)


MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

INTERNAL_IPS = ['127.0.0.1',]
ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


WSGI_APPLICATION = 'config.wsgi.application'


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'OPTIONS': {
#             'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
#         },
#         'NAME': 'sSju3vqwS4',
#         'HOST': 'remotemysql.com',
#         'PORT': '3306',
#         'USER': 'sSju3vqwS4',
#         'PASSWORD': 'uR5DwCGZEb',
#     }
# }
#THIS BELOW IS READY WITH DATA IN IT.
# CELERY_BROKER_URL  = 'redis://h:p5d7687414751e21817f5c167c0fa007e6694f8a143864b2ea2c441c37de02a79@ec2-3-227-28-169.compute-1.amazonaws.com:16279'
# from celery.schedules import crontab   
CELERY_BROKER_URL = 'amqp://localhost:5672/'

# CELERY_BROKER_URL = 'redis://h:p9a73c0517a4f5c5a3a0ab563e35fddb3fcf1a4412c4db9af6c35d90820438588@ec2-34-238-13-119.compute-1.amazonaws.com:14719'
# CELERY_RESULT_BACKEND = 'redis://redis:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'


CELERY_BEAT_SCHEDULE = {
 'check-for-upcoming-booking': {
       'task': 'upcoming_booking',
        # There are 4 ways we can handle time, read further 
        #runs every 4 hours
       'schedule': 14400 * 1.0,
        # If you're using any arguments
    },
 'delete-seen-notification': {
       'task': 'delete_notifications',
        # There are 4 ways we can handle time, read further 
        #runs every 4 hours
       'schedule': 3600 * 4.0,
        # If you're using any arguments
    }
    }

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'OPTIONS': {
#             'init_command': "SET sql_mode='STRICT_ALL_TABLES'",
#         },
#         'NAME': 'hxIj4AVfgR',
#         'HOST': 'remotemysql.com',
#         'PORT': '3306',
#         'USER': 'hxIj4AVfgR',
#         'PASSWORD': 'pdUQ9JbZWF',
#     }
# }

# DATABASE_URL = 'postgres://gnzeosccwnbuqi:56cf7b1e9a6fe0318c24716bae530bbfedd777237656028b972c5f7cc69d3c2d@ec2-107-22-228-141.compute-1.amazonaws.com:5432/dcmp06m70topk7'
# # DATABASES = {
# #     'default': {
# #         'ENGINE': 'django.db.backends.postgresql',
# #         'NAME': 'hello_django_dev',
# #         'USER': 'hello_django',
# #         'PASSWORD': 'hello_django',
# #         'HOST': 'db',
# #         'PORT': '5432',
# #     }
# # }
# import dj_database_url
# DATABASES = {
#     'default': dj_database_url.config(default=DATABASE_URL)
# }

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'OPTIONS': {
#             'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
#         },
#         'NAME': '0vjXiC7riN',
#         'HOST': 'remotemysql.com',
#         'PORT': '3306',
#         'USER': '0vjXiC7riN',
#         'PASSWORD': 'OGiY7jZYrk',
#     }
# }

#BELOW IS MY LOCAL
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'OPTIONS': {
#             'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
#         },
#         'NAME': 'phobizldb',
#         'HOST': '127.0.0.1',
#         'PORT': '',
#         'USER': 'phobizldbuser',
#         'PASSWORD': 'Shopon@123!',
#     }
# }





# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/
print(BASE_DIR,'<--')
if settings.ENV == 'LOCAL':
    STATIC_URL = '/static/'
    STATICFILES_DIRS = (os.path.join(os.path.dirname(BASE_DIR), "static_backend"),)
    MEDIA_URL= '/media/'
    STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_root")
    MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media_root")
    #
    # AWS_ACCESS_KEY_ID = 'AKIAI4M6SW74FDCTUM7A'
    # AWS_SECRET_ACCESS_KEY = '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'
    # AWS_STORAGE_BUCKET_NAME = 'mwv-s3-pbz-dev'
    # AWS_S3_CUSTOM_DOMAIN = AWS_STORAGE_BUCKET_NAME + '.s3.amazonaws.com'
    # AWS_PRELOAD_METADATA = True
    # AWS_QUERYSTRING_AUTH = False
    # AWS_S3_OBJECT_PARAMETERS = {
    #     'CacheControl': 'max-age=86400',
    # }
    # AWS_LOCATION = 'static'
    # STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
    # STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, AWS_LOCATION)
    # DEFAULT_FILE_STORAGE = 'config.storage_backends.MediaStorage'





    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_USE_TLS = True
    EMAIL_PORT = 587
    EMAIL_HOST_USER = 'connect.shopon@gmail.com'
    EMAIL_HOST_PASSWORD = 'Shopon@123'

    DEFAULT_FROM_EMAIL = 'connect.shopon@gmail.com'

    ADMINS = (
        ('Deepak Singh', 'connect.shopon@gmail.com'),
    )
    MANAGERS = ADMINS



CORS_ORIGIN_ALLOW_ALL = True

CORS_ORIGIN_WHITELIST = (
    'http://localhost:4200',
)
