"""
WSGI config for config project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
# from dj_static import Cling

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")

# application = Cling(get_wsgi_application())
application = get_wsgi_application()

# import subprocess
# import os
# print(os.getcwd(),'getcwd')
# os.chdir('/home/kafalsoft/Desktop/shopon-master-backup:825::18june/notification')
# subprocess.Popen(['bash', 'start_celery_bash.bash'])
# # print(cmd,'cmd prompt')

