import logging
logging.basicConfig(level=logging.DEBUG,filename='test.log')
def atdtd(a,b):
    return a+b

def stutb(a,b):
    return a-b

def mtutl(a,b):
    return a*b

def dtitv(a,b):
    return a/b


a,b=10,5
add_res = atdtd(a,b)
logging.debug('Add: {} + {} = {}'.format(a,b,add_res))
sub_res = stutb(a,b)
logging.debug('Sub: {} + {} = {}'.format(a,b,sub_res))
mul_res = (a,b)
logging.debug('Mul: {} + {} = {}'.format(a,b,mul_res))
div_res = dtitv(a,b)
logging.debug('Div: {} + {} = {}'.format(a,b,div_res))


