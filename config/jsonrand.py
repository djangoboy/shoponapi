from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import exception_handler


def custom_response(data: object, errors: object, message: object = '', status_code: object = HTTP_200_OK) -> object:
    return Response(
        {"data": data,
         "error": errors,
         "message": message,
         "status": status_code
         },
        status=HTTP_200_OK
    )


