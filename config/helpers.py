# for Gallery
def modify_input_for_multiple_files(service, name, img):
    dict = {}
    dict['service_id'] = service
    dict['title'] = name
    dict['image'] = img
    return dict


# for Documents
def modify_input_for_multiple(property_id, name, file, type):
    dict = {}
    dict['service_id'] = property_id
    dict['document_name'] = name
    dict['document_file'] = file
    dict['document_type'] = type
    return dict
