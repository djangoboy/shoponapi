# import debug_toolbar
import debug_toolbar
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.views.static import serve
from rest_framework.documentation import include_docs_urls
from rest_framework_swagger.views import get_swagger_view

from lookup.views import load_option_set, load_option_set_test
from lookup.viewsets import load_options
from accounts.views import register_view
# from model.views import CheckSlot_availibity
from booking.views import CheckBookingSlotAvail
schema_view = get_swagger_view(title='Shopon API')

urlpatterns = [
    url(r'^docs/', include_docs_urls(title='Shopon API')),
    url(r'^apis', schema_view),
    url(r'^admin/', admin.site.urls),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT, }),
    url(r'^api/user/', include('accounts.urls', namespace='accounts')),
    # url(r'^api/service/', include('model.urls')),
    url(r'^api/service_list/', include('services.urls')),
    url(r'^api/product/', include('product.urls')),

    # url(r'^payment/', include('payment.urls')),
    url(r'^api/notification/', include('notification.urls')),
    url(r'^api/load/', load_options, name='options'),
    url(r'^api/load_option_set/', load_option_set.as_view()),
    url(r'^api/load_option_set_test/', load_option_set_test.as_view()),
    # url(r'^api/model/', include('model.urls')),
    # url(r'^api/cameraman/', include('cameraman.urls')),
    url(r'^api/order/', include('orders.urls')),

    # url(r'^api/studio/', include('studio.urls')),
    # url(r'^api/vendor/', include('vendor.urls')),
    url(r'^api/bookings/', include('booking.urls')),
    url(r'^api/cart/', include('cartinfo.urls')),
    # url(r'^api/', include('doctor.urls')),
    # url(r'^api/reminders/', include('reminder.urls')),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        register_view, name='activate'),
    # url(r'^api/check_slot_availability$', CheckSlot_availibity.as_view(), name='check slots'),
    # url(r'^api/check_booking_slot$', CheckBookingSlot.as_view(), name='check booking slots'),
    url(r'^api/check_slot_availability$', CheckBookingSlotAvail.as_view(), name='check booking slots avail'),

]
if settings.ENV == 'LOCAL':
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += path('__debug__/', include(debug_toolbar.urls)),



    # + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    #
