from django.db import models
from djmoney.models.fields import MoneyField
POLISH_COLOR = (
    ('BLACK','BLACK'),
    ('BROWN','BROWN'),
    ('WHITE','WHITE')


)
SHOETYPE = (
    ('LATHER','LATHER'),
    ('SNEACKER','SNEACKER'),

)


# Create your models here.
class Service(models.Model):
    name = models.CharField(max_length=100)
    shoe_type = models.CharField(max_length=20,blank=False,null=False, default=None, choices=SHOETYPE)
    price = MoneyField(max_digits=14, decimal_places=2, default_currency='INR',null=True,blank=True)
    about_service=models.TextField(max_length=300,null=True,blank=True)
    polish_color = models.CharField(max_length=100, null=True, blank=True,choices=POLISH_COLOR,default=None)
    
    # service_type = models.ForeignKey(Vendor_businessLocation, related_name='businesslocation_models',on_delete=models.CASCADE,null=True,blank=True)
    
    def save(self,*args,**kwargs):
        if self.shoe_type:
            if self.shoe_type!='LATHER':
                self.polish_color=None
        return super().save(*args,**kwargs)

    def __str__(self):
        return self.name + ' > '+self.shoe_type
    
