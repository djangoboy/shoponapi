from .models import Service
from rest_framework import serializers

class ServiceSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""
    class Meta:
        model = Service
        fields = '__all__'

