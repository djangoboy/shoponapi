
import json
import pprint
import time
from datetime import datetime, date
from django.db.models import Q
from django.http import Http404, QueryDict
from django.shortcuts import render

# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework import status
from rest_framework.generics import CreateAPIView, RetrieveUpdateAPIView, ListAPIView
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_204_NO_CONTENT, HTTP_401_UNAUTHORIZED

from django.shortcuts import render
from config.jsonrand import custom_response
# from rest_framework.parsers import MultiPartParser, FormParser
# from rest_framework.response import Response
# from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_204_NO_CONTENT, HTTP_401_UNAUTHORIZED
# from rest_framework.permissions import AllowAny
# from rest_framework_jwt.authentication import JSONWebTokenAuthentication

# from booking.models import Booking
# from config.helpers import modify_input_for_multiple_files, modify_input_for_multiple
# from config.jsonrand import custom_response
# from rest_framework.views import APIView

# from studio.serializers import service_PriceSerializer
from .serializers import *
# from .models import *

# from .permissions import (
#     ProductPermission
# )
# Create your views here.
class ServicesList(ListAPIView):
    # authentication_classes = [JSONWebTokenAuthentication]
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('shoe_type',)

    def list(self, request, *args, **kwargs):
        import time
        # time.sleep(10)
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return custom_response(serializer.data, '', message='success', status_code=HTTP_200_OK)
   
  